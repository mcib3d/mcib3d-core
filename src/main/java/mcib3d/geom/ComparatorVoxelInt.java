/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mcib3d.geom;

import mcib3d.geom2.VoxelIntComparable;

import java.util.Comparator;

/**
 *
 * @author thomasb
 */
public class ComparatorVoxelInt implements Comparator<VoxelIntComparable> {

    @Override
    public int compare(VoxelIntComparable V0, VoxelIntComparable V1) {
        double v0 = V0.getValue();
        double v1 = V1.getValue();

        if (v0 == v1) {
            if (V0.getCompare() > V1.getCompare()) {
                return 1;
            } else {
                return +1;
            }
        } else if (v0 > v1) {
            return -1;
        } else {
            return 1;
        }
    }

}
