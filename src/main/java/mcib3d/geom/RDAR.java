package mcib3d.geom;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.measurements.MeasureEllipsoid;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thomasb on 19/7/16.
 */
public class RDAR {
    private Object3DInt volume;
    private Object3DInt ellipsoid;
    private List<Object3DInt> partsIn = null;
    private List<Object3DInt> partsOut = null;

    public RDAR(Object3DInt volume) {
        this.volume = volume;
        ellipsoid = null;
    }

    private void compute() {
        ellipsoid = new MeasureEllipsoid(volume).getEllipsoid();

        // difference volume - ellipsoid
        Object3DInt object3DVoxels = new Object3DComputation(volume).getObjectSubtracted(ellipsoid);
        if (object3DVoxels.size() > 0) {
            partsOut = new Object3DComputation(object3DVoxels).getConnexComponents();
        }

        // difference ellipsoid - volume
        object3DVoxels = new Object3DComputation(ellipsoid).getObjectSubtracted(volume);
        if (object3DVoxels.size() > 0) {
            partsIn = new Object3DComputation(object3DVoxels).getConnexComponents();
        }
    }

    public List<Object3DInt> getPartsIn() {
        return getPartsIn(0);
    }

    public List<Object3DInt> getPartsOut() {
        return getPartsOut(0);
    }

    public int getPartsInNumber() {
        if (getPartsIn() == null) return 0;
        else return getPartsIn().size();
    }

    public int getPartsOutNumber() {
        if (getPartsOut() == null) return 0;
        else return getPartsOut().size();
    }

    public int getPartsOutVolumePixels() {
        return getPartsOutVolumePixels(0);
    }

    public int getPartsOutVolumePixels(int minVolume) {
        if (getPartsOut(minVolume) == null) return 0;
        int volume = 0;
        for (Object3DInt object3DVoxels : getPartsOut(minVolume)) {
            volume += object3DVoxels.size();
        }

        return volume;
    }


    public double getPartsInVolumeUnit() {
        return getPartsInVolumeUnit(0);
    }

    public double getPartsInVolumeUnit(int minVolume) {
        if (getPartsIn(minVolume) == null) return 0;
        double volume = 0;
        for (Object3DInt object3DVoxels : getPartsIn(minVolume)) {
            volume += object3DVoxels.size();
        }

        return volume;
    }

    public double getPartsOutVolumeUnit() {
        return getPartsOutVolumeUnit(0);
    }

    public double getPartsOutVolumeUnit(int minVolume) {
        if (getPartsOut(minVolume) == null) return 0;
        double volume = 0;
        for (Object3DInt object3DVoxels : getPartsOut(minVolume)) {
            volume += object3DVoxels.size();
        }

        return volume;
    }


    public int getPartsInVolumePixels() {
        return getPartsInVolumePixels(0);
    }

    public int getPartsInVolumePixels(int minVolume) {
        if (getPartsIn(minVolume) == null) return 0;
        int volume = 0;
        for (Object3DInt object3DVoxels : getPartsIn(minVolume)) {
            volume += object3DVoxels.size();
        }

        return volume;
    }


    public List<Object3DInt> getPartsIn(int minVolume) {
        if (ellipsoid == null) compute();
        if (partsIn == null) return null;
        List<Object3DInt> result = new ArrayList<>();
        for (Object3DInt part : partsIn) if (part.size() > minVolume) result.add(part);

        return result;
    }

    public List<Object3DInt> getPartsOut(int minVolume) {
        if (ellipsoid == null) compute();
        if (partsOut == null) return null;
        List<Object3DInt> result = new ArrayList<>();
        for (Object3DInt part : partsOut) if (part.size() > minVolume) result.add(part);
        return result;
    }

    public int getPartsInNumber(int minVolume) {
        if (getPartsIn(minVolume) == null) return 0;
        else return getPartsIn(minVolume).size();
    }

    public int getPartsOutNumber(int minVolume) {
        if (getPartsOut(minVolume) == null) return 0;
        else return getPartsOut(minVolume).size();
    }

    public Object3DInt getEllipsoid() {
        return ellipsoid;
    }

    public void setVolume(Object3DInt volume) {
        this.volume = volume;
        ellipsoid = null;
    }
}
