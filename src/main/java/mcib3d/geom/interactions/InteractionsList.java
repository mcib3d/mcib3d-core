package mcib3d.geom.interactions;

import ij.IJ;
import ij.measure.ResultsTable;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.PairObjects3DInt;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class InteractionsList {
    HashMap<String, PairObjects3DInt> interactions;
    Objects3DIntPopulation population;

    public InteractionsList() {
        interactions = new HashMap<>();
    }

    public Objects3DIntPopulation getPopulation() {
        return population;
    }

    public void setPopulation(Objects3DIntPopulation population) {
        this.population = population;
    }

    public boolean contains(String inter) {
        return interactions.containsKey(inter);
    }

    public boolean containsInt(int a, int b) {
        return interactions.containsKey(buildKey(a, b));
    }

    public boolean containsFloat(float a, float b) {
        return interactions.containsKey(buildKey(a, b));
    }

    public boolean addInteraction(PairObjects3DInt pair) {
        PairObjects3DInt inter = interactions.putIfAbsent(buildKey(pair.getObject3D1().getLabel(), pair.getObject3D2().getLabel()), pair);
        return (inter != null);
    }

    public boolean addInteraction(Object3DInt object3D1, Object3DInt object3D2) {
        return addInteraction(object3D1, object3D2, 0);
    }

    public boolean addInteraction(Object3DInt object3D1, Object3DInt object3D2, int volume) {
        PairObjects3DInt pair;
        pair = new PairObjects3DInt(object3D1, object3D2, volume);
        PairObjects3DInt inter = interactions.putIfAbsent(buildKey(pair.getObject3D1().getLabel(), pair.getObject3D2().getLabel()), pair);

        return (inter != null);
    }

    @Deprecated
    public double incrementPairVolume(int a, int b) {
        return incrementPairVolume(a, b, 1);
    }

    public double incrementPairVolume(float a, float b) {
        return incrementPairVolume(a, b, 1);
    }

    @Deprecated
    public double incrementPairVolume(int a, int b, int vol) {
        PairObjects3DInt pair = getPair(a, b);
        pair.incrementValuePair(vol);

        return pair.getPairValue();
    }

    public double incrementPairVolume(float a, float b, int vol) {
        PairObjects3DInt pair = getPair(a, b);
        pair.incrementValuePair(vol);

        return pair.getPairValue();
    }



    public PairObjects3DInt getPair(float a, float b) {
        return interactions.get(buildKey(a, b));
    }

    // to be removed
    @Deprecated
    public HashMap<String, PairObjects3DInt> getMap() {
        return interactions;
    }

    private String buildKey(float a, float b) {
        return a + "-" + b;
    }

    public String toString() {
        String res = "";
        for (String S : interactions.keySet()) {
            res = res.concat(S) + ", ";
        }

        return res;
    }

    public ResultsTable getResultsTableOnlyColoc() {
        IJ.log("Interactions completed ("+interactions.size()+" interactions found), building results table");
        ResultsTable rt0 = ResultsTable.getResultsTable();
        if (rt0 == null) rt0 = new ResultsTable();
        rt0.reset();

        final ResultsTable rt = rt0;

        AtomicInteger ai = new AtomicInteger(0);
        population.getObjects3DInt().forEach(object -> {
            rt.incrementCounter();
            rt.setValue("Value", ai.get(), object.getLabel());

            List<PairObjects3DInt> list1 = getObject1ColocalisationPairs(object);

            if ((list1.size() == 0)) {
                rt.setValue("O1", ai.get(), 0);
                rt.setValue("V1", ai.get(), 0);
            }
            for (int c = 0; c < list1.size(); c++) {
                PairObjects3DInt colocalisation = list1.get(c);
                if (colocalisation.getObject3D1().getLabel() != object.getLabel()) IJ.log("Pb object1 : " + object.getLabel() +" / "+colocalisation.getObject3D1().getLabel());
                Object3DInt object2 = colocalisation.getObject3D2();
                rt.setValue("O" + (c + 1), ai.get(), object2.getLabel());
                rt.setValue("V" + (c + 1), ai.get(), colocalisation.getPairValue());
            }
            int andIncrement = ai.getAndIncrement();
        });
        rt.sort("Value");

        return rt;
    }

    public List<PairObjects3DInt> getObject1ColocalisationPairs(Object3DInt object3D){
        return getObject1ColocalisationPairs(object3D, false);
    }

    public List<PairObjects3DInt> getObject1ColocalisationPairs(Object3DInt object3D, boolean ascending) {
        float value = object3D.getLabel();
        return interactions.keySet().stream().filter(key -> key.startsWith(value + "-"))
                .map(key -> interactions.get(key))
                .sorted((p1, p2) -> {
                    if (ascending) return (int) Math.signum(p1.getPairValue() - p2.getPairValue());
                    else return (int) Math.signum(p2.getPairValue() - p1.getPairValue());
                })
                .collect(Collectors.toList());
    }

    public List<PairObjects3DInt> getObject2ColocalisationPairs(Object3DInt object3D) {
        float value = object3D.getLabel();
        return interactions.keySet().stream().filter(key -> key.endsWith("-" + value))
                .map(key -> interactions.get(key)).collect(Collectors.toList());
    }

}
