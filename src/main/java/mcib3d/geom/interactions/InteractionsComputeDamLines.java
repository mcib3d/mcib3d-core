package mcib3d.geom.interactions;

import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.utils.ArrayUtil;

public class InteractionsComputeDamLines implements InteractionsCompute {

    @Override
    public InteractionsList compute(ImageHandler image) {
        // get population from image
        Objects3DIntPopulation population = new Objects3DIntPopulation(image);
        InteractionsList interactions = new InteractionsList();
        interactions.setPopulation(population);
        for (int z = 0; z < image.sizeZ; z++) {
            for (int x = 0; x < image.sizeX; x++) {
                for (int y = 0; y < image.sizeY; y++) {
                    if (image.getPixel(x, y, z) == 0) {
                        ArrayUtil util = image.getNeighborhoodCross3D(x, y, z); // 6 neighbors
                        util = util.distinctValues();
                        for (int i = 0; i < util.size(); i++) {
                            for (int j = i + 1; j < util.size(); j++) {
                                float vali = util.getValueInt(i);
                                float valj = util.getValueInt(j);
                                if ((vali > 0) && (valj > 0)) {
                                    if (!interactions.containsFloat(vali, valj)) {
                                        interactions.addInteraction(population.getObjectByValue(vali), population.getObjectByValue(valj));
                                    }
                                    if (!interactions.containsFloat(valj, vali)) {
                                        interactions.addInteraction(population.getObjectByValue(valj), population.getObjectByValue(vali));
                                    }
                                    interactions.incrementPairVolume(vali, valj);
                                    interactions.incrementPairVolume(valj, vali);
                                }
                            }
                        }
                    }
                }
            }
        }

        return interactions;
    }

}
