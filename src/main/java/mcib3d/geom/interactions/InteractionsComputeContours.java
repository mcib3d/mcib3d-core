package mcib3d.geom.interactions;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;
import mcib3d.utils.ArrayUtil;

import java.util.List;

public class InteractionsComputeContours implements InteractionsCompute {
    @Override
    public InteractionsList compute(ImageHandler image) {
        // get population from image
        Objects3DIntPopulation population = new Objects3DIntPopulation(image);
        InteractionsList interactions = new InteractionsList();
        interactions.setPopulation(population);
        for (Object3DInt object3D : population.getObjects3DInt()) {
            List<VoxelInt> list = new Object3DComputation(object3D).getContour();
            for (VoxelInt voxel3D : list) {
                ArrayUtil util = image.getNeighborhoodCross3D(voxel3D.getX(), voxel3D.getY(), voxel3D.getZ());
                util = util.distinctValues();
                for (int i = 0; i < util.size(); i++) {
                    for (int j = i + 1; j < util.size(); j++) {
                        float vali = util.getValueInt(i);
                        float valj = util.getValueInt(j);
                        if ((vali > 0) && (valj > 0)) {
                            if ((vali == object3D.getLabel()) || (valj == object3D.getLabel())) {
                                if (!interactions.containsFloat(vali, valj)) {
                                    interactions.addInteraction(population.getObjectByValue(vali), population.getObjectByValue(valj));
                                }
                                interactions.incrementPairVolume(vali, valj);
                                if (!interactions.containsFloat(valj, vali)) {
                                    interactions.addInteraction(population.getObjectByValue(valj), population.getObjectByValue(vali));
                                }
                                interactions.incrementPairVolume(valj, vali);
                            }
                        }
                    }
                }
            }
        }

        return interactions;
    }
}
