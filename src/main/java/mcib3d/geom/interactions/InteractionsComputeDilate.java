package mcib3d.geom.interactions;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InteractionsComputeDilate implements InteractionsCompute {
    float rx, ry, rz;

    public InteractionsComputeDilate(float rx, float ry, float rz) {
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
    }

    @Override
    public InteractionsList compute(ImageHandler image) {
        // get population from image
        Objects3DIntPopulation population = new Objects3DIntPopulation(image);
        InteractionsList interactions = new InteractionsList();
        interactions.setPopulation(population);
        for (Object3DInt object3D : population.getObjects3DInt()) {
            float value = object3D.getLabel();
            // get dilated object and its contour
            Object3DInt dilated = new Object3DComputation(object3D).getObjectDilated(rx, ry, rz);
            List<VoxelInt> contours = new Object3DComputation(dilated).getContour();
            // check contour values in image
            Set<Float> neighbours = new HashSet<>();
            contours.stream().filter(image::contains).forEach(C -> neighbours.add(image.getPixel(C)));
            neighbours.stream().filter(N -> (N > 0)).filter(N -> (N != value))
                    .forEach(N -> {
                        float vali = value;
                        float valj = N;
                        if ((vali > 0) && (valj > 0)) {
                            if ((vali == object3D.getLabel()) || (valj == object3D.getLabel())) {
                                if (!interactions.containsFloat(vali, valj)) {
                                    interactions.addInteraction(population.getObjectByValue(vali), population.getObjectByValue(valj));
                                }
                                interactions.incrementPairVolume(vali, valj);
                                if (!interactions.containsFloat(valj, vali)) {
                                    interactions.addInteraction(population.getObjectByValue(valj), population.getObjectByValue(vali));
                                }
                                interactions.incrementPairVolume(valj, vali);
                            }
                        }
                    });
        }

        return interactions;
    }
}
