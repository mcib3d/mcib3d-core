package mcib3d.geom;

import mcib3d.geom2.VoxelInt;

public class Point3DInt {
    private int x, y, z;

    public Point3DInt(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3DInt() {
        x = 0;
        y = 0;
        z = 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setZ(int z) {
        this.z = z;
    }

    public void setXYZ(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public void translate(int tx, int ty, int tz) {
        x += tx;
        y += ty;
        z += tz;
    }

    public double distanceSquare(Voxel3D voxel3D) {
        return (x - voxel3D.getX()) * (x - voxel3D.getX()) + (y - voxel3D.getY()) * (y - voxel3D.getY()) + (z - voxel3D.getZ()) * (z - voxel3D.getZ());
    }

    public double distanceSquare(VoxelInt voxel3D) {
        return (x - voxel3D.getX()) * (x - voxel3D.getX()) + (y - voxel3D.getY()) * (y - voxel3D.getY()) + (z - voxel3D.getZ()) * (z - voxel3D.getZ());
    }

    public double distance(Voxel3D voxel3D) {
        return Math.sqrt(distanceSquare(voxel3D));
    }

    public double distance(VoxelInt voxel3D) {
        return Math.sqrt(distanceSquare(voxel3D));
    }


    public double distanceSquareScaled(Voxel3D voxel3D, double xy, double xz) {
        return (x - voxel3D.getX()) * (x - voxel3D.getX()) * xy * xy + (y - voxel3D.getY()) * (y - voxel3D.getY()) * xy * xy + (z - voxel3D.getZ()) * (z - voxel3D.getZ()) * xz * xz;
    }

    public double distanceSquareScaled(VoxelInt voxel3D, double xy, double xz) {
        return (x - voxel3D.getX()) * (x - voxel3D.getX()) * xy * xy + (y - voxel3D.getY()) * (y - voxel3D.getY()) * xy * xy + (z - voxel3D.getZ()) * (z - voxel3D.getZ()) * xz * xz;
    }

    public double distanceUnitScaled(Voxel3D voxel3D, double xy, double xz) {
        return Math.sqrt(distanceSquareScaled(voxel3D, xy, xz));
    }

    public double distanceUnitScaled(VoxelInt voxel3D, double xy, double xz) {
        return Math.sqrt(distanceSquareScaled(voxel3D, xy, xz));
    }

    public boolean isInsideBoundingBox(int[] boundingBox) { //xmin, xmax, ymin, ymax, zmin, zmax
        return (x >= boundingBox[0] && x <= boundingBox[1] && y >= boundingBox[2] && y <= boundingBox[3] && z >= boundingBox[4] && z <= boundingBox[5]);
    }

    public boolean sameVoxel(Point3DInt other) {
        return (this.x == other.x) && (this.y == other.y) && (this.z == other.z);
    }
}
