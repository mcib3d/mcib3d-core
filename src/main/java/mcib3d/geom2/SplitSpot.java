package mcib3d.geom2;

import ij.IJ;
import mcib3d.geom.Voxel3D;
import mcib3d.geom.Voxel3DComparable;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.distanceMap3d.EDT;
import mcib3d.image3d.processing.FastFilters3D;
import mcib3d.image3d.regionGrowing.Watershed3D;

import java.util.List;


public class SplitSpot {
    Object3DInt object3DInt;
    double distMin = 10; // minimum distance between the two seeds

    public SplitSpot(Object3DInt object3DInt, double distMin) {
        this.object3DInt = object3DInt;
        this.distMin = distMin;
    }

    public Object3DInt[] splitObject(){
        ImageHandler seg = new Object3DIntLabelImage(object3DInt).getCroppedLabelImage(255);
        Object3DInt[] res = null;
        try {
            ImageFloat edt3d = EDT.run(seg,128,false,0);
            // 3D filtering of the edt to remove small local maxima
            ImageFloat edt3dseeds = FastFilters3D.filterFloatImage(edt3d, FastFilters3D.MEAN, 1, 1, 1, 0, false);
            List<Voxel3DComparable> locals = FastFilters3D.getListMaximaFloat(edt3dseeds,1,1,1,0,false);

            int nb = locals.size();
            // IJ.log("nb=" + nb);
            if (nb < 2) {
                return null;
            }

            // get two new seeds
            Voxel3D[] seeds = kmeans(locals);
            Voxel3D PP1 = seeds[0];
            Voxel3D PP2 = seeds[1];

            // check minimal distances
            double distPP = PP1.distance(PP2);
            IJ.log("Centers found for split PP1=" + PP1 + " PP2=" + PP2 + " distance " + distPP);
            if (distPP < distMin) {
                return null;
            }
            // find closest max local to two barycenters, in case barycenters outside object
            Voxel3D PP1closest = new Voxel3D(locals.get(0));
            Voxel3D PP2closest = new Voxel3D(locals.get(0));
            double dist1 = PP1.distanceSquare(PP1closest);
            double dist2 = PP2.distanceSquare(PP2closest);
            for (Voxel3D local : locals) {
                double dd1 = PP1.distanceSquare(local);
                double dd2 = PP2.distanceSquare(local);
                if (dd1 < dist1) {
                    dist1 = dd1;
                    PP1closest = local;
                }
                if (dd2 < dist2) {
                    dist2 = dd2;
                    PP2closest = local;
                }
            }


            ImageInt seedsImage = new ImageShort("seeds", seg.sizeX, seg.sizeY, seg.sizeZ);
            seedsImage.setPixel(PP1closest.getRoundX(), PP1closest.getRoundY(), PP1closest.getRoundZ(), 1);
            seedsImage.setPixel(PP2closest.getRoundX(), PP2closest.getRoundY(), PP2closest.getRoundZ(), 2);
            ImageHandler wat2 = new Watershed3D(edt3dseeds, seedsImage,0,0).getWatershedImage3D();
            // in watershed label starts at 1
            Object3DInt ob1 = new Object3DInt(wat2, 1);
            Object3DInt ob2 = new Object3DInt(wat2, 2);
            ob1.translate(seg.offsetX, seg.offsetY, seg.offsetZ);
            ob2.translate(seg.offsetX, seg.offsetY, seg.offsetZ);
            res = new Object3DInt[2];
            res[0] = ob1;
            res[1] = ob2;
        } catch (Exception e) {
            IJ.log("Exception EDT " + e);
        }

        return res;
    }

    private Voxel3D[] kmeans(List<Voxel3DComparable> locals){
        int nb = locals.size();
        // look for most far apart local maxima
        int i1 = 0, i2 = 0;
        double d2max = 0.0, dd2;
        for (int i = 0; i < nb; i++) {
            for (int j = i + 1; j < nb; j++) {
                dd2 = locals.get(i).distanceSquare(locals.get(j));
                if (dd2 > d2max) {
                    d2max = dd2;
                    i1 = i;
                    i2 = j;
                }
            }
        }

        // perform a 2-means clustering
        double cx1 = 0, cy1 = 0, cz1 = 0;
        double cx2 = 0, cy2 = 0, cz2 = 0;
        double d1, d2;

        Voxel3D PP1 = new Voxel3D(locals.get(i1).getX(), locals.get(i1).getY(), locals.get(i1).getZ(), 1);
        Voxel3D PP2 = new Voxel3D(locals.get(i2).getX(), locals.get(i2).getY(), locals.get(i2).getZ(), 2);
        Voxel3D P1 = new Voxel3D(cx1, cy1, cz1, 0);
        Voxel3D P2 = new Voxel3D(cx2, cy2, cz2, 0);
        int nb1, nb2;

        int nbite = 0;
        while ((nb > 2) && ((P1.distance(PP1) > 1) || (P2.distance(PP2) > 1)) && (nbite < 100)) {
            nbite++;
            cx1 = 0;
            cy1 = 0;
            cx2 = 0;
            cy2 = 0;
            cz1 = 0;
            cz2 = 0;
            nb1 = 0;
            nb2 = 0;
            P1.setX(PP1.getX());
            P1.setY(PP1.getY());
            P1.setZ(PP1.getZ());
            P2.setX(PP2.getX());
            P2.setY(PP2.getY());
            P2.setZ(PP2.getZ());
            for (Voxel3D local : locals) {
                d1 = P1.distance(local);
                d2 = P2.distance(local);
                if (d1 < d2) {
                    cx1 += local.getX();
                    cy1 += local.getY();
                    cz1 += local.getZ();
                    nb1++;
                } else {
                    cx2 += local.getX();
                    cy2 += local.getY();
                    cz2 += local.getZ();
                    nb2++;
                }
            }
            cx1 /= nb1;
            cy1 /= nb1;
            cx2 /= nb2;
            cy2 /= nb2;
            cz1 /= nb1;
            cz2 /= nb2;

            PP1.setX(cx1);
            PP1.setY(cy1);
            PP1.setZ(cz1);
            PP2.setX(cx2);
            PP2.setY(cy2);
            PP2.setZ(cz2);
        }

        return new Voxel3D[]{PP1, PP2};
    }
}
