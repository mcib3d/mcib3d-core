package mcib3d.geom2;

import mcib3d.image3d.ImageHandler;

import java.util.*;
import java.util.stream.IntStream;

/**
 * The type Object3DInt, for 3D Objects with 3D voxel using integer coordinates.
 * The 3D object is composed of a list of 2D planes.
 * Each plane is then a list of VoxelInt.
 */
public class Object3DInt {
    private final Map<Integer, Object3DPlane> object3DPlanes;
    private final List<Object3DPlane> object3DPlaneList; // FIXME is it needed ?
    private BoundingBox boundingBox = null;
    private float label;
    private double voxelSizeXY = 1.0;
    private double voxelSizeZ = 1.0;
    private String unit = "pix";
    private double size;

    private String name = "";
    private int type = 0;
    private String comment = "";
    private double compareValue;

    private float idObject = 0;

    /**
     * Instantiates a new empty Object3dInt with label 0.
     */
    public Object3DInt() {
        object3DPlanes = new HashMap<>();
        object3DPlaneList = new LinkedList<>();
        label = 0;
    }

    /**
     * Instantiates a new Object3dInt with a single Voxel.
     *
     * @param voxelInt the voxel
     */
    public Object3DInt(VoxelInt voxelInt) {
        object3DPlanes = new HashMap<>();
        object3DPlaneList = new LinkedList<>();
        label = voxelInt.getValue();
        addVoxel(voxelInt);
    }

    /**
     * Instantiates a new empty Object3dInt with a specific label.
     *
     * @param label the label
     */
    public Object3DInt(float label) {
        object3DPlanes = new HashMap<>();
        object3DPlaneList = new LinkedList<>();
        this.label = label;
    }

    /**
     * Instantiates a new Object3dInt from a segmented image.
     * It will extract the voxels having the specified label in the image.
     *
     * @param handler the segmented image
     * @param label   the label
     */
    public Object3DInt(ImageHandler handler, float label) {
        object3DPlanes = new HashMap<>();
        object3DPlaneList = new LinkedList<>();
        this.label = label;
        buildObject3DInt(handler);
    }

    /**
     * Instantiates a new Object3dint from an image.
     * Here we assume there is only one object in the image (with value >0).
     *
     * @param handler the handler
     */
    public Object3DInt(ImageHandler handler) {
        object3DPlanes = new HashMap<>();
        object3DPlaneList = new LinkedList<>();
        // assuming only one object in image >0
        this.label = handler.getMinAboveValue(0);
        buildObject3DInt(handler);
    }

    /**
     * Add a list of voxel to the 3D object.
     *
     * @param list the list of voxels.
     */
    public void addVoxels(List<VoxelInt> list) {
        // FIXME to optimize, gather by z
        list.forEach(this::addVoxel);
    }

    /**
     * Is the 3D object contains a voxel with same coordinates.
     *
     * @param voxelInt the voxel
     * @return True if contains a voxel with same coordinates.
     */
    public boolean contains(VoxelInt voxelInt) {
        // check bounding box
        if (!getBoundingBox().contains(voxelInt)) return false;
        // check plane
        return object3DPlanes.get(voxelInt.getZ()).contains(voxelInt);
    }

    /**
     * Add one voxel to the 3D object.
     *
     * @param voxelInt the voxel to add.
     */
    public void addVoxel(VoxelInt voxelInt) {
        Object3DPlane planeZ =  object3DPlanes.get(voxelInt.getZ());
        if (planeZ == null) {
            Object3DPlane plane = new Object3DPlane(voxelInt.getZ());
            plane.addVoxel(voxelInt);
            object3DPlanes.put(voxelInt.getZ(), plane);
            object3DPlaneList.add(plane);
        } else {
            planeZ.addVoxel(voxelInt);
        }
        size++;
    }

    private void buildObject3DInt(ImageHandler handler) {
        voxelSizeXY = handler.getVoxelSizeXY();
        voxelSizeZ = handler.getVoxelSizeZ();

        size = 0;
        for (int z = 0; z < handler.sizeZ; z++) {
            Object3DPlane plane = createVoxelsListPlane(handler, z);
            if (plane != null) {
                object3DPlanes.put(z, plane);
                object3DPlaneList.add(plane);
                size += plane.size();
            }
        }
        if ((handler.offsetX != 0) || (handler.offsetY != 0) || (handler.offsetZ != 0)) {
            translate(handler.offsetX, handler.offsetY, handler.offsetZ);
        }
    }

    /**
     * Add a 2D plane to the 3D object.
     * If the 2D plane is already present, will add the voxels to the existing plane.
     *
     * @param plane the plane to be added
     */
    public void addPlane(Object3DPlane plane) {
        int z = plane.getZPlane();
        if (object3DPlanes.containsKey(z)) // add voxels to existing z plane
            object3DPlanes.get(z).addVoxels(plane.getVoxels());
        else {
            object3DPlanes.put(z, plane);
            object3DPlaneList.add(plane);
        }
        size += plane.size();
    }

    /**
     * Gets the id of the 3D object.
     * To be used in conjunction with DB for instance.
     *
     * @return the id object
     */
    public float getIdObject() {
        return idObject;
    }

    /**
     * Sets the id of the 3D object.
     * To be used in conjunction with DB for instance.
     *
     * @param idObject the id object
     */
    public void setIdObject(float idObject) {
        this.idObject = idObject;
    }

    /**
     * Sets the id of the 3D object as a string.
     * To be used in conjunction with DB for instance.
     *
     * @param idObject the id object
     */
    public void setIdObjectString(String idObject) {
        this.idObject = Float.parseFloat(idObject);
    }

    /**
     * Gets the name of the 3D object.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the 3D object.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the type of the 3D object.
     * Used for classification for instance.
     *
     * @return the type
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the type of the 3D object.
     * Used for classification for instance.
     *
     * @param type the type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Gets comment.
     *
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets comment.
     *
     * @param comment the comment
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * Gets the value to be used when comparing 3D objects.
     *
     * @return the compare value
     */
    public double getCompareValue() {
        return compareValue;
    }

    /**
     * Sets the value to be used when comparing 3D objects.
     *
     * @param compareValue the compare value
     */
    public void setCompareValue(double compareValue) {
        this.compareValue = compareValue;
    }

    private void computeBoundingBox() {
        boundingBox = new BoundingBox();
        getObject3DPlanes().forEach(plane -> plane.adjustBounding(boundingBox));
    }

    /**
     * Return the size of the 3D objects as the number of voxels.
     *
     * @return the size (or volume in voxel units)
     */
    public double size() {
        return size;
    }

    private Object3DPlane createVoxelsListPlane(ImageHandler ima, int z) {
        List<VoxelInt> voxelsTmp = new LinkedList<>();
        IntStream.range(0, ima.sizeY)
                .forEach(j -> IntStream.range(0, ima.sizeX)
                        .filter(i -> ima.getPixel(i, j, z) == label)
                        .forEach(i -> voxelsTmp.add(new VoxelInt(i, j, z, label))));

        return (voxelsTmp.isEmpty()) ? null : new Object3DPlane(voxelsTmp, z);
    }

    /**
     * Draw object in an image.
     * It will use the label value of the object to draw.
     *
     * @param handler the image
     */
    public void drawObject(ImageHandler handler) {
        drawObject(handler, this.getLabel());
    }

    /**
     * Draw object in an image with specific value.
     *
     * @param handler the image
     * @param val     the value
     */
    public void drawObject(ImageHandler handler, float val) {
        object3DPlanes.values().forEach(object3DPlane -> object3DPlane.drawObject(handler, val));
    }

    /**
     * Draw object in an image, when image has an offset, with a specific value.
     *
     * @param handler the image
     * @param val     the value
     */
    public void drawObjectUsingOffset(ImageHandler handler, float val) {
        object3DPlanes.values().forEach(object3DPlane -> object3DPlane.drawObjectUsingOffset(handler, val));
    }

    /**
     * Draw object using translation, with a specific value.
     * Will translate the object then draw it.
     *
     * @param handler the image
     * @param tx      the translation value in x
     * @param ty      the translation value in y
     * @param tz      the translation value in z
     * @param val     the value
     */
    public void drawObjectTranslate(ImageHandler handler, int tx, int ty, int tz, float val) {
        object3DPlanes.values().forEach(object3DPlane -> object3DPlane.drawObjectTranslate(handler, tx, ty, tz, val));
    }

    /**
     * Translate the object.
     *
     * @param tx the translation value in x
     * @param ty the translation value in y
     * @param tz the translation value in z
     */
    public void translate(int tx, int ty, int tz) {
        object3DPlanes.values().forEach(plane -> plane.translate(tx, ty, tz, true));
        boundingBox = null; // need to recompute BB
    }

    /**
     * Gets the list of planes.
     *
     * @return the planes
     */
    public List<Object3DPlane> getObject3DPlanes() {
        return object3DPlaneList;
    }

    /**
     * Gets first voxel of the object.
     *
     * @return the first voxel
     */
    public VoxelInt getFirstVoxel() {
        return getObject3DPlanes().get(0).getVoxels().get(0);
    }

    /**
     * Get a random voxel from the 3D object.
     *
     * @return a voxel
     */
    public VoxelInt getRandomVoxel() {
        Random rand = new Random();
        List<Object3DPlane> planes = getObject3DPlanes();
        int nbPlanes = planes.size();
        int randInt = rand.nextInt(nbPlanes);
        // first choose random plane
        Object3DPlane plane = planes.get(randInt);
        // get randomVoxel for that plane
        int nbVoxels = plane.size();
        randInt = rand.nextInt(nbVoxels);

        return plane.getVoxels().get(randInt);
    }

    /**
     * Gets the voxel size in XY (resolution).
     *
     * @return the voxel size in XY
     */
    @Deprecated
    public double getResXY() {
        return voxelSizeXY;
    }

    /**
     * Gets the voxel size in XY.
     *
     * @return the voxel size in XY
     */
    public double getVoxelSizeXY() {
        return voxelSizeXY;
    }

    /**
     * Gets the voxel size in Z.
     *
     * @return the voxel size in Z
     */
    public double getVoxelSizeZ() {
        return voxelSizeZ;
    }

    /**
     * Gets the voxel size in Z (resolution).
     *
     * @return the voxel size in Z
     */
    @Deprecated
    public double getResZ() {
        return voxelSizeZ;
    }

    /**
     * Sets the voxel size in XY.
     *
     * @param resXY the voxel size in XY
     */
    public void setVoxelSizeXY(double resXY) {
        this.voxelSizeXY = resXY;
    }

    /**
     * Sets the voxel size in XY (resolution).
     *
     * @param resXY the voxel size in XY
     */
    @Deprecated
    public void setResXY(double resXY) {
        this.voxelSizeXY = resXY;
    }

    /**
     * Sets the voxel size in Z.
     *
     * @param resZ the voxel size in Z
     */
    public void setVoxelSizeZ(double resZ) {
        this.voxelSizeZ = resZ;
    }

    /**
     * Sets the voxel size in Z (resolution).
     *
     * @param resZ the voxel size in Z
     */
    @Deprecated
    public void setResZ(double resZ) {
        this.voxelSizeZ = resZ;
    }

    /**
     * Sets the unit for the voxelSize.
     *
     * @param unit the unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Sets the unit for the voxelSize.
     *
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets value of the 3D object.
     * Use Label instead.
     *
     * @param label the label
     */
    @Deprecated
    public void setValue(float label) {
        this.label = label;
    }

    /**
     * Gets value of the 3D object.
     * Use Label instead.
     *
     * @return the value
     */
    @Deprecated
    public float getValue() {
        return label;
    }

    /**
     * Sets the label value of the 3D object.
     *
     * @param label the label
     */
    public void setLabel(float label) {
        this.label = label;
    }

    /**
     * Sets the label value of the 3D object.
     *
     * @return the label
     */
    public float getLabel() {
        return label;
    }

    /**
     * Gets the bounding box of the 3D object.
     *
     * @return the bounding box
     */
    public BoundingBox getBoundingBox() {
        if (boundingBox == null) computeBoundingBox();

        return boundingBox;
    }

    @Override
    public String toString() {
        return "Object3DInt{" +
                "label=" + label +
                ", size=" + size +
                ", name='" + name + '\'' +
                '}';
    }
}
