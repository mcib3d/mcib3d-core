package mcib3d.geom2.measurementsPopulation;

import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;

import java.util.HashMap;

public class MeasurePopulationColocalisation extends MeasurePopulationAbstract{

    public MeasurePopulationColocalisation(Objects3DIntPopulation population1, Objects3DIntPopulation population2) {
        super(population1,population2);
        defaultValue = 0;
    }

    @Override
    protected void computePairsValues() {
        pairsValues = new HashMap<>();
        computeColocalisation();
    }

    private void computeColocalisation() {
        ImageHandler image1 = population1.drawImage();
        ImageHandler image2 = population2.drawImage();

        int Xmax = Math.min(image1.sizeX, image2.sizeX);
        int Ymax = Math.min(image1.sizeY, image2.sizeY);
        int Zmax = Math.min(image1.sizeZ, image2.sizeZ);

        for (int k = 0; k < Zmax; k++) {
            for (int x = 0; x < Xmax; x++) {
                for (int y = 0; y < Ymax; y++) {
                    float pix1 = image1.getPixel(x, y, k);
                    float pix2 = image2.getPixel(x, y, k);
                    if ((pix1 > 0) && (pix2 > 0)) {
                        incrementValuePair(pix1, pix2, 1);
                    }
                }
            }
        }
    }
}
