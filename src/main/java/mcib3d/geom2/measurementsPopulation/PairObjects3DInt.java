package mcib3d.geom2.measurementsPopulation;

import mcib3d.geom2.Object3DInt;

public class PairObjects3DInt {
    private final Object3DInt object3D1; //  first object,  from population 1
    private final Object3DInt object3D2; // second object,  from population 2
    private final double valueObject1; // store any value, by default number of voxels
    private final double valueObject2;
    private double pairValue; // value from the pair

    public PairObjects3DInt(Object3DInt object3D1, Object3DInt object3D2) {
        this.object3D1 = object3D1;
        this.object3D2 = object3D2;
        this.pairValue = 0;
        valueObject1 = object3D1.size();
        valueObject2 = object3D2.size();
    }

    public PairObjects3DInt(Object3DInt object3D1, Object3DInt object3D2, double volume) {
        this.object3D1 = object3D1;
        this.object3D2 = object3D2;
        this.pairValue = volume;
        valueObject1 = object3D1.size();
        valueObject2 = object3D2.size();
    }


    public Object3DInt getObject3D1() {
        return object3D1;
    }

    public Object3DInt getObject3D2() {
        return object3D2;
    }

    public double getPairValue() {
        return pairValue;
    }

    public double getValueObject1() {
        return valueObject1;
    }

    public double getValueObject2() {
        return valueObject2;
    }

    public void incrementValuePair() {
        pairValue++;
    }

    public void incrementValuePair(double increment) {
        pairValue += increment;
    }
}
