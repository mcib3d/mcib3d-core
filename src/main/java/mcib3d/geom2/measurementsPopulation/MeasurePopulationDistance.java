package mcib3d.geom2.measurementsPopulation;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.Measure2Distance;
import mcib3d.geom2.measurements.MeasureDistancesCenter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MeasurePopulationDistance extends MeasurePopulationAbstract {
    // to remove, only use Measure2Distance static names
    public final static String DIST_CC_PIX = Measure2Distance.DIST_CC_PIX;
    public final static String DIST_CC_UNIT = Measure2Distance.DIST_CC_UNIT;
    public final static String DIST_CB_PIX = Measure2Distance.DIST_CB_PIX;
    public final static String DIST_CB_UNIT = Measure2Distance.DIST_CB_UNIT;
    public final static String DIST_BB_PIX = Measure2Distance.DIST_BB_PIX;
    public final static String DIST_BB_UNIT = Measure2Distance.DIST_BB_UNIT;

    private String measurement = DIST_CC_PIX;
    private double distMax = Double.POSITIVE_INFINITY;

    public MeasurePopulationDistance(Objects3DIntPopulation population1, Objects3DIntPopulation population2) {
        super(population1, population2);
    }

    public MeasurePopulationDistance(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distance) {
        super(population1, population2);
        distMax = distance;
    }

    public MeasurePopulationDistance(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distance, String method) {
        super(population1, population2);
        distMax = distance;
        measurement = method;
    }

    public void setDistanceMax(double distMax) {
        pairsValues = null;
        this.distMax = distMax;
    }

    public String getMeasurementMethod() {
        return measurement;
    }

    public void setMeasurementMethod(String measurement) {
        pairsValues = null;
        this.measurement = measurement;
    }

    @Override
    protected void computePairsValues() {
        pairsValues = new HashMap<>();
        if (measurement.equals(DIST_CC_PIX) || measurement.equals(DIST_CC_UNIT) || distMax == Double.POSITIVE_INFINITY)
            computeDistancesAll();
        else if (measurement.equals(DIST_BB_PIX)) computeDistancesBB(false);
        else if (measurement.equals(DIST_BB_UNIT)) computeDistancesBB(true);
        else if (measurement.equals(DIST_CB_PIX)) computeDistancesCB(false);
        else if (measurement.equals(DIST_CB_UNIT)) computeDistancesBB(true);
    }

    private void computeDistancesAll() {
        List<Object3DInt> pop1 = population1.getObjects3DInt();
        List<Object3DInt> pop2 = population2.getObjects3DInt();

        pop1.stream().flatMap(obj1 -> pop2.stream()
                        .map(obj2 -> {
                            Measure2Distance distance = new Measure2Distance(obj1, obj2);
                            return new PairObjects3DInt(obj1, obj2, distance.getValueMeasurement(measurement));
                        }))
                .filter(pair -> pair.getPairValue() < distMax)
                .forEach(pair -> pairsValues.put(pair.getObject3D1().getLabel() + "-" + pair.getObject3D2().getLabel(), pair));
    }

    private void computeDistancesBB(boolean useUnit) {
        // get max DC from both populations
        final Map<Object3DInt, Double> DCmax1 = new HashMap<>();
        final Map<Object3DInt, Double> DCmax2 = new HashMap<>();
        population1.getObjects3DInt().forEach(obj -> {
            MeasureDistancesCenter dc = new MeasureDistancesCenter(obj);
            double dcmax;
            if (useUnit) dcmax = dc.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_UNIT);
            else dcmax = dc.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_PIX);
            if (Double.isNaN(dcmax)) DCmax1.put(obj, Double.POSITIVE_INFINITY);
            else DCmax1.put(obj, dcmax);
        });
        population2.getObjects3DInt().forEach(obj -> {
            MeasureDistancesCenter dc = new MeasureDistancesCenter(obj);
            double dcmax;
            if (useUnit) dcmax = dc.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_UNIT);
            else dcmax = dc.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_PIX);
            if (Double.isNaN(dcmax)) DCmax2.put(obj, Double.POSITIVE_INFINITY);
            else DCmax2.put(obj, dcmax);
        });
        // get BB distance if DistCC - DCmax1 - DCmax2 < distMax
        List<Object3DInt> pop1 = population1.getObjects3DInt();
        List<Object3DInt> pop2 = population2.getObjects3DInt();
        pop1.stream().flatMap(obj1 -> pop2.stream()
                        .map(obj2 -> {
                            double distCC;
                            double distBB;
                            Measure2Distance distance = new Measure2Distance(obj1, obj2);
                            if (useUnit)
                                distCC = distance.getValueMeasurement(Measure2Distance.DIST_CC_UNIT);
                            else
                                distCC = distance.getValueMeasurement(Measure2Distance.DIST_CC_PIX);
                            if (distCC - DCmax1.get(obj1) - DCmax2.get(obj2) < distMax) {
                                if (useUnit)
                                    distBB = distance.getValueMeasurement(Measure2Distance.DIST_BB_UNIT);
                                else
                                    distBB = distance.getValueMeasurement(Measure2Distance.DIST_BB_PIX);
                                return new PairObjects3DInt(obj1, obj2, distBB);
                            }
                            return new PairObjects3DInt(obj1, obj2, distMax);
                        }))
                .filter(pair -> pair.getPairValue() < distMax)
                .forEach(pair -> pairsValues.put(pair.getObject3D1().getLabel() + "-" + pair.getObject3D2().getLabel(), pair));
    }

    private void computeDistancesCB(boolean useUnit) {
        // get max DC from both populations
        final Map<Object3DInt, Double> DCmax2 = new HashMap<>();
        population2.getObjects3DInt().forEach(obj -> {
            MeasureDistancesCenter dc = new MeasureDistancesCenter(obj);
            if (useUnit)
                DCmax2.put(obj, dc.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_UNIT));
            else
                DCmax2.put(obj, dc.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_PIX));
        });
        // get CB distance if DistCC - DCmax2 < distMax
        List<Object3DInt> pop1 = population1.getObjects3DInt();
        List<Object3DInt> pop2 = population2.getObjects3DInt();
        pop1.stream().flatMap(obj1 -> pop2.stream()
                        .map(obj2 -> {
                            double distCC;
                            double distCB;
                            Measure2Distance distance = new Measure2Distance(obj1, obj2);
                            if (useUnit)
                                distCC = distance.getValueMeasurement(Measure2Distance.DIST_CC_UNIT);
                            else
                                distCC = distance.getValueMeasurement(Measure2Distance.DIST_CC_PIX);
                            if (distCC - DCmax2.get(obj2) < distMax) {
                                if (useUnit)
                                    distCB = distance.getValueMeasurement(Measure2Distance.DIST_CB_UNIT);
                                else
                                    distCB = distance.getValueMeasurement(Measure2Distance.DIST_CB_PIX);
                                if (distCB < distMax)
                                    return new PairObjects3DInt(obj1, obj2, distCB);
                            }
                            return new PairObjects3DInt(obj1, obj2, distMax);
                        }))
                .filter(pair -> pair.getPairValue() < distMax)
                .forEach(pair -> pairsValues.put(pair.getObject3D1().getLabel() + "-" + pair.getObject3D2().getLabel(), pair));
    }
}
