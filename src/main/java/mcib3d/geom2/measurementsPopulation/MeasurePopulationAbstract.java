package mcib3d.geom2.measurementsPopulation;

import ij.IJ;
import ij.measure.ResultsTable;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public abstract class MeasurePopulationAbstract {
    private final static String LABEL = "LabelObj";
    protected final Objects3DIntPopulation population1;
    protected final Objects3DIntPopulation population2;
    protected Map<String, PairObjects3DInt> pairsValues = null;
    protected double defaultValue = Double.NaN;

    public MeasurePopulationAbstract(Objects3DIntPopulation population1, Objects3DIntPopulation population2) {
        this.population1 = population1;
        this.population2 = population2;
    }

    protected abstract void computePairsValues();

    protected void incrementValuePair(float val1, float val2, double increment) {
        String key = val1 + "-" + val2;

        if (!pairsValues.containsKey(key))
            pairsValues.put(key, new PairObjects3DInt(population1.getObjectByLabel(val1), population2.getObjectByLabel(val2), 0));
        pairsValues.get(key).incrementValuePair(increment);
    }

    public double[] getValuesObject1String(String value){
        return getValuesObject1(Float.parseFloat(value));
    }

    public double[] getValuesObject1(float label) {
        if (pairsValues == null) computePairsValues();

        List<PairObjects3DInt> pairsObject1 = getPairsObject1(label);
        double[] values = new double[pairsObject1.size() * 3];

        AtomicInteger i = new AtomicInteger(0);

        pairsObject1.forEach(pair -> {
            values[i.getAndIncrement()] = pair.getObject3D1().getLabel();
            values[i.getAndIncrement()] = pair.getObject3D2().getLabel();
            values[i.getAndIncrement()] = pair.getPairValue();
        });

        return values;
    }

    public double[] getValuesObject1Sorted(float label, boolean ascending) {
        if (pairsValues == null) computePairsValues();

        List<PairObjects3DInt> pairsObject1 = getPairsObject1(label);
        double[] values = new double[pairsObject1.size() * 3];

        AtomicInteger i = new AtomicInteger(0);

        pairsObject1.stream()
                .sorted((p1, p2) -> {
                    if (ascending) return (int) Math.signum(p1.getPairValue() - p2.getPairValue());
                    else return (int) Math.signum(p2.getPairValue() - p1.getPairValue());
                })
                .forEach(pair -> {
                    values[i.getAndIncrement()] = pair.getObject3D1().getLabel();
                    values[i.getAndIncrement()] = pair.getObject3D2().getLabel();
                    values[i.getAndIncrement()] = pair.getPairValue();
                });

        return values;
    }

    public double[] getValuesObject2(float label) {
        if (pairsValues == null) computePairsValues();

        List<PairObjects3DInt> pairsObject2 = getPairsObject2(label);
        double[] values = new double[pairsObject2.size() * 3];

        AtomicInteger i = new AtomicInteger(0);

        pairsObject2.forEach(pair -> {
            values[i.getAndIncrement()] = pair.getObject3D2().getLabel();
            values[i.getAndIncrement()] = pair.getObject3D1().getLabel();
            values[i.getAndIncrement()] = pair.getPairValue();
        });

        return values;
    }

    public List<PairObjects3DInt> getPairsObject1(float value){
        return getPairsObject1(value, true);
    }

    public List<PairObjects3DInt> getPairsObject1(float label, boolean ascending) {
        if (pairsValues == null) computePairsValues();

        return pairsValues.keySet().stream().filter(s -> s.startsWith(label + "-"))
                .map(s -> new PairObjects3DInt(pairsValues.get(s).getObject3D1(), pairsValues.get(s).getObject3D2(), pairsValues.get(s).getPairValue()))
                .sorted((p1, p2) -> {
                    if (ascending) return (int) Math.signum(p1.getPairValue() - p2.getPairValue());
                    else return (int) Math.signum(p2.getPairValue() - p1.getPairValue());
                })
                .collect(Collectors.toList());
    }

    public List<PairObjects3DInt> getAllPairs(){
        return getAllPairs(true);
    }

    public List<PairObjects3DInt> getAllPairs(boolean ascending){
        if (pairsValues == null) computePairsValues();

        return pairsValues.keySet().stream()
        .map(s -> new PairObjects3DInt(pairsValues.get(s).getObject3D1(), pairsValues.get(s).getObject3D2(), pairsValues.get(s).getPairValue()))
                .sorted((p1, p2) -> {
                    if (ascending) return (int) Math.signum(p1.getPairValue() - p2.getPairValue());
                    else return (int) Math.signum(p2.getPairValue() - p1.getPairValue());
                })
                .collect(Collectors.toList());
    }

    public List<PairObjects3DInt> getPairsObject2(float label) {
        if (pairsValues == null) computePairsValues();

        return pairsValues.keySet().stream().filter(s -> s.endsWith("-" + label))
                .map(s -> new PairObjects3DInt(pairsValues.get(s).getObject3D1(), pairsValues.get(s).getObject3D2(), pairsValues.get(s).getPairValue()))
                .collect(Collectors.toList());
    }

    public double getValueObjectsPair(Object3DInt object3DInt1, Object3DInt object3DInt2) {
        if (pairsValues == null) computePairsValues();

        PairObjects3DInt pair = pairsValues.get(object3DInt1.getLabel() + "-" + object3DInt2.getLabel());
        if (pair != null)
            return pair.getPairValue();
        else
            return defaultValue;
    }

    public double getValueObjectsPair(float val1, float val2) {
        if (pairsValues == null) computePairsValues();

        PairObjects3DInt pair = pairsValues.get(val1 + "-" + val2);
        if (pair != null)
            return pair.getPairValue();
        else
            return defaultValue;
    }

    public ResultsTable getResultsTableOnlyColoc(){
        return getResultsTableOnlyColoc(true);
    }

    public ResultsTable getResultsTableOnlyColoc(boolean ascending) {
        if (pairsValues == null) computePairsValues();
        IJ.log("Interactions completed (" + pairsValues.size() + " interactions found), building results table");
        ResultsTable rt0 = ResultsTable.getResultsTable();
        if (rt0 == null) rt0 = new ResultsTable();
        rt0.reset();

        final ResultsTable rt = rt0;

        AtomicInteger ai = new AtomicInteger(0);
        population1.getObjects3DInt().forEach(object -> {
            rt.incrementCounter();
            rt.setValue(LABEL, ai.get(), object.getLabel());

            List<PairObjects3DInt> list1 = getPairsObject1(object.getLabel(), ascending);

            if ((list1.size() == 0) ) {
                rt.setValue("O1", ai.get(), 0);
                rt.setValue("V1", ai.get(), 0);
            }
            AtomicInteger c = new AtomicInteger(0);
            list1.forEach(P -> {
                if (P.getObject3D1().getLabel() != object.getLabel())
                    IJ.log("Pb object1 : " + object.getLabel() + " / " + P.getObject3D1().getLabel());
                Object3DInt object2 = P.getObject3D2();
                rt.setValue("O" + (c.get() + 1), ai.get(), object2.getLabel());
                rt.setValue("V" + (c.get() + 1), ai.get(), P.getPairValue());
                c.getAndIncrement();
            });
            ai.getAndIncrement();
        });
        rt.sort(LABEL);

        return rt;
    }
}
