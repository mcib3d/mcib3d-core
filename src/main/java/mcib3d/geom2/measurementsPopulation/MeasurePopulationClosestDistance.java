package mcib3d.geom2.measurementsPopulation;

import mcib3d.geom.Point3D;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureCentroid;
import mcib3d.utils.KDTreeC;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

public class MeasurePopulationClosestDistance extends MeasurePopulationAbstract {
    public final static String CLOSEST_CC1_UNIT = "Closest1CenterUnit";
    public final static String CLOSEST_CC2_UNIT = "Closest2CenterUnit";
    public final static String CLOSEST_CC1_PIX = "Closest1CenterPix";
    public final static String CLOSEST_CC2_PIX = "Closest2CenterPix";
    public final static String CLOSEST_BB1_UNIT = "Closest1BorderUnit";
    public final static String CLOSEST_BB2_UNIT = "Closest2BorderUnit";
    public final static String CLOSEST_BB1_PIX = "Closest1BorderPix";
    public final static String CLOSEST_BB2_PIX = "Closest2BorderPix";
    private String measurement = CLOSEST_CC1_UNIT;
    private double distMax = Double.POSITIVE_INFINITY;
    private boolean removeZeroDistance = true;

    public MeasurePopulationClosestDistance(Objects3DIntPopulation population1, Objects3DIntPopulation population2) {
        super(population1, population2);
    }

    public MeasurePopulationClosestDistance(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distance) {
        super(population1, population2);
        distMax = distance;
    }

    public MeasurePopulationClosestDistance(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distance, String method) {
        super(population1, population2);
        distMax = distance;
        measurement = method;
    }

    public void setClosestMethod(String method) {
        measurement = method;
    }

    public void setDistanceMax(double distMax) {
        this.distMax = distMax;
    }

    public void setRemoveZeroDistance(boolean removeZeroDistance) {
        this.removeZeroDistance = removeZeroDistance;
    }

    @Override
    protected void computePairsValues() {
        pairsValues = new HashMap<>();

        switch (measurement) {
            case CLOSEST_CC1_UNIT:
                computeClosestCenter(1, true);
                break;
            case CLOSEST_CC2_UNIT:
                computeClosestCenter(2, true);
                break;
            case CLOSEST_CC1_PIX:
                computeClosestCenter(1, false);
                break;
            case CLOSEST_CC2_PIX:
                computeClosestCenter(2, false);
                break;
            case CLOSEST_BB1_UNIT:
                computeClosestBorder(1, true);
                break;
            case CLOSEST_BB2_UNIT:
                computeClosestBorder(2, true);
                break;
            case CLOSEST_BB1_PIX:
                computeClosestBorder(1, false);
                break;
            case CLOSEST_BB2_PIX:
                computeClosestBorder(2, false);
                break;
        }
    }

    // Distance in pixel unit
    private void computeClosestCenter(int nb, boolean useUnit) {
        List<Object3DInt> pop1 = population1.getObjects3DInt();
        List<Object3DInt> pop2 = population2.getObjects3DInt();

        // calibration
        double resXY = 1.0;
        double resZ = 1.0;
        if (useUnit) {
            resXY = population1.getFirstObject().getVoxelSizeXY();
            resZ = population1.getFirstObject().getVoxelSizeZ();
        }

        // KD-tree for population2
        KDTreeC kdTreeC = new KDTreeC(3);
        kdTreeC.setScale3(resXY, resXY, resZ);
        pop2.forEach(obj -> {
            MeasureCentroid centroid = new MeasureCentroid(obj);
            Point3D point = centroid.getCentroidAsPoint();
            kdTreeC.add(new double[]{point.x, point.y, point.z}, obj);
        });
        // get nearest 2 for objects population 1
        pop1.forEach(object3DInt -> {
            MeasureCentroid centroid = new MeasureCentroid(object3DInt);
            Point3D point = centroid.getCentroidAsPoint();
            KDTreeC.Item[] closest = kdTreeC.getNearestNeighbor(new double[]{point.x, point.y, point.z}, 2);
            // first closest
            Object3DInt closest1 = (Object3DInt) closest[0].obj;
            double dist = Math.sqrt(closest[0].distanceSq);
            if ((dist > 0) || (!removeZeroDistance))
                if (dist < distMax)
                    pairsValues.put(object3DInt.getLabel() + "-" + closest1.getLabel(), new PairObjects3DInt(object3DInt, closest1, dist));
            // second closest
            if (nb == 2) {
                Object3DInt closest2 = (Object3DInt) closest[1].obj;
                dist = Math.sqrt(closest[1].distanceSq);
                if ((dist > 0) || (!removeZeroDistance))
                    if (dist < distMax)
                        pairsValues.put(object3DInt.getLabel() + "-" + closest2.getLabel(), new PairObjects3DInt(object3DInt, closest2, dist));
            }
        });
    }

    private void computeClosestBorder(int nb, boolean useUnit) {
        // need actually to compute all BB distances ( <distmax )
        final MeasurePopulationDistance distance = new MeasurePopulationDistance(population1, population2, distMax);
        if (useUnit)
            distance.setMeasurementMethod(MeasurePopulationDistance.DIST_BB_UNIT);
        else
            distance.setMeasurementMethod(MeasurePopulationDistance.DIST_BB_PIX);
        // sort then extract first 1 or 2
        population1.getObjects3DInt().forEach(obj -> {
            List<PairObjects3DInt> BBOjects = distance.getPairsObject1(obj.getLabel());
            if ((BBOjects != null) && (!BBOjects.isEmpty())) {
                BBOjects.sort(Comparator.comparingDouble(PairObjects3DInt::getPairValue));
                // first distance
                PairObjects3DInt pair = BBOjects.get(0);
                double dist = pair.getPairValue();
                if ((dist > 0) || (!removeZeroDistance))
                    if (dist < distMax)
                        pairsValues.put(pair.getObject3D1().getLabel() + "-" + pair.getObject3D2().getLabel(), pair);
                if ((nb == 2) && (BBOjects.size() > 1)) {
                    pair = BBOjects.get(1);
                    dist = pair.getPairValue();
                    if ((dist > 0) || (!removeZeroDistance))
                        if (dist < distMax)
                            pairsValues.put(pair.getObject3D1().getLabel() + "-" + pair.getObject3D2().getLabel(), pair);
                }
            }
        });
    }
}
