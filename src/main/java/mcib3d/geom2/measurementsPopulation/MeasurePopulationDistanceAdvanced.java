package mcib3d.geom2.measurementsPopulation;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.Measure2Distance;
import mcib3d.image3d.ImageHandler;

import java.util.HashMap;
import java.util.List;

import ij.IJ;
import ij.ImagePlus;

public class MeasurePopulationDistanceAdvanced extends MeasurePopulationAbstract {
    // to remove, only use Measure2Distance static names
    public final static String DIST_HAUSDORFF_PIX = Measure2Distance.DIST_HAUSDORFF_PIX;
    public final static String DIST_HAUSDORFF_UNIT = Measure2Distance.DIST_HAUSDORFF_UNIT;
    public final static String DIST_MEANMINDIST_PIX = Measure2Distance.DIST_MEANMINDIST_PIX;
    public final static String DIST_MEANMINDIST_UNIT = Measure2Distance.DIST_MEANMINDIST_UNIT;

    private String measurement = DIST_HAUSDORFF_PIX;
    private double distMax = Double.POSITIVE_INFINITY;

    public MeasurePopulationDistanceAdvanced(Objects3DIntPopulation population1, Objects3DIntPopulation population2) {
        super(population1, population2);
    }

    public MeasurePopulationDistanceAdvanced(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distance) {
        super(population1, population2);
        distMax = distance;
    }

    public MeasurePopulationDistanceAdvanced(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distance, String method) {
        super(population1, population2);
        distMax = distance;
        measurement = method;
    }

    public void setDistanceMax(double distMax) {
        pairsValues = null;
        this.distMax = distMax;
    }

    public String getMeasurementMethod() {
        return measurement;
    }

    public void setMeasurementMethod(String measurement) {
        pairsValues = null;
        this.measurement = measurement;
    }

    @Override
    protected void computePairsValues() {
        pairsValues = new HashMap<>();
        computeDistancesAll();
    }

    private void computeDistancesAll() {
        List<Object3DInt> pop1 = population1.getObjects3DInt();
        List<Object3DInt> pop2 = population2.getObjects3DInt();

        pop1.stream().flatMap(obj1 -> pop2.stream()
                        .map(obj2 -> {
                            Measure2Distance distance = new Measure2Distance(obj1, obj2);
                            return new PairObjects3DInt(obj1, obj2, distance.getValueMeasurement(measurement));
                        }))
                .filter(pair -> pair.getPairValue() < distMax)
                .forEach(pair -> pairsValues.put(pair.getObject3D1().getLabel() + "-" + pair.getObject3D2().getLabel(), pair));
    }

    public static void main(String[] args) {
        ImagePlus plus = IJ.openImage("/home/thomas/AMU/DATA/Misc/cells.tif");
        ImageHandler handler = ImageHandler.wrap(plus);
        Objects3DIntPopulation pop1 = new Objects3DIntPopulation(handler);
        Objects3DIntPopulation pop2 = new Objects3DIntPopulation(handler);
        // one objet
        Object3DInt obj1 = pop1.getObjectByLabel(1);
        Object3DInt obj2 = pop2.getObjectByLabel(2);
        Measure2Distance distance = new Measure2Distance(obj1, obj2);
        System.out.println("dist "+distance.getValueMeasurement(DIST_MEANMINDIST_UNIT));

        MeasurePopulationDistanceAdvanced advanced = new MeasurePopulationDistanceAdvanced(pop1, pop2);
        advanced.setMeasurementMethod(DIST_MEANMINDIST_PIX);
        advanced.getResultsTableOnlyColoc().show("TEST");
    }
}
