package mcib3d.geom2;

import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.image3d.ImageHandler;

public class Objects3DIntPopulationComputation {
    Objects3DIntPopulation population;

    public Objects3DIntPopulationComputation(Objects3DIntPopulation population) {
        this.population = population;
    }

    public Objects3DIntPopulation getPopulation() {
        return population;
    }

    public void setPopulation(Objects3DIntPopulation population) {
        this.population = population;
    }

    public Objects3DIntPopulation getPopulationCopy() {
        Objects3DIntPopulation population1 = new Objects3DIntPopulation();

        getPopulation().getObjects3DInt().forEach(population1::addObject);

        population1.setVoxelSizeXY(getPopulation().getVoxelSizeXY());
        population1.setVoxelSizeZ(getPopulation().getVoxelSizeZ());
        population1.setName(getPopulation().getName());

        return population1;
    }

    public Objects3DIntPopulation getExcludeBorders(ImageHandler img, boolean Z) {
        Objects3DIntPopulation population1 = new Objects3DIntPopulation();

        getPopulation().getObjects3DInt().stream()
                .filter(O -> !new Object3DComputation(O).touchBorders(img, Z))
                .forEach(population1::addObject);

        population1.setVoxelSizeXY(getPopulation().getVoxelSizeXY());
        population1.setVoxelSizeZ(getPopulation().getVoxelSizeZ());
        population1.setName(getPopulation().getName() + "_ExcludeBorders");

        return population1;
    }

    public Objects3DIntPopulation getFilterSize(double minSize, double maxSize) {
        Objects3DIntPopulation population1 = new Objects3DIntPopulation();

        getPopulation().getObjects3DInt().stream()
                .filter(O -> (O.size() >= minSize) && (O.size() <= maxSize))
                .forEach(population1::addObject);

        population1.setVoxelSizeXY(getPopulation().getVoxelSizeXY());
        population1.setVoxelSizeZ(getPopulation().getVoxelSizeZ());
        population1.setName(getPopulation().getName() + "_FilterSize");

        return population1;
    }

    public Objects3DIntPopulation getFilterMeasurement(double minValue, double maxValue, boolean keep, String measure) {
        Objects3DIntPopulation population1 = new Objects3DIntPopulation();

        getPopulation().getObjects3DInt().stream()
                .filter(O -> {
                    double value = new MeasureObject(O).measure(measure);
                    if (keep) {
                        return ((value >= minValue) && (value <= maxValue));
                    } else {
                        return ((value < minValue) || (value > maxValue));
                    }
                })
                .forEach(population1::addObject);

        population1.setVoxelSizeXY(getPopulation().getVoxelSizeXY());
        population1.setVoxelSizeZ(getPopulation().getVoxelSizeZ());
        population1.setName(getPopulation().getName() + "_FilterSize");

        return population1;
    }

    public Objects3DIntPopulation getFilterMeasurementIntensity(ImageHandler image, double minValue, double maxValue, boolean keep, String measure) {
        Objects3DIntPopulation population1 = new Objects3DIntPopulation();

        getPopulation().getObjects3DInt().stream()
                .filter(O -> {
                    double value = new MeasureObject(O).measureIntensity(measure, image);
                    if (keep) {
                        return ((value >= minValue) && (value <= maxValue));
                    } else {
                        return ((value < minValue) || (value > maxValue));
                    }
                })
                .forEach(population1::addObject);

        population1.setVoxelSizeXY(getPopulation().getVoxelSizeXY());
        population1.setVoxelSizeZ(getPopulation().getVoxelSizeZ());
        population1.setName(getPopulation().getName() + "_FilterSize");

        return population1;
    }


}
