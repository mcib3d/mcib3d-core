package mcib3d.geom2;

import mcib3d.geom.Voxel3D;
import mcib3d.image3d.ImageHandler;

import java.io.BufferedWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.DoubleAdder;

/**
 * A class to store list of voxels,
 * used as a 2D plane for 3D object.
 * A 3D object is then a list of 2D planes.
 */
public class Object3DPlane {
    private final List<VoxelInt> voxels;
    private int zPlane;

    /**
     * Instantiates a new plane wit ha specified Z.
     *
     * @param z the z
     */
    public Object3DPlane(int z) {
        zPlane = z;
        voxels = new LinkedList<>();
    }

    /**
     * Instantiates a new plane from a list of voxels.
     * Do not check if the voxels have same Z value.
     *
     * @param voxel3DS the list of voxels
     * @param z        the z
     */
    public Object3DPlane(List<VoxelInt> voxel3DS, int z) {
        zPlane = z;
        voxels = new LinkedList<>();
        addVoxels(voxel3DS);
    }

    /**
     * Gets the Z value of the plane.
     *
     * @return the z value
     */
    public int getZPlane() {
        return zPlane;
    }

    /**
     * Add voxels to the plane.
     * Do not check if the voxels have same Z value.
     *
     * @param voxel3DS the list of voxels to add.
     */
    public void addVoxels(List<VoxelInt> voxel3DS) {
        voxels.addAll(voxel3DS);
    }

    /**
     * Add one voxel to the plane.
     * Do not check if the voxel has same Z value.
     *
     * @param voxel the voxel to add
     */
    public void addVoxel(VoxelInt voxel) {
        voxels.add(voxel);
    }

    /**
     * Checks if the plane contains any voxel.
     *
     * @return True if plane is empty
     */
    public boolean isEmpty() {
        return voxels.isEmpty();
    }

    /**
     * Draw the list of voxels of the plane in a given image.
     *
     * @param handler the image
     * @param val     the value to draw the voxels
     */
    public void drawObject(ImageHandler handler, float val) {
        voxels.stream().filter(handler::contains)
                .forEach(voxel -> handler.setPixel(voxel.getX(), voxel.getY(), voxel.getZ(), val));
    }

    /**
     * Draw the list of voxels of the plane in a given image, using offset.
     *
     * @param handler the image
     * @param val     the value to draw the voxels
     */
    public void drawObjectUsingOffset(ImageHandler handler, float val) {
        final int offX = handler.offsetX;
        final int offY = handler.offsetY;
        final int offZ = handler.offsetZ;

        voxels.stream().map(V -> new VoxelInt(V.getX() - offX, V.getY() - offY, V.getZ() - offZ, val))
                .filter(V -> handler.contains(V.getX(), V.getY(), V.getZ()))
                .forEach(V -> handler.setPixel(V.getX(), V.getY(), V.getZ(), val));
    }

    /**
     * Draw the list of voxels of the plane in a given image, after translating the voxels.
     *
     * @param handler the image
     * @param tx      the translation in X
     * @param ty      the translation in Y
     * @param tz      the translation in Z
     * @param val     the value to draw the voxels
     */
    public void drawObjectTranslate(ImageHandler handler, int tx, int ty, int tz, float val) {
        voxels.stream()
                .filter(V -> handler.contains(V.getX() + tx, V.getY() + ty, V.getZ() + tz))
                .forEach(V -> handler.setPixel(V.getX() + tx, V.getY() + ty, V.getZ() + tz, val));
    }

    /**
     * Return the number of voxels in this plane.
     *
     * @return the size of the plane.
     */
    public int size() {
        return voxels.size();
    }

    /**
     * Gets the sum of all coordinates as a voxel.
     * Used for centroid computation.
     *
     * @return the sum of all coordinates.
     */
    public Voxel3D getSumCoordinates() {
        return voxels.stream()
                .map(v -> new Voxel3D(v.getX(), v.getY(), v.getZ(), v.getValue()))
                .reduce(new Voxel3D(), (s, v) -> new Voxel3D(s.x + v.x, s.y + v.y, s.z + v.z, s.value));
    }

    /**
     * Within the plane, any value inside the image has value within the specified range.
     * The two values of range are inclusive.
     *
     * @param handler the image
     * @param t0      the minimal value of the range
     * @param t1      the maximal value of the range
     * @return True if at least one voxel has value within range
     */
    public boolean hasOneVoxelValueRange(ImageHandler handler, float t0, float t1) {
        for (VoxelInt vox : getVoxels()) {
            float pix = handler.getPixel(vox);
            if ((pix >= t0) && (pix <= t1)) return true;
        }

        return false;
    }

    /**
     * Within the plane, any value inside the image has value above specified value.
     *
     * @param handler the image
     * @param t0      the minimal value of the range
     * @return True if at least one voxel has value above specified value
     */
    public boolean hasOneVoxelValueAboveStrict(ImageHandler handler, float t0) {
        for (VoxelInt vox : getVoxels()) {
            float pix = handler.getPixel(vox);
            if (pix > t0) return true;
        }

        return false;
    }


    /**
     * Is the plane contains a voxel with same coordinates.
     *
     * @param voxelInt the voxel
     * @return True if contains a voxel with same coordinates.
     */
    public boolean contains(VoxelInt voxelInt) {
        return voxels.contains(voxelInt);
    }

    /**
     * Compute the moments order 2 of the 3D object.
     * It will use voxel coordinates. (No normalization).
     *
     * @param centroid the centroid of the 3D object
     * @return the moments order 2 (sum200, sum020, sum002, sum110, sum101, sum011)
     */
    public Double[] computeMoments2(Voxel3D centroid) {
        final DoubleAdder s200 = new DoubleAdder();
        final DoubleAdder s110 = new DoubleAdder();
        final DoubleAdder s020 = new DoubleAdder();
        final DoubleAdder s011 = new DoubleAdder();
        final DoubleAdder s101 = new DoubleAdder();
        final DoubleAdder s002 = new DoubleAdder();

        final double bx = centroid.getX(), by = centroid.getY(), bz = centroid.getZ();

        voxels.forEach(
                V -> {
                    double i = V.getX(), j = V.getY(), k = V.getZ();
                    s200.add((i - bx) * (i - bx));
                    s020.add((j - by) * (j - by));
                    s002.add((k - bz) * (k - bz));
                    s110.add((i - bx) * (j - by));
                    s101.add((i - bx) * (k - bz));
                    s011.add((j - by) * (k - bz));
                }
        );

        return new Double[]{s200.sum(), s020.sum(), s002.sum(), s110.sum(), s101.sum(), s011.sum()};
    }

    /**
     * Adjust bounding from another bounding box.
     * It will adjust the box to the maximal extend of the box + voxels of the plane.
     *
     * @param box the  bounding box
     */
    public void adjustBounding(BoundingBox box) {
        voxels.forEach(v -> box.adjustBounding(v.getX(), v.getY(), v.getZ()));
    }

    /**
     * Translate the voxels of the plane.
     * The Z value of the plane can also be adjusted.
     *
     * @param tx      the translation in X
     * @param ty      the translation in Y
     * @param tz      the translation in Z
     * @param updateZ do we adjust the Z value
     */
    public void translate(int tx, int ty, int tz, boolean updateZ) {
        voxels.forEach(V -> V.translate(tx, ty, tz));
        if (updateZ) zPlane += tz;
    }

    /**
     * Return the list of voxels from this plane.
     *
     * @return the voxels
     */
    public List<VoxelInt> getVoxels() {
        return voxels;
    }

    /**
     * Save the list of voxels in a file.
     * Will use the Object3DInt format.
     *
     * @param bf the bf
     */
    public void saveVoxels(BufferedWriter bf) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setGroupingUsed(false);
        nf.setMaximumFractionDigits(0);
        try {
            // save plane information
            bf.write("plane \t" + getZPlane() + "\n");
            for (VoxelInt voxelInt : voxels) {
                bf.write(nf.format(voxelInt.getX()) + "\t" + nf.format(voxelInt.getY()) + "\t" + nf.format(voxelInt.getZ()) + "\t" + voxelInt.getValue() + "\n");
            }
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    /**
     * Will update an histogram for an image with voxels from the plane.
     * THe histogram is a map of (Intensity value, number).
     *
     * @param ima  the image
     * @param hist the histogram
     */
    public void updateHist(ImageHandler ima, Map<Float, Integer> hist) {
        voxels.forEach(V -> {
            float val = ima.getPixel(V);
            hist.merge(val, 1, Integer::sum);
        });
    }

    /**
     * Will update an histogram for an image with voxels from the plane.
     * It will check if the pixel is inside the image.
     * THe histogram is a map of (Intensity value, number).
     *
     * @param ima  the image
     * @param hist the histogram
     */
    public void updateHistCheckVoxel(ImageHandler ima, Map<Float, Integer> hist) {
        voxels.forEach(V -> {
            if(ima.contains(V)){
                float val = ima.getPixel(V);
                hist.merge(val, 1, Integer::sum);
            }   
        });
    }



    /**
     * Within the plane, any value inside the image has value within the specified range.
     * Voxels coordinate values will be translated before checking.
     * The two values of range are inclusive.
     *
     * @param handler the image
     * @param t0      the minimal value of the range
     * @param t1      the maximal value of the range
     * @return True if at least one voxel has value within range
     */
    public boolean hasOneVoxelValueRangeTranslate(ImageHandler handler, float t0, float t1, int tx, int ty, int tz) {
        for (VoxelInt vox : getVoxels()) {
            int xx = vox.getX() + tx;
            int yy = vox.getY() + ty;
            int zz = vox.getZ() + tz;
            if (handler.contains(xx, yy, zz)) {
                float pix = handler.getPixel(xx, yy, zz);
                if ((pix >= t0) && (pix <= t1)) return true;
            }
        }

        return false;
    }
}
