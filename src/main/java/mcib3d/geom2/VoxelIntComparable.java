package mcib3d.geom2;

/**
 * The comparable version of VoxelInt
 */
public class VoxelIntComparable extends VoxelInt {
    private float label;
    private int compare;

    /**
     * Instantiates a new Voxel int comparable.
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param z     the z coordinate
     * @param value the value of the voxel
     * @param label the label of the voxel
     */
    public VoxelIntComparable(int x, int y, int z, float value, float label) {
        super(x, y, z, value);
        this.label = label;
    }

    /**
     * Gets label.
     *
     * @return the label
     */
    public float getLabel() {
        return label;
    }

    /**
     * Sets label.
     *
     * @param label the label
     */
    public void setLabel(float label) {
        this.label = label;
    }

    /**
     * Gets the value to be compared.
     *
     * @return the compare
     */
    public int getCompare() {
        return compare;
    }

    /**
     * Sets the value to be compared.
     *
     * @param compare the compare
     */
    public void setCompare(int compare) {
        this.compare = compare;
    }
}
