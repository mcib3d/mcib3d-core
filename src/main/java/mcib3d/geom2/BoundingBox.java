package mcib3d.geom2;

import mcib3d.geom.Point3DInt;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

/**
 * The type Bounding box, as a 3D brick.
 */
public class BoundingBox {
    /**
     * The minimum value in X.
     */
    public int xmin, /**
     * The minimum value in X.
     */
    ymin, /**
     * The minimum value in X.
     */
    zmin, /**
     * The maximum value in X.
     */
    xmax, /**
     * The maximum value in Y.
     */
    ymax, /**
     * The maximum value in Z.
     */
    zmax;

    /**
     * Instantiates a new Bounding box.
     * With values 0 and MAX_VALUE.
     */
    public BoundingBox() {
        xmin = Integer.MAX_VALUE;
        xmax = 0;
        ymin = Integer.MAX_VALUE;
        ymax = 0;
        zmin = Integer.MAX_VALUE;
        zmax = 0;
    }

    /**
     * Instantiates a new Bounding box from an image.
     * Will get the dimensions of the image.
     *
     * @param handler the image
     */
    public BoundingBox(ImageHandler handler) {
        xmin = handler.offsetX;
        xmax = xmin + handler.sizeX - 1;
        ymin = handler.offsetY;
        ymax = ymin + handler.sizeY - 1;
        zmin = handler.offsetZ;
        zmax = zmin + handler.sizeZ - 1;
    }

    public BoundingBox(BoundingBox other) {
        xmin = other.xmin;
        xmax = other.xmax;
        ymin = other.ymin;
        ymax = other.ymax;
        zmin = other.zmin;
        zmax = other.zmax;
    }

    /**
     * Instantiates a new Bounding box.
     *
     * @param xmin The minimum value in X.
     * @param xmax The maximum value in X.
     * @param ymin The minimum value in Y.
     * @param ymax The maximum value in Y.
     * @param zmin The minimum value in Z.
     * @param zmax The maximum value in Z.
     */
    public BoundingBox(int xmin, int xmax, int ymin, int ymax, int zmin, int zmax) {
        this.xmin = xmin;
        this.ymin = ymin;
        this.zmin = zmin;
        this.xmax = xmax;
        this.ymax = ymax;
        this.zmax = zmax;
    }

    /**
     * Sets the bounding values.
     *
     * @param xmin The minimum value in X.
     * @param xmax The maximum value in X.
     * @param ymin The minimum value in Y.
     * @param ymax The maximum value in Y.
     * @param zmin The minimum value in Z.
     * @param zmax The maximum value in Z.
     */
    public void setBounding(int xmin, int xmax, int ymin, int ymax, int zmin, int zmax) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.ymin = ymin;
        this.ymax = ymax;
        this.zmin = zmin;
        this.zmax = zmax;
    }

    public int[] getSizes() {
        int sx = xmax - xmin + 1;
        int sy = ymax - ymin + 1;
        int sz = zmax - zmin + 1;

        return new int[]{sx, sy, sz};
    }

    public ImageHandler createImage(boolean bit32) {
        int[] size = this.getSizes();
        ImageHandler image;
        if (bit32) image = new ImageFloat("coloc", size[0], size[1], size[2]);
        else image = new ImageShort("coloc", size[0], size[1], size[2]);
        image.setOffset(this.xmin, this.ymin, this.zmin);

        return image;
    }

    public ImageHandler createImage() {
       return createImage(false);
    }

    /**
     * Adjust the bounding box with a new point.
     * If the point is outside the box, the box will be extended up to the point.
     *
     * @param x the x coordinate of the point
     * @param y the y coordinate of the point
     * @param z the z coordinate of the point
     */
    public void adjustBounding(int x, int y, int z) {
        xmin = Math.min(xmin, x);
        xmax = Math.max(xmax, x);
        ymin = Math.min(ymin, y);
        ymax = Math.max(ymax, y);
        zmin = Math.min(zmin, z);
        zmax = Math.max(zmax, z);
    }

    /**
     * Adjust bounding with another bounding box.
     * It will adjust the box to the maximal extend of the two boxes.
     *
     * @param boundingBox the other bounding box
     */
    public void adjustBounding(BoundingBox boundingBox) {
        xmin = Math.min(xmin, boundingBox.xmin);
        xmax = Math.max(xmax, boundingBox.xmax);
        ymin = Math.min(ymin, boundingBox.ymin);
        ymax = Math.max(ymax, boundingBox.ymax);
        zmin = Math.min(zmin, boundingBox.zmin);
        zmax = Math.max(zmax, boundingBox.zmax);
    }

    public double volumePixel() {
        return (xmax - xmin + 1) * (ymax - ymin + 1) * (zmax - zmin + 1);
    }

    /**
     * Checks if the two boxes are separated (disjoint).
     * It will check if they do not intersect in any axes.
     *
     * @param box the other box
     * @return True if they are disjoint
     */
    public boolean disjoint(BoundingBox box) {
        int oxmin = box.xmin;
        int oxmax = box.xmax;
        int oymin = box.ymin;
        int oymax = box.ymax;
        int ozmin = box.zmin;
        int ozmax = box.zmax;

        boolean intersectX = ((xmax >= oxmin) && (oxmax >= xmin));
        boolean intersectY = ((ymax >= oymin) && (oymax >= ymin));
        boolean intersectZ = ((zmax >= ozmin) && (ozmax >= zmin));

        return !(intersectX && intersectY && intersectZ);
    }

    /**
     * Checks if the box contains another box (inside).
     *
     * @param box the other box
     * @return True if the other box is contained
     */
    public boolean contains(BoundingBox box) {
        if (xmin > box.xmin) return false;
        if (xmax < box.xmax) return false;
        if (ymin > box.ymin) return false;
        if (ymax < box.ymax) return false;
        if (zmin > box.zmin) return false;
        return zmax >= box.zmax;
    }

    /**
     * Checks if the box contains a point (inside).
     *
     * @param point the point
     * @return True if the point is contained
     */
    public boolean contains(Point3DInt point) {
        return contains(point.getX(), point.getY(), point.getZ());
    }

    public boolean contains(double x, double y, double z) {
        return containsExtend(x, y, z, 0, 0, 0);
    }

    public boolean containsExtend(double x, double y, double z, double rx, double ry, double rz) {
        return (x >= xmin - rx) && (x <= xmax + rx) && (y >= ymin - ry) && (y <= ymax + ry) && (z >= zmin - rz) && (z <= zmax + rz);
    }


    /**
     * Computes the union of the two boxes.
     * It will compute the maximal extend of the two boxes.
     *
     * @param box the other box
     * @return the union of the two boxes
     */
    public BoundingBox union(BoundingBox box) {
        int xmi = Math.min(xmin, box.xmin);
        int xma = Math.max(xmax, box.xmax);
        int ymi = Math.min(ymin, box.ymin);
        int yma = Math.max(ymax, box.ymax);
        int zmi = Math.min(zmin, box.zmin);
        int zma = Math.max(zmax, box.zmax);

        return new BoundingBox(xmi, xma, ymi, yma, zmi, zma);
    }

    public BoundingBox intersection(BoundingBox other) {
        if (disjoint(other)) return null;
        int ixmin, ixmax, iymin, iymax, izmin, izmax;

        // X
        ixmin = Math.max(this.xmin, other.xmin);
        ixmax = Math.min(other.xmax, this.xmax);
        // Y
        iymin = Math.max(this.ymin, other.ymin);
        iymax = Math.min(other.ymax, this.ymax);
        // Z
        izmin = Math.max(this.zmin, other.zmin);
        izmax = Math.min(other.zmax, this.zmax);

        return new BoundingBox(ixmin, ixmax, iymin, iymax, izmin, izmax);
    }


    @Override
    public String toString() {
        return "BoundingBox{" +
                "xmin=" + xmin +
                ", ymin=" + ymin +
                ", zmin=" + zmin +
                ", xmax=" + xmax +
                ", ymax=" + ymax +
                ", zmax=" + zmax +
                '}';
    }
}
