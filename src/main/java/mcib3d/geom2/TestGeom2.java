package mcib3d.geom2;

import ij.IJ;
import ij.ImagePlus;
import ij.measure.Calibration;
import mcib3d.geom.*;
import mcib3d.geom2.measurements.*;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationClosestDistance;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationColocalisation;
import mcib3d.geom2.measurementsPopulation.PairObjects3DInt;
import mcib3d.image3d.*;
import mcib3d.image3d.processing.*;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class TestGeom2 {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException, InterruptedException {
//        ImagePlus plus1 = IJ.openImage("/home/thomas/Download/C1-test1.tif");
//        ImagePlus plus2 = IJ.openImage("/home/thomas/Download/C2-test1.tif");
//        testOrientationIntensity(plus1, plus2);
//        testHistogramOrientation(plus1, plus2);

//        plus1 = IJ.openImage("/home/thomas/Download/C1-test2.tif");
//        plus2 = IJ.openImage("/home/thomas/Download/C2-test2.tif");
//        testOrientationIntensity(plus1, plus2);
//        testHistogramOrientation(plus1, plus2);

//        ArrayList<String> cm = new ArrayList<>();
//        cm.add("cmd.exe");
//        cm.add("\"F:\\HETMLAB\\Projects\\DM_P0_OLD projects\\UNLOC_2022\\Version_2022_Octobre\\UNLOC_Plugin_20221017\\plugins\\UNLOC\\EXECUTABLE\\UNLOC_detect.exe\"");
//        cm.add("\"D:\\TEMP\\Data_test\\UNLOC_20221018_13h24m23s_HD\\UNLOC_detect_param.m\"");
//        cm.add("\"D:\\TEMP\\Data_test\\UNLOC_20221018_13h24m23s_HD\\UNLOC_detect_param_expert.m\"");
//        ProcessBuilder builder = new ProcessBuilder(cm);
//
//        builder.command().forEach(c-> System.out.println(c+" "));

        //testGeom2();

        ImageHandler img = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/DATA/Misc/cells.tif"));
        ImageHandler mask = ImageHandler.wrap(IJ.openImage("/home/thomas/mask.tif"));
        testShuffler(img, mask);
    }

    private static void testShuffler(ImageHandler ima, ImageHandler mask){
        ImageShuffler shuffler = new ImageShuffler(ima);
        shuffler.setMask(mask);
        shuffler.shuffle().show();
    }

    private static ImageHandler cellposeSeg(ImageHandler img) throws IOException, InterruptedException {
        String dirCellposeTmp = "/home/thomas/tmpcellpose";
        String dirCellposeTmp2 = dirCellposeTmp + File.separator + "seg";

        String title = img.getTitle();

        // save image
        img.setTitle("image");
        img.save(dirCellposeTmp,true);

        // RUN CELLPOSE (linux only)
        String python = "/home/thomas/miniconda3/envs/cellpose/bin/python"; // CHANGE PATH HERE
        String model = "nuclei";
        double diameter = 30.0;
        boolean do3d = true;
        String do3ds = do3d ? "--do_3D" : "";
        double anisotropy = img.getVoxelSizeZ() / img.getVoxelSizeXY();
        anisotropy = Math.max(1.0, anisotropy);
        String cmd = python + " -m cellpose --dir " + dirCellposeTmp + " --chan 0 --pretrained_model " + model + " --diameter " + diameter
                + " --save_tif --no_npy " + do3ds + " --use_gpu --anisotropy " + anisotropy + " --savedir " + dirCellposeTmp2 + " --verbose";
        System.out.println(cmd);

        // EXE from BIOP cellpose wrapper
        ProcessBuilder pb = new ProcessBuilder("/usr/bin/bash", "-c", cmd);
        Process p = pb.start();
        p.waitFor();
        int exitValue = p.exitValue();
        if (exitValue != 0) {
            System.out.println("Runner exited with value " + exitValue + ". Please check output above for indications of the problem.");
        } else {
            System.out.println(" run finished");
        }

        // LOAD result
        ImagePlus plus = IJ.openImage(dirCellposeTmp2+File.separator+"image_cp_masks.tif");
        Files.move(Paths.get(dirCellposeTmp2,"image_cp_masks.tif"),Paths.get(dirCellposeTmp2,title+".tif"));

        return ImageHandler.wrap(plus);
    }
    private static void cellpose(ImageHandler img) throws IOException, InterruptedException {
        String dirCellposeTmp = "/home/thomas/tmpcellpose";
        String dirCellposeTmp2 = dirCellposeTmp + File.separator + "seg";
        // FIRST SPLIT IMAGE
        int nbSplitX = 4; // parameter
        int nbSplitY = 4; // parameter
        int nbSplitZ = 1; // parameter
        int overlapX = 64, overlapY = 64, overlapZ = 0; // parameters
        int sizeX = (img.sizeX + (nbSplitX - 1) * overlapX) / nbSplitX;
        int sizeY = (img.sizeY + (nbSplitY - 1) * overlapY) / nbSplitY;
        int sizeZ = (img.sizeZ + (nbSplitZ - 1) * overlapZ) / nbSplitZ;
        List<ImageHandler> files = split(dirCellposeTmp, img, sizeX, sizeY, sizeZ, overlapX, overlapY, overlapZ);

       List<ImageHandler> res = new ArrayList<>();
        for (ImageHandler file : files) {
            System.out.println("Processing "+file.getTitle());
            res.add(cellposeSeg(file));
        }

        merge();
    }

    private static List<ImageHandler> split(String dir, ImageHandler img1, int sizeX, int sizeY, int sizeZ, int overlapX, int overlapY, int overlapZ) throws IOException {
        List<ImageHandler> crops = new ImageCropper(img1).cropSplit(sizeX, sizeY, sizeZ, overlapX, overlapY, overlapZ);
        // save image
        AtomicInteger ai = new AtomicInteger(0);
        crops.forEach(crop -> {
            crop.setTitle("crop_" + ai.getAndIncrement());
            //crop.save(dir, true);
        });

        // SAVE INFORMATION
        BufferedWriter bf = new BufferedWriter(new FileWriter(dir + File.separator + "cals.txt"));
        crops.forEach(crop -> {
            Calibration calibration = crop.getImagePlus().getCalibration();
            try {
                bf.write(calibration.pixelWidth + ":" + calibration.pixelHeight + ":" + calibration.pixelDepth
                        + ":" + calibration.xOrigin + ":" + calibration.yOrigin + ":" + calibration.zOrigin + "\n");
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        bf.close();

        return crops;
    }

    private static void merge() throws IOException {
        String dirCellposeTmp = "/home/thomas/tmpcellpose";
        String dirCellposeTmp2 = dirCellposeTmp + File.separator + "seg";
        File dirSeg = new File(dirCellposeTmp2);
        FilenameFilter tifFileFilter = (d, s) -> s.toLowerCase().endsWith(".tif");
        String[] files = dirSeg.list(tifFileFilter);
        // LOAD INFORMATION
        int nb = files.length;// CALCULATE new File(<directory path>).listFiles().length
        System.out.println("Analysing "+nb+" files");
        int count = 0;
        String[] lines = new String[nb];
        BufferedReader br = new BufferedReader(new FileReader("/home/thomas/tmpcellpose/cals.txt"));
        String line = br.readLine();
        while ((line != null) && (!line.isEmpty())) {
            lines[count++] = line;
            for (String s : line.split(":")) {
                //System.out.println(s);
            }
            line = br.readLine();
            //System.out.println("");
        }
        br.close();

        // LOAD IMAGES
        List<ImageHandler> crops = new ArrayList<>();
        for (int i = 0; i < nb; i++) {
            ImagePlus plus = IJ.openImage("/home/thomas/tmpcellpose/seg/crop_" + i+".tif");
            Calibration cal = plus.getCalibration();
            String[] cals = lines[i].split(":");
            cal.pixelWidth = Double.parseDouble(cals[0].trim());
            cal.pixelHeight = Double.parseDouble(cals[1].trim());
            cal.pixelDepth = Double.parseDouble(cals[2].trim());
            cal.xOrigin = Double.parseDouble(cals[3].trim());
            cal.yOrigin = Double.parseDouble(cals[4].trim());
            cal.zOrigin = Double.parseDouble(cals[5].trim());

            ImageHandler img = ImageHandler.wrap(plus);
            System.out.println("Image " + img.offsetX + " " + img.offsetY + " " + img.offsetZ);
            crops.add(img);
        }

        // MERGE
        ImageHandler merged = new ImageStitcher(crops).stitchImages();
        merged.setTitle("merged.tif");
        merged.save("/home/thomas/");

        // MERGE POP
        System.out.println("Merging population");
        ImageHandler mergedPop = new ImageStitcher(crops).stitchLabelImages().drawImage();
        mergedPop.setTitle("mergedPop.tif");
        mergedPop.save("/home/thomas/");
    }


    private static void testGeom2() throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException, IOException, InterruptedException {

        // TEST CROP 3D WITH MULTI LABEL




        // TEST STITCH SEG
//        ImagePlus plus = IJ.openImage("/home/thomas/tmpcellpose/stitchSeg2-offX480.tif");
//        int offX = (int) -plus.getCalibration().xOrigin;
//        int offY = (int) -plus.getCalibration().yOrigin;
//        int offZ = (int) -plus.getCalibration().zOrigin;
//        System.out.println("Offset "+offX+" "+offY+" "+offZ);
//
//        ImageHandler img1 = ImageHandler.wrap(IJ.openImage("/home/thomas/tmpcellpose/stitchSeg1.tif"));
//        ImageHandler img2 = ImageHandler.wrap(IJ.openImage("/home/thomas/tmpcellpose/stitchSeg2.tif"));
//        img2.setOffset(offX, offY, offZ);
//        BoundingBox box1 = new BoundingBox(img1);
//        BoundingBox box2 = new BoundingBox(img2);
//        System.out.println(box1+" "+box2+" "+box1.disjoint(box2));
//        box1.adjustBounding(box2);
//        System.out.println(box1+" "+box2+" "+box1.disjoint(box2));
//
//        Objects3DIntPopulation pop1 = new Objects3DIntPopulation(img1);
//        Objects3DIntPopulation pop2 = new Objects3DIntPopulation(img2);
//        pop2.translateObjects(offX, offY, offZ);
//
//        Objects3DIntPopulation population = stitchSeg(pop1, pop2);
//
//        BoundingBox box = new BoundingBox(box1);
//        box.adjustBounding(box2);
//        ImageHandler draw = new ImageShort("pop", box.xmax+1, box.ymax+1,box.zmax+1);
//        population.drawImage().show();
//
//        System.out.println("BOX POP "+population.findEnclosingBoundingBox());


//        double d = 0.000256856;
//        DecimalFormat df = new DecimalFormat("#########.################");
//        System.out.println("" + df.format(d));
//
//        ImageHandler img0 = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/Projects/HOLO3D/DATA/FINAL-Macrophages-Suspended-label/Suspended_10_stack.tif-cell2"));
//        Objects3DIntPopulation population = new Objects3DIntPopulation(img0);
//        Object3DInt object3DInt1 = population.getObjects3DInt().get(0);
//        Object3DInt object3DInt2 = population.getObjects3DInt().get(1);
//        Measure2DistanceAdvanced distanceAdvanced = new Measure2DistanceAdvanced(object3DInt1, object3DInt2);
//        System.out.println(distanceAdvanced.getValue(Measure2DistanceAdvanced.RADIUS_CENTRE_PIX));
//        System.out.println(distanceAdvanced.getValue(Measure2DistanceAdvanced.ECCENTRICITY_PIX));
//        System.out.println(distanceAdvanced.getValue(Measure2DistanceAdvanced.DISTBORDER_RADCENTER_PIX));
//        System.out.println(distanceAdvanced.getValue(Measure2DistanceAdvanced.PERIPH_PIX));
//        Measure2Distance distance = new Measure2Distance(object3DInt1, object3DInt2);
//        System.out.println(""+distance.getValue(Measure2Distance.DIST_BB_PIX));


        // TEST MAXIMA FINDER
//        ImageHandler img0 = ImageHandler.wrap(IJ.openImage("/home/thomas/Download/C3-p28U2_2-4_3D_001RedCrop-mini.tif"));
//        MaximaFinder maximaFinder = new MaximaFinder(img0, 2,2,50);
//        maximaFinder.getImagePeaks().show();

        // TEST DISTANCES
//        ImageHandler img0 = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/Projects/HOLO3D/DATA/FINAL-Macrophages-Suspended-label/Suspended_10_stack.tif-cell2"));
//        Objects3DIntPopulation population = new Objects3DIntPopulation(img0);
//        Object3DInt object3DInt1 = population.getObjects3DInt().get(0);
//        Object3DInt object3DInt2 = population.getObjects3DInt().get(1);
//        MeasurePopulationDistance distance = new MeasurePopulationDistance(population, population);
//        distance.setMeasurementMethod(MeasurePopulationDistance.DIST_CB_UNIT);
//        distance.getResultsTableOnlyColoc().show("CB");

        // TEST CLOSEST DISTANCES
        ImageHandler img0 = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/Projects/SAPHIR2DB/Data/image-plaque-de-peyer_2021-10-14_0734/ROOT/FalletProj/dataset-seg/19juil05a1.czi-nuclei.tif"));
        double distMax = 10000;
        Objects3DIntPopulation pop1 = new Objects3DIntPopulation(img0);
        MeasurePopulationClosestDistance distance = new MeasurePopulationClosestDistance(pop1, pop1, distMax, MeasurePopulationClosestDistance.CLOSEST_CC2_PIX);
        distance.setRemoveZeroDistance(true);
        pop1.getObjects3DInt().forEach(obj -> {
            double[] dists = distance.getValuesObject1Sorted(obj.getLabel(),true);
            if(dists.length>2) System.out.println("obj1:"+dists[0]+" obj2:"+dists[1]+" dist:"+dists[2]);
        });


        // LOAD SAVE POPULATION
//        ImageHandler img0 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Mailly/wetransfer_m1-ant-yh2ax-contra-czi-c-0-tif_2022-05-31_0903/M1-ant-yH2Ax-contra.czi - C=0.tif"));
//        Objects3DIntPopulation population = new Objects3DIntPopulation(img0);
//        System.out.println("Saving "+population.getNbObjects()+" objects");
//        population.saveObjects("/home/thomas/population3d.zip");
//
//        Objects3DIntPopulation population1 = new Objects3DIntPopulation();
//        population1.loadObjects("/home/thomas/population3d.zip");
//        System.out.println("Loaded "+population1.getNbObjects()+" objects");
//
//        System.out.println(population.getObjectByLabel(1).size()+" / "+population1.getObjectByLabel(1).size());

        //ObjectCreator3D creator3D = new ObjectCreator3D(256,256,128);
        //creator3D.createEllipsoidQuadrants(128,128,64,64,64,32);
        //creator3D.getImageHandler().save("/home/thomas/");

        //ImagePlus plus = new ImagePlus("/home/thomas/Downloads/Shape3D.tif");
        // ImagePlus plus = new ImagePlus("/home/thomas/bat-cochlea-volume.tif");
        //ImagePlus plus = new ImagePlus("/home/thomas/Downloads/draw.tif");
        //ImagePlus plus = new ImagePlus("/home/thomas/lymphNode.tif");
        //ImageHandler seg = ImageHandler.wrap(IJ.openImage("/home/thomas/Download/seg1.tif"));
        //ImageHandler raw = ImageHandler.wrap(IJ.openImage("/home/thomas/Download/raw1.tif"));

//        ImageHandler bin = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/Projects/Cell_1_20174936-1.zip"));
//
//        ImageFloat edt = EDT.run(bin, 128, false, 0);
//        Instant instant0 = Instant.now();
//        EDT.normalizeDistanceMap(edt, (ImageInt) bin, true);
//        System.out.println(Duration.between(instant0, Instant.now()));
//        edt.setTitle("EV1.tif");
//        edt.save("/home/thomas/");


        //ImagePlus plus = new ImagePlus("/home/thomas/AMU/Projects/HOLO3D/DATA/LT.tif");
        //ImageHandler handler = ImageHandler.wrap(plus);
//         plus = new ImagePlus("/home/thomas/AMU/Projects/HOLO3D/DATA/LT-seg.tif");
//
//        Object3DInt object3DInt = new Object3DInt(handler, 1);
//        Object3DInt edge = new Object3DComputation(object3DInt).getObjectEdgeMorpho(2,2,2,true);
//        new Object3DIntLabelImage(edge).getLabelImage(255).save(("/home/thomas/"));


        //TrackThreshold2 TT = new TrackThreshold2(10,1000000,1,1000,0);
        //TT.setCriteriaMethod(TrackThreshold2.CRITERIA_METHOD_MAX_EDGES);
        //TT.setMethodThreshold(TrackThreshold2.THRESHOLD_METHOD_STEP);
        //for (ImageHandler imageHandler : TT.segmentAll(handler, false)) {
        //    imageHandler.save("/home/thomas/");
        //}
        //System.out.println("Finished");


        // TEST BINARYCLOSELABELS
        //ImageInt img1 = ImageInt.wrap(IJ.openImage("/home/thomas/Downloads/CloseLabels-2.tif"));
        //img1.show();
        //BinaryMorpho.binaryCloseMultilabel(img1, 10, 0).show("TEST1");
        // new BinaryMultiLabel(img1).binaryCloseMultilabel(10,0).show();

        // TEST ERODE / DILATE
        //ImageHandler img1 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Misc-Test/Shape3D.tif"));
        //ImageHandler img1 = ImageInt.wrap(IJ.openImage("/home/thomas/Download/CloseLabels-1.tif"));
        //img1.show();
        //BinaryMultiLabel.fillHoles2DMultiLabel(img1).show();

        // TEST MAILLY

//        ImageHandler img0 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Mailly/wetransfer_m1-ant-yh2ax-contra-czi-c-0-tif_2022-05-31_0903/M1-ant-yH2Ax-contra.czi - C=0.tif"));
//        ImageHandler img3 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Mailly/wetransfer_m1-ant-yh2ax-contra-czi-c-0-tif_2022-05-31_0903/M1-ant-yH2Ax-contra.czi - C=3.tif"));
//        Objects3DIntPopulation pop1 = new Objects3DIntPopulation(img0);
//        Objects3DIntPopulation pop3 = new Objects3DIntPopulation(img3);
//        System.out.println("Pop1: "+pop1.getNbObjects()+" pop3: "+pop3.getNbObjects());
//
//        for (Object3DInt obj1 : pop1.getObjects3DInt()) {
//            for (Object3DInt obj2 : pop3.getObjects3DInt()) {
//                double coloc1 = new Measure2Colocalisation(obj1,obj2).getValue(Measure2Colocalisation.COLOC_VOLUME);
//                if(coloc1 > 0) {
//                    double coloc2 = new Measure2Colocalisation(obj2,obj1).getValue(Measure2Colocalisation.COLOC_VOLUME);
//                    System.out.println(obj1+" "+obj2+" "+coloc1+" "+coloc2);
//                }
//            }
//        }


        //MeasurePopulationDistance distance = new MeasurePopulationDistance(pop1, pop1, distMax, MeasurePopulationDistance.DIST_CC_PIX);
//        double distMax = 10000;
//        MeasurePopulationClosestDistance distance = new MeasurePopulationClosestDistance(pop1, pop1, distMax, MeasurePopulationClosestDistance.CLOSEST_CC2_PIX);
//        distance.setRemoveZeroDistance(true);
//        pop1.getObjects3DInt().forEach(obj -> {
//            double[] dists = distance.getValuesObject1Sorted(obj.getLabel(),true);
//            if(dists.length>2) System.out.println("obj1:"+dists[0]+" obj2:"+dists[1]+" dist:"+dists[2]);
//        });

//        MeasurePopulationColocalisation coloc = new MeasurePopulationColocalisation(pop1, pop3);
//        for(Object3DInt obj0:pop1.getObjects3DInt()){
//            System.out.println("Processing "+obj0.getLabel());
//            double[] colocVal = coloc.getValuesObject1(obj0.getLabel());
//            for(double a:colocVal) System.out.println(a);
//        }
//        System.out.println("Finished");


        // TEST DILATED
//       ImageHandler img1 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Misc-Test/Shape3D.tif"));
//        Object3DInt object3DInt = new Object3DInt(img1);
//        System.out.println(""+new MeasureCentroid(object3DInt).getCentroidAsVoxel());
//        Object3DInt dilated = new Object3DComputation(object3DInt).getObjectDilated(2,2,2);
//        System.out.println(""+new MeasureCentroid(dilated).getCentroidAsVoxel());
//
//        Objects3DIntPopulation population = new Objects3DIntPopulation();
//        population.addObject(object3DInt);
//        System.out.println("Nb objects "+population.getNbObjects());
//        population.addObject(object3DInt);
//        System.out.println("Nb objects "+population.getNbObjects());
//        Object3DInt object3DInt2 =  new Object3DComputation(object3DInt).getObject3DCopy();
//        population.resetLabels();
//        population.addObject(object3DInt2);
//        System.out.println("Nb objects "+population.getNbObjects());

        // COLOC
//        ImageHandler img1 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Heck/A1.tif"));
//        ImageHandler img2 = ImageHandler.wrap(IJ.openImage("/home/thomas/DATALOCAL/MISC-TEST/Heck/B1.tif"));
//        Objects3DIntPopulation pop1 = new Objects3DIntPopulation(img1);
//        Objects3DIntPopulation pop2 = new Objects3DIntPopulation(img2);
//        MeasurePopulationColocalisation colocalisation = new MeasurePopulationColocalisation(pop1, pop2);
//        pop1.getObjects3DInt().stream().forEach(obj1 -> {
//            double[] coloc = colocalisation.getValuesObject1(obj1.getLabel());
//            for(int i=0;i<coloc.length; i+=3) {
//                Object3DInt obj2 = pop2.getObjectByLabel((float) coloc[i+1]);
//                // do stuff
//                System.out.println(obj1.getLabel()+" "+obj2.getLabel());
//            }
//        });
        // TEST LABEL IMAGE
        // new Object3DIntLabelImage(population.getFirstObject()).getLabelImage(255).show();
        // new Object3DIntLabelImage(population.getFirstObject()).getMiniLabelImage(1,1,1,255,false).show();
        // TEST FIRST OBJECT VALUES
//        Object3DInt object3DInt = population.getObjectByLabel(1);
//        Object3DVoxels object3DVoxels = new Object3DVoxels(seg, (int) object3DInt.getLabel());
//
//        List<Double[]> results = population.getMeasurementsList(new MeasureEllipsoid().getNamesMeasurement());
//        Arrays.stream(results.get(0)).forEach(System.out::println);

//        Double[] values = new MeasureObject(population.getFirstObject()).measureList(new String[]{MeasureVolume.VOLUME_PIX, MeasureCentroid.CX_PIX, MeasureCentroid.CY_PIX, "test"});
//        Arrays.stream(values).forEach(System.out::println);
//        Double[] valuesInt = new MeasureObject(population.getFirstObject()).measureIntensityList(new String[]{MeasureIntensity.INTENSITY_SUM, MeasureCenterOfMass.MASS_CENTER_X_PIX, MeasureCenterOfMass.MASS_CENTER_Y_PIX, "test"}, raw);
//        Arrays.stream(valuesInt).forEach(System.out::println);
//        //System.out.println("SUM "+new MeasureObject(population.getFirstObject()).measureIntensity(MeasureIntensity.INTENSITY_SUM, raw));
//        Instant instant0 = Instant.now();
//        List<Double[]> results = population.getMeasurementsIntensity(MeasureIntensity.INTENSITY_SUM, raw);
//        double sum = results.stream().map(arr -> arr[1]).reduce(0.0, Double::sum);
//        System.out.println("NB " + population.getNbObjects() + " sum " + sum);
//        System.out.println(Duration.between(instant0, Instant.now()));
//        instant0 = Instant.now();
//        results = population.getMeasurements(MeasureFeret.FERET_PIX);
//        sum = results.stream().map(arr -> arr[1]).reduce(0.0, Double::sum);
//        System.out.println("NB " + results.size() + " sum " + sum);
//        System.out.println(Duration.between(instant0, Instant.now()));


//
        //ImageHandler imgA = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/DATA/Heck/A1.tif"));
//        ImageHandler imgB = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/DATA/Heck/B1.tif"));
//
//        ImageHandler imgAcoloc = imgA.createSameDimensions();
//        imgAcoloc.setTitle(imgA.getTitle()+"-coloc.tif");
//        ImageHandler imgBcoloc = imgB.createSameDimensions();
//        imgBcoloc.setTitle(imgB.getTitle()+"-coloc.tif");
//
        // Objects3DIntPopulation popA = new Objects3DIntPopulation(imgA);
        //VoxelInt centre = new VoxelInt(imgA.sizeX/2, imgA.sizeY/2, imgA.sizeZ/2,1);
        // System.out.println(popA.getFirstObject().contains(centre));
        // Object3DInt centre2 = new Object3DInt(centre);
        // System.out.println(new MeasureCentroid(centre2).getCentroidAsPoint());
        //  Measure2Distance distance = new Measure2Distance(popA.getFirstObject(), centre2);
        //  System.out.println(distance.getValue(Measure2Distance.DIST_CC_PIX));
        //System.out.println(popA.getFirstObject().getLabel()+" "+ popA.getNbObjects());
        //popA.removeObject(popA.getFirstObject());
        //System.out.println(popA.getFirstObject().getLabel()+" "+ popA.getNbObjects());
//        Objects3DIntPopulation popB = new Objects3DIntPopulation(imgB);
//
//        Objects3DIntPopulation popAcoloc = new Objects3DIntPopulation();
//        Objects3DIntPopulation popBcoloc = new Objects3DIntPopulation();
//
//        Instant instant0 = Instant.now();

//        MeasurePopulationColocalisation colocalisation = new MeasurePopulationColocalisation(popA,popB);
//
//        popA.getObjects3DInt().stream().filter(A -> colocalisation.getValuesObject1(A.getValue()).length>0).forEach(popAcoloc::addObject);
//        popB.getObjects3DInt().stream().filter(B -> colocalisation.getValuesObject2(B.getValue()).length>0).forEach(popBcoloc::addObject);
//
//        popAcoloc.drawInImage(imgAcoloc);
//        popBcoloc.drawInImage(imgBcoloc);
//
//        imgAcoloc.save("/home/thomas/");
//        imgBcoloc.save("/home/thomas/");
//
//        List<VoxelInt> list = new LinkedList<>();
//        popA.getFirstObject().getObject3DPlanes().forEach(plane -> list.addAll(plane.getVoxels()));


//        // Shuffle
//        Objects3DPopulation population = new Objects3DPopulation(seg);
//        List<Object3D> list = new ArrayList<>();
//        list.add(population.getObject(0));
//        list.add(population.getObject(1));
//
//        Objects3DPopulation population1 = new Objects3DPopulation(list);
//
//
//        Instant instant0 = Instant.now();
//
//        seg.show();
//        ImageShuffler shuffler = new ImageShuffler(seg);
//        ImageHandler shuffle = shuffler.shuffle();
//        if(shuffle != null) {
//            shuffle.show();
//            shuffle.setTitle("shuffle");
//            shuffle.save("/home/thomas/");
//        }
//        System.out.println(shuffle);

        // crop
//        Objects3DIntPopulation population = new Objects3DIntPopulation(seg);
//        ImageCropper cropper = new ImageCropper(raw);
//        ImageHandler crop1 =  cropper.cropBoundingBoxObject(population.getObjectByValue(8));
//        crop1.setTitle("crop1"); crop1.save("/home/thomas/");
//        ImageHandler crop2 = cropper.cropBoundingBoxMaskObject(population.getObjectByValue(8));
//        crop2.setTitle("crop2"); crop2.save("/home/thomas/");

        // binary close
//        cropper = new ImageCropper(seg);
//        ImageHandler crop3 = cropper.cropBoundingBoxMaskObject(population.getObjectByValue(8));
//        crop3.setTitle("crop3"); crop3.save("/home/thomas/");
//
//        BinaryMultiLabel multiLabel = new BinaryMultiLabel(seg);
//        ImageHandler dilated = multiLabel.binaryCloseMultilabel(4,2);
//        dilated.setTitle("dilated"); dilated.save("/home/thomas/");
    }

    static void testOrientationIntensity(ImagePlus plus1, ImagePlus plus2) {
        ImageHandler nucleus = ImageHandler.wrap(plus1);
        ImageHandler signal = ImageHandler.wrap(plus2);
        Objects3DIntPopulation popNucleus = new Objects3DIntPopulation(nucleus);
        Objects3DIntPopulation popSignal = new Objects3DIntPopulation(signal);
        Object3DInt objNucleus = popNucleus.getFirstObject();
        Voxel3D centreNucleus = new MeasureCentroid(objNucleus).getCentroidAsVoxel();
        System.out.println("Nucleus " + objNucleus + " " + centreNucleus);
        Vector3D sum = new Vector3D();
        int nb = 0;
        for (Object3DInt object3DInt : popSignal.getObjects3DInt()) {
            VoxelInt voxelInt = object3DInt.getFirstVoxel();
            Vector3D vector3D = new Vector3D(centreNucleus.x - voxelInt.getX(), centreNucleus.y - voxelInt.getY(), centreNucleus.z - voxelInt.getZ());
            sum = sum.add(vector3D);
            nb++;
        }
        double mult = -1.0 / (double) nb;
        sum = sum.multiply(mult);
        System.out.println("AVG VECT " + sum + " " + sum.getLength());
    }

    static void testHistogramOrientation(ImagePlus plus1, ImagePlus plus2) {
        ImageHandler nucleus = ImageHandler.wrap(plus1);
        ImageHandler signal = ImageHandler.wrap(plus2);
        Objects3DIntPopulation popNucleus = new Objects3DIntPopulation(nucleus);
        Objects3DIntPopulation popSignal = new Objects3DIntPopulation(signal);
        Object3DInt objNucleus = popNucleus.getFirstObject();
        Voxel3D centreNucleus = new MeasureCentroid(objNucleus).getCentroidAsVoxel();
        System.out.println("Nucleus " + objNucleus + " " + centreNucleus);

        int[] histAngles = new int[37];
        Vector3D axisX = new Vector3D(1, 0, 0);
        Vector3D axisY = new Vector3D(0, -1, 0);

        Vector3D v1 = new Vector3D(1, 1, 0);
        System.out.println("V1 " + axisX.dotProduct(v1) + " " + axisY.dotProduct(v1) + " " + axisX.angleDegrees(v1) + " " + axisY.angleDegrees(v1));
        System.out.println("V1 " + finalAngle(v1, axisX, axisY));

        Vector3D v2 = new Vector3D(-1, 1, 0);
        System.out.println("V2 " + axisX.dotProduct(v2) + " " + axisY.dotProduct(v2) + " " + axisX.angleDegrees(v2) + " " + axisY.angleDegrees(v2));
        System.out.println("V2 " + finalAngle(v2, axisX, axisY));

        Vector3D v3 = new Vector3D(-1, -1, 0);
        System.out.println("V3 " + axisX.dotProduct(v3) + " " + axisY.dotProduct(v3) + " " + axisX.angleDegrees(v3) + " " + axisY.angleDegrees(v3));
        System.out.println("V3 " + finalAngle(v3, axisX, axisY));

        Vector3D v4 = new Vector3D(1, -1, 0);
        System.out.println("V4 " + axisX.dotProduct(v4) + " " + axisY.dotProduct(v4) + " " + axisX.angleDegrees(v4) + " " + axisY.angleDegrees(v4));
        System.out.println("V4 " + finalAngle(v4, axisX, axisY));


        for (Object3DInt object3DInt : popSignal.getObjects3DInt()) {
            VoxelInt voxelInt = object3DInt.getFirstVoxel();
            Vector3D vector3D = new Vector3D(centreNucleus.x - voxelInt.getX(), centreNucleus.y - voxelInt.getY(), centreNucleus.z - voxelInt.getZ());
            double angle = finalAngle(vector3D, axisX, axisY);

            histAngles[(int) Math.round(angle / 10)]++;
        }
        for (int a = 0; a < 37; a++) {
            System.out.println(a + ", " + histAngles[a]);
        }
    }

    private static double finalAngle(Vector3D vector3D, Vector3D axisX, Vector3D axisY) {
        double angle = axisX.angleDegrees(vector3D);
        double dotX = axisX.dotProduct(vector3D);
        double dotY = axisY.dotProduct(vector3D);
        if ((dotX > 0) && (dotY > 0)) angle = angle;
        if ((dotX < 0) && (dotY > 0)) angle += 90;
        if ((dotX < 0) && (dotY < 0)) angle += 180;
        if ((dotX > 0) && (dotY < 0)) angle += 270;

        return angle;
    }


}
