package mcib3d.geom2;

import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

/**
 * The class to create a Label Image from an Object3DInt
 */
public class Object3DIntLabelImage {
    /**
     * The 3D object.
     */
    Object3DInt object3DInt;

    /**
     * Instantiates a new Label Image creator.
     *
     * @param object3DInt the 3D object
     */
    public Object3DIntLabelImage(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
    }

    /**
     * Gets label image for the 3D object.
     * It will create an image to the maximal extend of the 3D object.
     * Extra border can also be added.
     *
     * @param borderX the border in X
     * @param borderY the border in Y
     * @param borderZ the border in Z
     * @param value   the value for the 3D object inside the image.
     * @param bit32   True if the label image should be 32-bit
     * @return the label image
     */
    public ImageHandler getLabelImage(int borderX, int borderY, int borderZ, float value, boolean bit32) {
        // bounding box
        BoundingBox box = object3DInt.getBoundingBox();
        int xma = box.xmax;
        int yma = box.ymax;
        int zma = box.zmax;

        ImageHandler segImage;
        if (bit32)
            segImage = new ImageFloat("Object_" + object3DInt.getLabel(), xma + 1 + borderX, yma + 1 + borderY, zma + 1 + borderZ);
        else
            segImage = new ImageShort("Object_" + object3DInt.getLabel(), xma + 1 + borderX, yma + 1 + borderY, zma + 1 + borderZ);
        segImage.setVoxelSize(object3DInt.getVoxelSizeXY(), object3DInt.getVoxelSizeZ(), object3DInt.getUnit());

        object3DInt.drawObject(segImage, value);

        return segImage;
    }

    public ImageHandler getCustomLabelImage(int sizex, int sizey, int sizez, float value, boolean bit32){
        ImageHandler segImage;
        if (bit32)
            segImage = new ImageFloat("Object_" + object3DInt.getLabel(), sizex, sizey, sizez);
        else
            segImage = new ImageShort("Object_" + object3DInt.getLabel(), sizex, sizey, sizez);
        segImage.setVoxelSize(object3DInt.getVoxelSizeXY(), object3DInt.getVoxelSizeZ(), object3DInt.getUnit());

        object3DInt.drawObject(segImage, value);

        return segImage;

    }

    /**
     * Gets label image for the 3D object.
     * It will create an image to the maximal extend of the 3D object.
     *
     * @param value   the value for the 3D object inside the image.
     * @param bit32   True if the label image should be 32-bit
     * @return the label image
     */
    public ImageHandler getLabelImage(float value, boolean bit32) {
        return getLabelImage(0, 0, 0, value, bit32);
    }

    /**
     * Gets label image for the 3D object.
     * It will create an image to the maximal extend of the 3D object.
     *
     * @param value   the value for the 3D object inside the image.
     * @return the label image
     */
    public ImageHandler getLabelImage(float value) {
        return getLabelImage(0, 0, 0, value, false);
    }

    /**
     * Gets label image for the 3D object.
     *
     * @return the label image
     */
    public ImageHandler getLabelImage() {
        return getLabelImage(0, 0, 0, object3DInt.getLabel(), false);
    }

    /**
     * Gets a cropped version of the label image.
     * The image is restricted to the bounding box of the 3D object.
     * The image will have corresponding offset values.
     * Extra border can be added.
     *
     * @param borderX the border in X
     * @param borderY the border in Y
     * @param borderZ the border in Z
     * @param value   the value for the 3D object inside the image.
     * @param bit32   True if the label image should be 32-bit
     * @return the cropped label image
     */
    public ImageHandler getCroppedLabelImage(int borderX, int borderY, int borderZ, float value, boolean bit32) {
        // bounding box
        BoundingBox box = object3DInt.getBoundingBox();
        // size
        int sx = box.xmax - box.xmin + 1;
        int sy = box.ymax - box.ymin + 1;
        int sz = box.zmax - box.zmin + 1;

        ImageHandler segImage;

        if (bit32)
            segImage = new ImageFloat("Object_" + object3DInt.getLabel(), sx + 2 * borderX, sy + 2 * borderY, sz + 2 * borderZ);
        else
            segImage = new ImageShort("Object_" + object3DInt.getLabel(), sx + 2 * borderX, sy + 2 * borderY, sz + 2 * borderZ);
        segImage.setOffset(box.xmin - borderX, box.ymin - borderY, box.zmin - borderZ);
        segImage.setVoxelSize(object3DInt.getVoxelSizeXY(), object3DInt.getVoxelSizeZ(), object3DInt.getUnit());

        object3DInt.drawObjectUsingOffset(segImage, value);

        return segImage;
    }

    /**
     * Gets a cropped version of the label image.
     * The image is restricted to the bounding box of the 3D object.
     * The image will have corresponding offset values.
     *
     * @param value   the value for the 3D object inside the image.
     * @return the cropped label image
     */
    public ImageHandler getCroppedLabelImage(float value) {
        return getCroppedLabelImage(0, 0, 0, value, false);
    }

    /**
     * Gets a cropped version of the label image.
     * The image is restricted to the bounding box of the 3D object.
     * The image will have corresponding offset values.
     * The image will use the label value of the 3D object.
     *
     * @return the cropped label image
     */
    public ImageHandler getCroppedLabelImage() {
        return getCroppedLabelImage(object3DInt.getLabel());
    }

}
