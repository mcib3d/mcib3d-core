package mcib3d.geom2;

import mcib3d.geom.Object3DVoxels;
import mcib3d.utils.AboutMCIB;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * The class to save a 3D object to file.
 * Will save the lsit of voxels, organised by planes.
 */
public class Object3DIntSaver {
    private Object3DInt object3DInt;

    /**
     * Instantiates a new saver.
     *
     * @param object3DInt the object 3 d int
     */
    public Object3DIntSaver(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
    }

    /**
     * Saves the 3D object in to a file.
     *
     * @param dir  the directory for the file
     * @param name the name of the file
     * @return True if the file was saved correctly
     */
    public boolean saveObject(String dir, String name, boolean zip) {
        BufferedWriter bf;
        final String fs = File.separator;
        try {
            bf = new BufferedWriter(new FileWriter(dir + fs + name));
            saveInfo(bf);
            object3DInt.getObject3DPlanes().forEach(object3DPlane -> object3DPlane.saveVoxels(bf));
            bf.close();
        } catch (IOException ex) {
            Logger.getLogger(Object3DVoxels.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        if (zip) {
            return zipFile(dir, name, true);
        }

        return true;
    }

    private boolean zipFile(String dir, String name, boolean deleteAfterZip) {
        final String fs = File.separator;
        String zipFileName = dir + fs + name + "-3droi.zip";
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFileName));
             FileInputStream fis = new FileInputStream(dir + fs + name + ".3droi")) {
            ZipEntry zipEntry = new ZipEntry(name + ".3droi");
            zos.putNextEntry(zipEntry);

            byte[] buffer = new byte[1024];
            int len;
            while ((len = fis.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
            zos.closeEntry();

            if (deleteAfterZip) {
                Files.delete(Paths.get(dir, name + ".3droi"));
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void saveInfo(BufferedWriter bf) throws IOException {
        // version
        bf.write("version=\t" + AboutMCIB.getVERSION() + "\n");
        // calibration
        bf.write("cal=\t" + object3DInt.getVoxelSizeXY() + "\t" + object3DInt.getVoxelSizeZ() + "\t" + object3DInt.getUnit() + "\n");
        // comments
        if (!object3DInt.getComment().isEmpty()) {
            bf.write("comment=\t" + object3DInt.getComment() + "\n");
        }
        // type
        if (object3DInt.getType() > 0) {
            bf.write("type=\t" + object3DInt.getType() + "\n");
        }
        // value
        bf.write("value=\t" + object3DInt.getLabel() + "\n");
    }
}
