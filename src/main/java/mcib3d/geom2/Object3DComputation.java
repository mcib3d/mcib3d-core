package mcib3d.geom2;

import mcib3d.geom.Point3D;
import mcib3d.geom.Vector3D;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.measurements.MeasureCentroid;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;
import mcib3d.image3d.processing.BinaryMorpho;
import mcib3d.image3d.processing.FastFilters3D;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.DoubleAdder;
import java.util.stream.Collectors;


/**
 * Misc. methods to use with 3D objects.
 */
public class Object3DComputation {
    private Object3DInt object3DInt;

    /**
     * Instantiates a new computation for a 3D object.
     *
     * @param object3DInt the 3D object
     */
    public Object3DComputation(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
    }

    /**
     * Gets the 3D object.
     *
     * @return the 3D object.
     */
    public Object3DInt getObject3D() {
        return object3DInt;
    }

    /**
     * Sets the 3D object.
     *
     * @param object3DInt the 3D object
     */
    public void setObject3D(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
    }

    /**
     * Gets the sum of all coordinates for the voxels inside the object.
     * The result is a voxel as sum of all voxels.
     * (Used for Centroid computation)
     *
     * @return a voxel as sum of all voxels
     */
    public Voxel3D getSumCoordinates() {
        return object3DInt.getObject3DPlanes().stream()
                .map(Object3DPlane::getSumCoordinates)
                .reduce(new Voxel3D(), (s, v) -> new Voxel3D(s.x + v.x, s.y + v.y, s.z + v.z, s.value));
    }

    /**
     * Within the 3D object, any value inside the image has value within the specified range.
     * The two values of range are inclusive.
     *
     * @param handler the image
     * @param t0      the minimal value of the range
     * @param t1      the maximal value of the range
     * @return True if at least one voxel has value within range
     */
    public boolean hasOneVoxelValueRange(ImageHandler handler, float t0, float t1) {
        for (Object3DPlane plane : getObject3D().getObject3DPlanes()) {
            if (plane.hasOneVoxelValueRange(handler, t0, t1)) return true;
        }

        return false;
    }

    /**
     * Within the 3D object, any value inside the image has value above specified value.
     * The two values of range are inclusive.
     *
     * @param handler the image
     * @param t0      the minimal value of the range
     * @return True if at least one voxel has value within range
     */
    public boolean hasOneVoxelValueAboveStrict(ImageHandler handler, float t0) {
        for (Object3DPlane plane : getObject3D().getObject3DPlanes()) {
            if (plane.hasOneVoxelValueAboveStrict(handler, t0)) return true;
        }

        return false;
    }


    /**
     * Within the translated 3D object, any value inside the image has value within the specified range.
     * The two values of range are inclusive.
     *
     * @param handler the image
     * @param t0      the minimal value of the range
     * @param t1      the maximal value of the range
     * @param tx      the translation value in x
     * @param ty      the translation value in x
     * @param tz      the translation value in x
     * @return True if at least one translated voxel has value within range
     */
    public boolean hasOneVoxelValueRangeTranslate(ImageHandler handler, float t0, float t1, int tx, int ty, int tz) {
        for (Object3DPlane plane : getObject3D().getObject3DPlanes()) {
            if (plane.hasOneVoxelValueRangeTranslate(handler, t0, t1, tx, ty, tz)) return true;
        }

        return false;
    }


    /**
     * Get a 3D copy of the 3D object.
     * A new 3D object is created from a label image containing only the 3D object.
     * All inner information are copied.
     *
     * @return the object 3 d int
     */
    public Object3DInt getObject3DCopy() {
        Object3DInt copy = new Object3DInt(new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage());
        copy.setName(getObject3D().getName());
        copy.setLabel(getObject3D().getLabel());
        copy.setComment(getObject3D().getComment());
        copy.setType(getObject3D().getType());
        copy.setIdObject(getObject3D().getIdObject());
        copy.setCompareValue(getObject3D().getCompareValue());
        copy.setVoxelSizeXY(getObject3D().getVoxelSizeXY());
        copy.setVoxelSizeZ(getObject3D().getVoxelSizeZ());
        copy.setUnit(getObject3D().getUnit());

        return copy;
    }

    /**
     * Checks if the 3D object is touching the borders of the image.
     * Check XY borders by default, border in Z in option.
     *
     * @param img the image
     * @param Z   check borders in Z ?
     * @return True if the 3D object touch borders
     */
    public boolean touchBorders(ImageHandler img, boolean Z) {
        return touchBorders(img, true, Z);
    }

    /**
     * Checks if the 3D object is touching the borders of the image.
     * Check XY borders and border in Z.
     *
     * @param img the image
     * @param XY  check borders in XY
     * @param Z   check borders in Z
     * @return True if the 3D object touch borders
     */
    public boolean touchBorders(ImageHandler img, boolean XY, boolean Z) {
        if (!(XY || Z)) return false;

        BoundingBox box = getObject3D().getBoundingBox();
        // 0
        if (XY) {
            if ((box.xmin <= 0) || (box.ymin <= 0)) {
                return true;
            }
            if ((box.xmax >= img.sizeX - 1) || (box.ymax >= img.sizeY - 1)) {
                return true;
            }
        }

        if (Z && (box.zmin <= 0)) {
            return true;
        }

        return Z && (box.zmax >= img.sizeZ - 1);
    }


    /**
     * Compute the moments order 2 of the 3D object.
     * It will use voxel coordinates.
     *
     * @param centroid  the centroid of the 3D object
     * @param normalize normalise by dividing by volume
     * @return the moments order 2 (sum200, sum020, sum002, sum110, sum101, sum011)
     */
    public Double[] computeMoments2(Voxel3D centroid, boolean normalize) {
        final DoubleAdder s200 = new DoubleAdder();
        final DoubleAdder s110 = new DoubleAdder();
        final DoubleAdder s020 = new DoubleAdder();
        final DoubleAdder s011 = new DoubleAdder();
        final DoubleAdder s101 = new DoubleAdder();
        final DoubleAdder s002 = new DoubleAdder();

        object3DInt.getObject3DPlanes()
                .forEach(plane -> {
                    Double[] sums = plane.computeMoments2(centroid);
                    s200.add(sums[0]);
                    s020.add(sums[1]);
                    s002.add(sums[2]);
                    s110.add(sums[3]);
                    s101.add(sums[4]);
                    s011.add(sums[5]);
                });

        // resolution
        double resXY = object3DInt.getVoxelSizeXY();
        double resZ = object3DInt.getVoxelSizeZ();
        double sum200 = s200.sum() * resXY * resXY;
        double sum020 = s020.sum() * resXY * resXY;
        double sum002 = s002.sum() * resZ * resZ;
        double sum110 = s110.sum() * resXY * resXY;
        double sum101 = s101.sum() * resXY * resZ;
        double sum011 = s011.sum() * resXY * resZ;

        // normalize by volume
        if (normalize) {
            double volume = object3DInt.size();
            sum200 /= volume;
            sum020 /= volume;
            sum002 /= volume;
            sum110 /= volume;
            sum101 /= volume;
            sum011 /= volume;
        }

        return new Double[]{sum200, sum020, sum002, sum110, sum101, sum011};
    }

    /**
     * Gets the dilated version of the 3D object.
     *
     * @param rx the radius x for dilation
     * @param ry the radius y for dilation
     * @param rz the radius z for dilation
     * @return the object dilated
     */
    public Object3DInt getObjectDilated(float rx, float ry, float rz) {
        return getMorphologicalObject(BinaryMorpho.MORPHO_DILATE, rx, ry, rz);
    }

    public Object3DInt getObjectEdgeMorpho(float rx, float ry, float rz, boolean outside) {
        if (outside) {
            Object3DInt dilated = getObjectDilated(rx, ry, rz);
            return new Object3DComputation(dilated).getObjectSubtracted(getObject3D());
        } else {
            Object3DInt eroded = getObjectEroded(rx, ry, rz);
            return getObjectSubtracted(eroded);
        }
    }

    public Object3DInt getSubtracted(Object3DInt other) {
        BoundingBox box = getObject3D().getBoundingBox();
        // object 1
        ImageHandler compute1 = new ImageByte("Edge Morpho", box.xmax + 1, box.ymax + 1, box.zmax + 1);
        compute1.fill(3);
        getObject3D().drawObject(compute1, 1);
        // object 2
        ImageHandler compute2 = new ImageByte("Edge Morpho", box.xmax + 1, box.ymax + 1, box.zmax + 1);
        other.drawObject(compute1, 1);
        // subtract image
        ImageHandler compute = compute1.addImage(compute2, 1, -1);
        // get object value 1
        Object3DInt result = new Object3DInt(compute, 1);
        result.setVoxelSizeXY(getObject3D().getVoxelSizeXY());
        result.setVoxelSizeZ(getObject3D().getVoxelSizeZ());

        return result;
    }


    /**
     * Gets the closed version of the 3D object (dilated then eroded);
     *
     * @param rx the radius x for dilation
     * @param ry the radius y for dilation
     * @param rz the radius z for dilation
     * @return the object closed
     */
    public Object3DInt getObjectClosed(float rx, float ry, float rz) {
        return getMorphologicalObject(BinaryMorpho.MORPHO_CLOSE, rx, ry, rz);
    }

    /**
     * Gets the eroded version of the 3D object.
     *
     * @param rx the radius x for dilation
     * @param ry the radius y for dilationv
     * @param rz the radius y for dilation
     * @return the object eroded
     */
    public Object3DInt getObjectEroded(float rx, float ry, float rz) {
        return getMorphologicalObject(BinaryMorpho.MORPHO_ERODE, rx, ry, rz);
    }

    /**
     * Gets the opened version of the 3D object (eroded then dilated);
     *
     * @param rx the radius x for dilation
     * @param ry the radius y for dilation
     * @param rz the radius z for dilation
     * @return the object opened
     */
    public Object3DInt getObjectOpened(float rx, float ry, float rz) {
        return getMorphologicalObject(BinaryMorpho.MORPHO_OPEN, rx, ry, rz);
    }

    /**
     * Get an object as a subtraction between this object and the other object.
     *
     * @param other the other object
     * @return the subtracted object
     */
    public Object3DInt getObjectSubtracted(Object3DInt other) {
        ImageHandler label = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage();
        other.drawObjectUsingOffset(label, 0);
        other.drawObject(label, 0);
        Object3DInt sub = new Object3DInt(label);
        sub.setLabel(getObject3D().getLabel());

        return sub;
    }

    /**
     * Get an object as the union between this object and the other object.
     *
     * @param other the other object
     * @return the union object
     */
    public Object3DInt getUnion(Object3DInt other) {
        // first adjust bounding box
        BoundingBox box = getObject3D().getBoundingBox();
        BoundingBox boxo = other.getBoundingBox();
        box.adjustBounding(boxo);

        // draw objects
        ImageHandler label = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage(1);
        other.drawObjectUsingOffset(label, 1);
        Object3DInt union = new Object3DInt(label, 1);
        union.setLabel(getObject3D().getLabel());

        return union;
    }

    public Object3DInt getUnion(List<Object3DInt> others) {
        // draw objects
        ImageHandler label = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage(1);
        for (Object3DInt other : others) other.drawObjectUsingOffset(label, 1);
        Object3DInt union = new Object3DInt(label);
        union.setLabel(getObject3D().getLabel());

        return union;
    }

    /**
     * Get an object as the intersection between this object and the other object.
     *
     * @param other the other object
     * @return the intersection object
     */
    public Object3DInt getIntersection(Object3DInt other) {
        // draw object
        ImageHandler label1 = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage(1);
        ImageHandler label2 = new Object3DIntLabelImage(other).getCroppedLabelImage(2);
        ImageHandler sum = label1.addImageWithOffset(label2, 1, 1);
        Object3DInt inter = new Object3DInt(sum, 3);
        inter.setLabel(getObject3D().getLabel());

        return inter;
    }

    /**
     * Gets the connex components of the objects (parts);
     *
     * @return the connex components
     */
    public List<Object3DInt> getConnexComponents() {
        ImageHandler seg = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage();
        ImageLabeller labeler = new ImageLabeller();
        List<Object3DInt> objs = labeler.getObjects3D(seg);
        for (Object3DInt obj : objs) {
            obj.translate(seg.offsetX, seg.offsetY, seg.offsetZ);
        }

        return objs;
    }

    /**
     * Check if the object is connex (all voxels connected).
     *
     * @return True is connex
     */
    public boolean isConnex() {
        return (getConnexComponents().size() == 1);
    }

    private Object3DInt getMorphologicalObject(int op, float radX, float radY, float radZ) {
        // special case radii = 0
        if ((radX == 0) && (radY == 0) && (radZ == 0)) {
            return getObject3D();
        }

        // label Image
        ImageHandler labelImage;
        // resize if DILATE or CLOSE
        if ((op == BinaryMorpho.MORPHO_DILATE) || (op == BinaryMorpho.MORPHO_CLOSE))
            labelImage = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage((int) Math.ceil(radX), (int) Math.ceil(radY), (int) Math.ceil(radZ), 1, false);
        else
            labelImage = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage(1, 1, 1, 1, false);

        ImageHandler segImage2;
        /// use fastFilter if rx != ry
        if ((radY != radX) || (radZ == 0)) {
            int filter = 0;
            if (op == BinaryMorpho.MORPHO_DILATE) {
                filter = FastFilters3D.MAX;
            } else if (op == BinaryMorpho.MORPHO_ERODE) {
                filter = FastFilters3D.MIN;
            } else if (op == BinaryMorpho.MORPHO_CLOSE) {
                filter = FastFilters3D.CLOSEGRAY;
            } else if (op == BinaryMorpho.MORPHO_OPEN) {
                filter = FastFilters3D.OPENGRAY;
            }
            segImage2 = FastFilters3D.filterImage(labelImage, filter, radX, radY, radZ, 0, true);
            segImage2.setOffset(labelImage);
        } // else use binaryMorpho class (based on edm)
        else {
            if (op == BinaryMorpho.MORPHO_DILATE) // use enlarge image
                segImage2 = BinaryMorpho.binaryDilate(labelImage, radX, radZ, 0, true);
            else
                segImage2 = BinaryMorpho.binaryMorpho(labelImage, op, radX, radZ, 0);
        }
        if (segImage2 == null) {
            return null;
        }
        Object3DInt objMorpho = new Object3DInt(segImage2);
        objMorpho.setLabel(getObject3D().getLabel());
        objMorpho.setVoxelSizeXY(getObject3D().getVoxelSizeXY());
        objMorpho.setVoxelSizeZ(getObject3D().getVoxelSizeZ());

        return objMorpho;
    }

    /**
     * Gets the contour of the object as a list of voxels.
     *
     * @return the contour
     */
    public List<VoxelInt> getContour() {
        final ImageHandler labelImage = new Object3DIntLabelImage(getObject3D()).getCroppedLabelImage(1, 1, 1, 1, false);
        final List<VoxelInt> voxelInts = new LinkedList<>();
        object3DInt.getObject3DPlanes().forEach(p -> voxelInts.addAll(computeContourPlane(p, labelImage)));

        return voxelInts;
    }


    private List<VoxelInt> computeContourPlane(Object3DPlane plane, ImageHandler labelImage) {
        return plane.getVoxels().stream().filter(v -> isContour(v, labelImage)).collect(Collectors.toList());
    }

    private boolean isContour(VoxelInt v, ImageHandler labelImage) {
        int pix1, pix2, pix3, pix4, pix5, pix6;
        int sx = labelImage.sizeX, sy = labelImage.sizeY, sz = labelImage.sizeZ;

        int i = v.getX() - labelImage.offsetX, j = v.getY() - labelImage.offsetY, k = v.getZ() - labelImage.offsetZ;
        pix1 = (i + 1 < sx) ? (int) labelImage.getPixel(i + 1, j, k) : 0;
        if (pix1 == 0) return true;
        pix2 = (i - 1 >= 0) ? (int) labelImage.getPixel(i - 1, j, k) : 0;
        if (pix2 == 0) return true;
        pix3 = (j + 1 < sy) ? (int) labelImage.getPixel(i, j + 1, k) : 0;
        if (pix3 == 0) return true;
        pix4 = (j - 1 >= 0) ? (int) labelImage.getPixel(i, j - 1, k) : 0;
        if (pix4 == 0) return true;
        pix5 = (k + 1 < sz) ? (int) labelImage.getPixel(i, j, k + 1) : 0;
        if (pix5 == 0) return true;
        pix6 = (k - 1 >= 0) ? (int) labelImage.getPixel(i, j, k - 1) : 0;
        return pix6 == 0;
    }

    /**
     * Vector from a point along a direction to the border of the object The
     * distance is the distance to the point where there is a change in the
     * object inside / outside
     *
     * @param x x coordinate of the point
     * @param y y coordinate of the point
     * @param z z coordinate of the point
     * @param V the direction vector
     * @return The shortest distance Vector
     */
    public Vector3D vectorPixelBorder(double x, double y, double z, Object3DInt other, Vector3D V) {
        BoundingBox box = this.getObject3D().getBoundingBox();
        box.adjustBounding(other.getBoundingBox());
        ImageHandler label = new Object3DIntLabelImage(this.getObject3D()).getCustomLabelImage(box.xmax, box.ymax, box.zmax, 1, false); // FIXME should use larger label image
        Vector3D dir = new Vector3D(V);
        dir.normalize();
        float vx = (float) dir.getX();
        float vy = (float) dir.getY();
        float vz = (float) dir.getZ();
        float px;
        float py;
        float pz;
        px = (float) x;
        py = (float) y;
        pz = (float) z;
        float step = 0.5f;
        boolean in0;
        float pix0 = label.getPixel((int) Math.round(x), (int) Math.round(y), (int) Math.round(z));
        in0 = !Float.isNaN(pix0) && (pix0 != 0);
        boolean in = in0;
        while ((in == in0) && (label.contains(px, py, pz))) {
            px += step * vx;
            py += step * vy;
            pz += step * vz;
            pix0 = label.getPixel((int) Math.round(px), (int) Math.round(py), (int) Math.round(pz));
            in = !Float.isNaN(pix0) && (pix0 != 0);
        }
        if (in != in0) {
            return new Vector3D(px - 0.5 * step * vx - x, py - 0.5 * step * vy - y, pz - 0.5 * step * vz - z);
        } else {
            return null;
        }
    }

    public Vector3D vectorBorderBorder(Object3DInt other, boolean opposite) {
        Point3D point0 = new MeasureCentroid(object3DInt).getCentroidAsPoint();
        Point3D point1 = new MeasureCentroid(other).getCentroidAsPoint();
        Vector3D dir = new Vector3D(point0, point1);
        Vector3D tmp = this.vectorPixelBorder(point0.getX(), point0.getY(), point0.getZ(), other, dir);
        Vector3D V0;
        if (tmp != null) {
            V0 = point0.getVector3D().add(tmp);
        } else {
            return null;
        }
        if (opposite) {
            dir = dir.multiply(-1);
        }

        tmp = new Object3DComputation(other).vectorPixelBorder(point1.getX(), point1.getY(), point1.getZ(), this.getObject3D(), dir);
        Vector3D V1;
        if (tmp != null) {
            V1 = point1.getVector3D().add(tmp);
        } else {
            return null;
        }

        return new Vector3D(V0, V1);
    }
}
