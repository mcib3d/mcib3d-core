package mcib3d.geom2.tracking;

import mcib3d.geom.interactions.InteractionsList;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.measurementsPopulation.PairObjects3DInt;

public class CostTouching implements AssociationCost {
    double maxCost;
    InteractionsList interactionsList;
    int minTouch = 10;


    public CostTouching(InteractionsList interactionsList) {
        this.interactionsList = interactionsList;
    }

    public double getMaxCost() {
        return this.maxCost;
    }


    public void setMaxCost(double maxCost) {
        this.maxCost = maxCost;
    }

    @Override
    public double cost(Object3DInt object3D1, Object3DInt object3D2) {
        float val1 = object3D1.getLabel();
        float val2 = object3D2.getLabel();
        if (val1 == val2) return this.maxCost;
        PairObjects3DInt pair = this.interactionsList.getPair(val1, val2);
        if (pair == null) return -1.0D;
        double vol = pair.getPairValue();
        if (vol == 0) return -1.0D;
        if (vol < this.minTouch) return -1.0D;

        return 1.0D / pair.getPairValue();
    }


}