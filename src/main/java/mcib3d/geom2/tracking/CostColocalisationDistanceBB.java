package mcib3d.geom2.tracking;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.Measure2Distance;
import mcib3d.geom2.measurements.MeasureDistancesCenter;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationColocalisation;

import java.util.HashMap;

public class CostColocalisationDistanceBB implements AssociationCost {
    private final MeasurePopulationColocalisation colocalisation;
    private final HashMap<Object3DInt, Double> objectsMaxRadialDist1; // Max distance center to border for objects population1
    private final HashMap<Object3DInt, Double> objectsMaxRadialDist2; // Max distance center to border for objects population2
    private final double distMax; // max distance BB (pixel)
    private final double pcColocMin;

    public CostColocalisationDistanceBB(Objects3DIntPopulation population1, Objects3DIntPopulation population2, double distMax, double colocMin) {
        this.distMax = distMax;
        this.pcColocMin = colocMin;
        this.colocalisation = new MeasurePopulationColocalisation(population1, population2);
        objectsMaxRadialDist1 = new HashMap<>(population1.getNbObjects());
        for (Object3DInt object3D : population1.getObjects3DInt()) {
            MeasureDistancesCenter distancesCenter = new MeasureDistancesCenter(object3D);
            objectsMaxRadialDist1.put(object3D, distancesCenter.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_PIX));
        }
        objectsMaxRadialDist2 = new HashMap<>(population2.getNbObjects());
        for (Object3DInt object3D : population2.getObjects3DInt()) {
            MeasureDistancesCenter distancesCenter = new MeasureDistancesCenter(object3D);
            objectsMaxRadialDist2.put(object3D, distancesCenter.getValueMeasurement(MeasureDistancesCenter.DIST_CENTER_MAX_PIX));
        }
    }

    @Override
    public double cost(Object3DInt object3D1, Object3DInt object3D2) {
        double V1 = object3D1.size();
        double V2 = object3D2.size();
        // check if coloc mode or distance mode
        if(distMax == 0){ // mode coloc
            double coloc = colocalisation.getValueObjectsPair(object3D1, object3D2);
            double pcColoc = coloc /  V1;
            if (pcColoc > pcColocMin) { // colocalised
                return (1.0 - (coloc / Math.max(V1, V2)));//  Max(V1,V2)
            }
        }
        else { // mode distance -> colocMin = 0
            Measure2Distance distance = new Measure2Distance(object3D1,object3D2);
            double distCC = distance.getValue(Measure2Distance.DIST_CC_PIX);
            double distCCMax = distMax + objectsMaxRadialDist1.get(object3D1) + objectsMaxRadialDist2.get(object3D2);
            if (distCC < distCCMax) {
                double dist = distance.getValue(Measure2Distance.DIST_BB_PIX);
                if (dist < distMax) return dist;
            }
        }

       return -1;
    }
}
