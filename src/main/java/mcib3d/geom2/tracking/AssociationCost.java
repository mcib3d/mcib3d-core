package mcib3d.geom2.tracking;

import mcib3d.geom2.Object3DInt;

public interface AssociationCost {
     double cost(Object3DInt object3D1, Object3DInt object3D2);
}
