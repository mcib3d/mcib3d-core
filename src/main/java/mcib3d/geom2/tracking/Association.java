package mcib3d.geom2.tracking;

import ij.measure.ResultsTable;
import mcib3d.geom.Object3D;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class Association {
    double[][] costs;
    Objects3DIntPopulation population1;
    Objects3DIntPopulation population2;
    AssociationCost associationCost;
    double NOASSO = 1000;
    // temp list
    Map<String, Double> CostsAll = null;
    Map<String, Double> CostsOK;
    HashMap<String, Double> CostsCompute;
    List<String> Orphan1;
    List<String> Orphan2;
    public boolean verbose = false;


    public Association(Objects3DIntPopulation population1, Objects3DIntPopulation population2, AssociationCost associationCost) {
        this.population1 = population1;
        this.population2 = population2;
        this.associationCost = associationCost;
    }

    public static float getValue1FromAsso(String asso) {
        int pos = asso.indexOf("-");
        if (pos < 0) return -1;
        String valS = asso.substring(0, pos);

        return Float.parseFloat(valS);
    }

    public static float getValue2FromAsso(String asso) {
        int pos = asso.indexOf("-");
        if (pos < 0) return -1;
        String valS = asso.substring(pos + 1);

        return Float.parseFloat(valS);
    }

    public static float[] getValues(String s) {
        String[] split = s.split("-");
        float val1 = Float.parseFloat(split[0]);
        float val2 = Float.parseFloat(split[1]);

        return new float[]{val1, val2};
    }

    @Deprecated
    public void drawAssociation(ImageHandler draw) {
        if (CostsAll == null) computeAssociation();

        float max = 0;
        for (String a : CostsOK.keySet()) {
            float[] vals = Association.getValues(a);
            float val1 = vals[0];
            float val2 = vals[1];
            Object3DInt object3D2 = population2.getObjectByValue(val2);
            //object3D2.setValue(val1);
            object3D2.drawObject(draw, val1);
            if (val1 > max) max = val1;
        }
        // orphan2
        for (String orphan : Orphan2) {
            float val = Float.parseFloat(orphan);
            max++;
            Object3DInt object3D2 = population2.getObjectByValue(val);
            //object3D2.setValue(max);
            object3D2.drawObject(draw, max);
        }
    }

    public List<AssociationPair> getAssociationPairs() {
        if (CostsAll == null) computeAssociation();

        List<AssociationPair> pairs = new LinkedList<>();

        for (String a : CostsOK.keySet()) {
            float[] values = Association.getValues(a);
            float val1 = values[0];
            float val2 = values[1];
            AssociationPair pair = new AssociationPair(population1.getObjectByValue(val1), population2.getObjectByValue(val2), CostsOK.get(a));
            pairs.add(pair);
        }

        return pairs;
    }

    @Deprecated
    public void drawAssociationPath(ImageHandler draw, ImageHandler path, ImageHandler track) {
        if (CostsAll == null) computeAssociation();

        // normal association
        for (String a : CostsOK.keySet()) {
            float[] values = Association.getValues(a);
            float val1 = values[0];
            float val2 = values[1];
            Object3DInt object3D2 = population2.getObjectByValue(val2);
            // get object in pop1 and value in path
            Object3DInt object3D1 = population1.getObjectByValue(val1);
            float pathValue = object3D1.getFirstVoxel().getValue();
            object3D2.drawObject(draw, pathValue);
        }
        // orphan2
        for (String orphan : Orphan2) {
            float val = Float.parseFloat(orphan);
            Object3DInt object3D2 = population2.getObjectByValue(val);
            float pathValue = object3D2.getFirstVoxel().getValue();
            object3D2.drawObject(draw, pathValue);
        }
    }

    @Deprecated
    public void drawOrphan1(ImageHandler draw) {
        if (CostsAll == null) computeAssociation();

        // orphan1
        for (String orphan : Orphan1) {
            float val = Float.parseFloat(orphan);
            Object3DInt object3D1 = population1.getObjectByValue(val);
            object3D1.drawObject(draw);
        }
    }

    @Deprecated
    public void drawOrphan2(ImageHandler draw) {
        if (CostsAll == null) computeAssociation();

        // orphan2
        for (String orphan : Orphan2) {
            float val = Float.parseFloat(orphan);
            Object3DInt object3D2 = population2.getObjectByValue(val);
            object3D2.drawObject(draw);
        }
    }

    @Deprecated
    public ResultsTable getAssociationTable() {
        ResultsTable rt = ResultsTable.getResultsTable();
        if (rt == null) rt = new ResultsTable();

        // normal asso
        int row = rt.getCounter();
        for (String s : CostsOK.keySet()) {
            Object3DInt object3D1 = getObject3D1fromAsso(s);
            Object3DInt object3D2 = getObject3D2fromAsso(s);
            rt.setValue("Label1", row, object3D1.getLabel());
            rt.setValue("Label2", row, object3D2.getLabel());
            rt.setValue("CostAsso", row, CostsOK.get(s));
            row++;
        }
        // orphan1
        row = rt.getCounter();
        for (String s : getOrphan1()) {
            rt.setValue("Label1", row, s);
            rt.setValue("Label2", row, 0);
            rt.setValue("CostAsso", row, 0);
            row++;
        }
        // orphan2
        row = rt.getCounter();
        for (String s : getOrphan2()) {
            rt.setValue("Label1", row, 0);
            rt.setValue("Label2", row, s);
            rt.setValue("CostAsso", row, 0);
            row++;
        }

        return rt;
    }

    public void computeAssociation() {
        CostsAll = new HashMap<>();
        CostsOK = new HashMap<>();
        CostsCompute = new HashMap<>();
        computePairCosts();
        associationOK();
        computeAssociationsCosts();

        // not associated 1
        Orphan1 = new LinkedList<>();
        for(Object3DInt object3DInt:population1.getObjects3DInt()) {
            String match = object3DInt.getLabel() + "-";
            boolean found = false;
            for (String s : CostsOK.keySet()) {
                if (s.startsWith(match)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                Orphan1.add(match.subSequence(0, match.length() - 1).toString());
            }
        }
        // not associated 2
        Orphan2 = new LinkedList<>();
        for(Object3DInt object3DInt:population2.getObjects3DInt()) {
            String match = "-" + object3DInt.getLabel();
            boolean found = false;
            for (String s : CostsOK.keySet()) {
                if (s.endsWith(match)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                Orphan2.add(match.subSequence(1, match.length()).toString());
            }
        }
    }

    public Map<String, Double> getAssociations() {
        if (CostsAll == null) computeAssociation();

        return CostsOK;
    }

    public Object3DInt getAssociated(Object3D object3D) {
        int val = object3D.getValue();
        for (String asso : getAssociations().keySet()) {
            if (asso.startsWith(val + "-")) {
                return getObject3D2fromAsso(asso);
            }
        }

        return null;
    }

    public Object3DInt getObject3D1fromAsso(String asso) {
        float val = getValue1FromAsso(asso);
        if (val > 0)
            return population1.getObjectByValue(val);
        else return null;
    }

    public Object3DInt getObject3D2fromAsso(String asso) {
        float val = getValue2FromAsso(asso);
        if (val > 0)
            return population2.getObjectByValue(val);
        else return null;
    }

    public List<String> getOrphan1() {
        if (CostsAll == null) computeAssociation();

        return Orphan1;
    }

    public Objects3DIntPopulation getOrphan1Population() {
        Objects3DIntPopulation pop = new Objects3DIntPopulation();

        for (String orphanS : getOrphan1()) {
            float val = Float.parseFloat(orphanS);
            Object3DInt object3D1 = population1.getObjectByValue(val);
            pop.addObject(object3D1);
        }

        return pop;
    }

    public List<String> getOrphan2() {
        if (CostsAll == null) computeAssociation();

        return Orphan2;
    }

    public Objects3DIntPopulation getOrphan2Population() {
        Objects3DIntPopulation pop = new Objects3DIntPopulation();

        for (String orphanS : getOrphan2()) {
            float val = Float.parseFloat(orphanS);
            Object3DInt object3D2 = population2.getObjectByValue(val);
            pop.addObject(object3D2);
        }

        return pop;
    }

    private void computePairCosts() {
        for(Object3DInt object3DInt1:population1.getObjects3DInt()){
            for(Object3DInt object3DInt2:population2.getObjects3DInt()){
                double cost = associationCost.cost(object3DInt1, object3DInt2);
                float val1 = object3DInt1.getLabel();
                float val2 = object3DInt2.getLabel();
                if (cost >= 0) {
                    CostsAll.put(val1 + "-" + val2, cost);
                }
            }
        }
    }

    private void associationOK() {
        for (String s : CostsAll.keySet()) {
            String[] split = s.split("-");
            Float val1 = Float.parseFloat(split[0]);
            Float val2 = Float.parseFloat(split[1]);
            // Nb asso for val1
            if (nbAsso1(val1) == 1) {
                if (nbAsso2(val2) == 1) {
                    CostsOK.put(s, CostsAll.get(s));
                } else {
                    CostsCompute.put(s, CostsAll.get(s));
                }
            }
            if (nbAsso1(val1) > 1) {
                CostsCompute.put(s, CostsAll.get(s));
            }
        }
    }

    private int nbAsso1(float val1) {
        int c = 0;
        for (String s : CostsAll.keySet()) {
            if (s.startsWith(val1 + "-")) c++;
        }

        return c;
    }

    private int nbAsso2(float val2) {
        int c = 0;
        for (String s : CostsAll.keySet()) {
            if (s.endsWith("-" + val2)) c++;
        }

        return c;
    }

    private void computeAssociationsCosts() {
        // find number of objects involved
        Map<Float, Integer> valuesIndices1 = new HashMap<>();
        Map<Float, Integer> valuesIndices2 = new HashMap<>();
        // read pairs
        int c1 = 0;
        int c2 = 0;
        for (String s : CostsCompute.keySet()) {
            String[] split = s.split("-");
            float val1 = Float.parseFloat(split[0]);
            float val2 = Float.parseFloat(split[1]);
            if (!valuesIndices1.containsKey(val1)) {
                valuesIndices1.put(val1, c1);
                c1++;
            }
            if (!valuesIndices2.containsKey(val2)) {
                valuesIndices2.put(val2, c2);
                c2++;
            }
        }
        int nb1 = valuesIndices1.size();
        int nb2 = valuesIndices2.size();
        if (nb1 * nb2 == 0) {

            return;
        }

        costs = new double[nb1][nb2];

        // init all values to impossible
        for (int i1 = 0; i1 < nb1; i1++) {
            for (int i2 = 0; i2 < nb2; i2++) {
                costs[i1][i2] = NOASSO;
            }
        }
        // read pairs
        for (String s : CostsCompute.keySet()) {
            String[] split = s.split("-");
            float val1 = Float.parseFloat(split[0]);
            float val2 = Float.parseFloat(split[1]);
            double cost = associationCost.cost(population1.getObjectByValue(val1), population2.getObjectByValue(val2));
            costs[valuesIndices1.get(val1)][valuesIndices2.get(val2)] = cost;
        }
        // do the association
        HungarianAlgorithm algorithm = new HungarianAlgorithm(costs);
        algorithm.execute();
        // get associated
        int[] associated = algorithm.getAssociations();
        for (String s : CostsAll.keySet()) {
            String[] split = s.split("-");
            float val1 = Float.parseFloat(split[0]);
            float val2 = Float.parseFloat(split[1]);
            for (int i = 0; i < associated.length; i++) {
                if ((valuesIndices1.containsKey(val1)) && (valuesIndices2.containsKey(val2))) {
                    int i1 = valuesIndices1.get(val1);
                    int i2 = valuesIndices2.get(val2);
                    double cost = costs[i1][i2];
                    if (cost != NOASSO) {
                        if ((i1 == i) && (i2 == associated[i]))
                            CostsOK.put(val1 + "-" + val2, cost);
                    }
                }
            }
        }
    }
}
