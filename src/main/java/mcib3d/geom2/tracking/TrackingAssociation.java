package mcib3d.geom2.tracking;

import ij.measure.ResultsTable;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;

import java.util.List;

public class TrackingAssociation {
    List<AssociationPair> finalAssociations = null;
    List<Object3DInt> finalOrphan1;
    List<Object3DInt> finalOrphan2;

    private ImageHandler img1;
    private ImageHandler img2;
    private ImageHandler path = null;
    private ImageHandler tracked = null;
    private ImageHandler pathed = null;

    private double distMaxCost = 10;
    private double pcMinCost = 0.0;

    private float maxLabel = 0; // to draw orphans

    public TrackingAssociation(ImageHandler img1, ImageHandler img2) {
        this.img1 = img1;
        this.img2 = img2;
    }

    public TrackingAssociation(ImageHandler img1, ImageHandler img2, double distMax, double pcMin) {
        this.img1 = img1;
        this.img2 = img2;
        distMaxCost = distMax;
        pcMinCost = pcMin;
    }

    public void setPathImage(ImageHandler path) {
        this.path = path;
    }

    public float getMaxLabel() {
        return maxLabel;
    }

    public void setMaxLabel(float maxLabel) {
        this.maxLabel = maxLabel;
    }

    public ImageHandler getTrackedImage() {
        if (finalAssociations == null) computeTracking();
        if (this.tracked == null) drawAssociation();

        return this.tracked;
    }

    public ImageHandler getPathedImage() {
        if (finalAssociations == null) computeTracking();
        if (this.path == null) return null;
        if (this.pathed == null) drawAssociation();

        return this.pathed;
    }

    public void setImage1(ImageHandler img1) {
        this.finalAssociations = null;
        this.img1 = img1;
        this.tracked = null;
    }

    public void setImage2(ImageHandler img2) {
        this.finalAssociations = null;
        this.img2 = img2;
        this.tracked = null;
    }


    private void computeTracking() {
        Objects3DIntPopulation population1 = new Objects3DIntPopulation(this.img1);
        Objects3DIntPopulation population2 = new Objects3DIntPopulation(this.img2);

        Association association = new Association(population1, population2, new CostColocalisationDistanceBB(population1, population2, distMaxCost, pcMinCost));
        association.verbose = false;

        association.computeAssociation();

        // final associations
        finalAssociations = association.getAssociationPairs();
        finalOrphan1 = association.getOrphan1Population().getObjects3DInt();
        finalOrphan2 = association.getOrphan2Population().getObjects3DInt();
    }

    private void drawAssociation() {
        if (finalAssociations == null) {
            computeTracking();
        }
        // create results
        this.tracked = this.img1.createSameDimensions();
        if (this.path != null) this.pathed = this.img1.createSameDimensions();
        // draw results
        float max = maxLabel;
        for (AssociationPair pair : finalAssociations) {
            float val1 = pair.getObject3D1().getLabel();
            //object3D2.setValue(val1);
            pair.getObject3D2().drawObject(this.tracked, val1);
            if (val1 > max) max = val1;
        }
        // orphan2
        for (Object3DInt object3D : finalOrphan2) {
            max++;
            object3D.drawObject(this.tracked, max);
        }
        // update maxLabel
        maxLabel = max;
    }

    public ResultsTable getResultsTableAssociation() {
        if (finalAssociations == null) computeTracking();
        // create results table
        ResultsTable table = new ResultsTable();
        // list associations
        int row = table.getCounter();
        for (AssociationPair pair : finalAssociations) {
            float val1 = pair.getObject3D1().getLabel();
            float val2 = pair.getObject3D2().getLabel();
            double cost = pair.getAsso();
            // table
            table.setValue("Label1", row, val1);
            table.setValue("Label2", row, val2);
            table.setValue("Cost", row, cost);
            row++;
        }
        // orphan1
        row = table.getCounter();
        for (Object3DInt object3D : finalOrphan1) {
            table.setValue("Label1", row, object3D.getLabel());
            table.setValue("Label2", row, 0);
            table.setValue("CostAsso", row, 0);
            row++;
        }
        // orphan2
        row = table.getCounter();
        for (Object3DInt object3D : finalOrphan2) {
            table.setValue("Label1", row, 0);
            table.setValue("Label2", row, object3D.getLabel());
            table.setValue("CostAsso", row, 0);
            row++;
        }

        return table;
    }
}