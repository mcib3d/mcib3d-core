package mcib3d.geom2;

import mcib3d.geom2.measurements.MeasureObject;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * The class for a population (list) of 3D objects.
 */
public class Objects3DIntPopulation {
    private final Map<Float, Object3DInt> objectsByLabel;
    private final List<Object3DInt> objects3DInt;
    private String name;
    private double voxelSizeXY = 1.0;
    private double voxelSizeZ = 1.0;
    private String unit = "pix";

    /**
     * Instantiates a new population from a labelled image.
     * Will extract all objects  as voxels having values > 0.
     * Do not check if objects are connex.
     *
     * @param handler the image
     */
    public Objects3DIntPopulation(ImageHandler handler) {
        name = handler.getTitle() + "_Population";
        voxelSizeXY = handler.getVoxelSizeXY();
        voxelSizeZ = handler.getVoxelSizeZ();
        unit = handler.getUnit();

        objects3DInt = new LinkedList<>();
        objectsByLabel = new HashMap<>();
        buildObjects(handler);
    }

    /**
     * Instantiates a new empty population.
     */
    public Objects3DIntPopulation() {
        objects3DInt = new LinkedList<>();
        objectsByLabel = new HashMap<>();
    }

    /**
     * Gets the voxel size XY set for this population.
     *
     * @return the voxel size in XY
     */
    public double getVoxelSizeXY() {
        return voxelSizeXY;
    }

    /**
     * Gets the voxel size XY set for this population.
     *
     * @param voxelSizeXY the voxel size in XY
     */
    public void setVoxelSizeXY(double voxelSizeXY) {
        this.voxelSizeXY = voxelSizeXY;
    }

    /**
     * Gets the voxel size in Z.
     *
     * @return the voxel size in Z
     */
    public double getVoxelSizeZ() {
        return voxelSizeZ;
    }

    /**
     * Sets the voxel size in Z.
     *
     * @param voxelSizeZ the voxel size in Z
     */
    public void setVoxelSizeZ(double voxelSizeZ) {
        this.voxelSizeZ = voxelSizeZ;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Gets the name of the population.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of the population.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the first object of the list of objects.
     *
     * @return the first object
     */
    public Object3DInt getFirstObject() {
        return objects3DInt.get(0);
    }

    /**
     * Gets an object specifying its label value.
     *
     * @param val the label value
     * @return the object
     */
    @Deprecated
    public Object3DInt getObjectByValue(float val) {
        return objectsByLabel.get(val);
    }

    /**
     * Gets an object specifying its label value.
     *
     * @param val the label value
     * @return the object
     */
    public Object3DInt getObjectByLabel(float val) {
        return objectsByLabel.get(val);
    }

    /**
     * Gets an object specifying its label value as a string.
     *
     * @param val the label value as a string
     * @return the object
     */
    @Deprecated
    public Object3DInt getObjectByValueString(String val) {
        return objectsByLabel.get(Float.valueOf(val));
    }

    /**
     * Gets an object specifying its label value as a string.
     *
     * @param val the label value as a string
     * @return the object
     */
    public Object3DInt getObjectByLabelString(String val) {
        return objectsByLabel.get(Float.valueOf(val));
    }

    private void buildObjects2(ImageHandler handler) {
        HashMap<Float, Integer> testValues = new HashMap<>();
        for (int z = 0; z < handler.sizeZ; z++)
            for (int y = 0; y < handler.sizeY; y++)
                for (int x = 0; x < handler.sizeX; x++) {
                    testValues.merge(handler.getPixel(x, y, z), 1, ((a, b) -> 1));
                }
        testValues.keySet().stream().filter(pix -> pix > 0)
                .forEach(pix -> {
                    Object3DInt object3DInt = new Object3DInt(handler, pix);
                    objects3DInt.add(object3DInt);
                    objectsByLabel.put(pix, object3DInt);
                });
    }

    private void buildObjects(ImageHandler handler) {
        double resXY = handler.getVoxelSizeXY();
        double resZ = handler.getVoxelSizeZ();
        Map<Float, Object3DInt> objects = new HashMap<>();
        for (int z = 0; z < handler.sizeZ; z++) {
            Map<Float, Object3DPlane> map = buildObjectsPlane(handler, z);
            for (Float val : buildObjectsPlane(handler, z).keySet()) {
                if (objects.containsKey(val)) {
                    // System.out.println("adding plane "+z+" to object "+val+" with size "+map.get(val).size());
                    objects.get(val).addPlane(map.get(val));
                } else { // new Object3D
                    // System.out.println("New object found at "+z+" with value "+val+" with size "+map.get(val).size());
                    Object3DInt object3DInt = new Object3DInt(val);
                    object3DInt.setVoxelSizeXY(resXY);
                    object3DInt.setVoxelSizeZ(resZ);
                    object3DInt.setName("Obj-" + val);
                    object3DInt.addPlane(map.get(val));
                    objects.put(val, object3DInt);
                    objectsByLabel.put(val, object3DInt);
                }
            }
            map.clear();
        }
        objects3DInt.addAll(objects.values());
        objects.clear();
    }

    /**
     * Gets measurements for all objects of the list.
     * The measurements are available in the package <i>geom2.measurements</i>.
     * The values returned here are: the label of the object, the value for the measurement.
     *
     * @param measurement the measurement
     * @return the measurements
     */
    public List<Double[]> getMeasurements(String measurement) {
        return getObjects3DInt().stream()
                .map(obj -> new Double[]{(double) obj.getLabel(), new MeasureObject(obj).measure(measurement)})
                .collect(Collectors.toList());
    }

    /**
     * Gets measurements for all objects of the list.
     * The measurements are available in the package <i>geom2.measurements</i>.
     * Here they are specified as a list of measurements
     * The values returned here are the value for the measurements.
     * The label value is not added to the measurements.
     *
     * @param measurements the list of measurement
     * @return the measurements
     */
    public List<Double[]> getMeasurementsList(String[] measurements) {
        return getObjects3DInt().stream()
                .map(obj -> new MeasureObject(obj).measureList(measurements))
                .collect(Collectors.toList());
    }

    /**
     * Gets intensity measurements for all objects of the list, for the specified image.
     * The measurements are available in the package <i>geom2.measurements</i>.
     * The values returned here are: the label of the object, the value for the measurement.
     *
     * @param measurement the measurement
     * @param handler     the image
     * @return the measurements
     */
    public List<Double[]> getMeasurementsIntensity(String measurement, ImageHandler handler) {
        return getObjects3DInt().stream()
                .map(obj -> new Double[]{(double) obj.getLabel(), new MeasureObject(obj).measureIntensity(measurement, handler)})
                .collect(Collectors.toList());
    }

    /**
     * Gets intensity measurements for all objects of the list,  for the specified image.
     * The measurements are available in the package <i>geom2.measurements</i>.
     * Here they are specified as a list of measurements
     * The values returned here are the value for the measurements.
     * The label value is not added to the measurements.
     *
     * @param measurements the list of measurement
     * @param handler      the image
     * @return the measurements
     */
    public List<Double[]> getMeasurementsIntensityList(String[] measurements, ImageHandler handler) {
        return getObjects3DInt().stream()
                .map(obj -> new MeasureObject(obj).measureIntensityList(measurements, handler))
                .collect(Collectors.toList());
    }

    /**
     * Gets the list of 3D objects.
     *
     * @return the 3D objects
     */
    public List<Object3DInt> getObjects3DInt() {
        return objects3DInt;
    }

    /**
     * Gets the number of objects in the population.
     *
     * @return the number of objects
     */
    public int getNbObjects() {
        return objects3DInt.size();
    }

    /**
     * Draw all the objects of the population in the specified image.
     * Each object will be drawn with its label value.
     *
     * @param image the image
     */
    public void drawInImage(ImageHandler image) {
        objects3DInt.forEach(object3DInt -> object3DInt.drawObject(image));
    }

    /**
     * Draw all the objects of the population in an image.
     * Each object will be drawn with its label value.
     *
     * @return the image
     */
    public ImageHandler drawImage() {
        ImageHandler handler;
        BoundingBox box = findEnclosingBoundingBox();

        if (getMaxLabel() > Math.pow(2, 16))
            handler = new ImageFloat("population", box.xmax + 1, box.ymax + 1, box.zmax + 1);
        else
            handler = new ImageShort("population", box.xmax + 1, box.ymax + 1, box.zmax + 1);

        drawInImage(handler);

        return handler;
    }

    /**
     * Gets the maximum value of labels for the objects of the population.
     *
     * @return the maximum label value
     */
    public float getMaxLabel() {
        if (getNbObjects() == 0) return 0;
        return objects3DInt.stream().map(Object3DInt::getLabel).reduce(Math::max).get();
    }

    /**
     * Reset labels of the objects
     */
    public void resetLabels() {
        AtomicReference<Float> labelObject = new AtomicReference<>(0.0F);
        objectsByLabel.clear();
        getObjects3DInt().forEach(obj -> {
            float label = labelObject.updateAndGet(f -> f + 1);
            obj.setLabel(label);
            objectsByLabel.put(label, obj);
        });
    }

    /**
     * Find minimal enclosing bounding box for all objects in the population.
     *
     * @return the bounding box
     */
    public BoundingBox findEnclosingBoundingBox() {
        BoundingBox box = new BoundingBox();
        objects3DInt.forEach(object3DInt -> box.adjustBounding(object3DInt.getBoundingBox()));

        return box;
    }

    private Map<Float, Object3DPlane> buildObjectsPlane(ImageHandler handler, int z) {
        Map<Float, Object3DPlane> map = new HashMap<>();

        for (int i = 0; i < handler.sizeX; i++) {
            for (int j = 0; j < handler.sizeY; j++) {
                float pix = handler.getPixel(i, j, z);
                if (pix > 0) {
                    if (map.containsKey(pix)) {
                        map.get(pix).addVoxel(new VoxelInt(i + handler.offsetX, j + handler.offsetY, z + handler.offsetZ, pix));
                    } else {
                        Object3DPlane plane = new Object3DPlane(z);
                        plane.addVoxel(new VoxelInt(i + handler.offsetX, j + handler.offsetY, z + handler.offsetZ, pix));
                        map.put(pix, plane);
                    }
                }
            }
        }

        return map;
    }

    /**
     * Add one object to the population
     *
     * @param object3D the object
     */
    public void addObject(Object3DInt object3D) {
        objects3DInt.add(object3D);
        if (objectsByLabel.containsKey(object3D.getLabel()))
            System.out.println("WARNING: Label " + object3D.getLabel() + " already exists in population");
        objectsByLabel.put(object3D.getLabel(), object3D);
    }

    public void addObject(Object3DInt object3D, float label) {
        addObject(object3D, label, false);
    }

    public void addObject(Object3DInt object3D, float label, boolean testLabel) {
        object3D.setLabel(label);
        objects3DInt.add(object3D);
        if (testLabel && objectsByLabel.containsKey(object3D.getLabel()))
            System.out.println("WARNING: Label " + object3D.getLabel() + " already exists in population");
        objectsByLabel.put(object3D.getLabel(), object3D);
    }

    public void addObjects(List<Object3DInt> objects) {
        addObjects(objects, true);
    }

    public void addObjects(List<Object3DInt> objects, boolean testLabel) {
        objects.forEach(obj -> addObject(obj, obj.getLabel(), testLabel));
    }

    public void addImageLabel(ImageHandler handler) {
        voxelSizeXY = handler.getVoxelSizeXY();
        voxelSizeZ = handler.getVoxelSizeZ();
        buildObjects(handler);
    }

    /**
     * Remove one object from the population.
     *
     * @param object3DInt the object
     */
    public void removeObject(Object3DInt object3DInt) {
        if (object3DInt == null) return;
        objects3DInt.remove(object3DInt);
        objectsByLabel.remove(object3DInt.getLabel());
    }

    /**
     * Remove one object from the popualtion, specifying its label.
     *
     * @param label the label
     */
    public void removeObject(float label) {
        removeObject(getObjectByValue(label));
    }

    /**
     * Translate all objects of the population.
     *
     * @param tx the translation value in x
     * @param ty the translation value in y
     * @param tz the translation value in z
     */
    public void translateObjects(int tx, int ty, int tz) {
        getObjects3DInt().forEach(obj -> obj.translate(tx, ty, tz));
    }

    /**
     * Load list of objects from a file and add them to the population.
     *
     * @param path the path
     * @return true if the objects were correctly loaded
     */
    public boolean loadObjects(String path) {
        //ImagePlus plus = this.getImage();
        ZipInputStream zipinputstream;
        ZipEntry zipentry;
        FileOutputStream fileoutputstream;
        int n;
        byte[] buf = new byte[1024];
        File file;
        Object3DInt obj;
        File f = new File(path);
        String dir = f.getParent();
        String fs = File.separator;
        try {
            zipinputstream = new ZipInputStream(Files.newInputStream(Paths.get(path)));
            zipentry = zipinputstream.getNextEntry();
            while (zipentry != null) {
                //for each entry to be extracted
                String entryName = zipentry.getName();
                //System.out.println("Dezipping "+entryName);
                fileoutputstream = new FileOutputStream(dir + fs + entryName);
                file = new File(dir + fs + entryName);
                while ((n = zipinputstream.read(buf, 0, 1024)) > -1) {
                    fileoutputstream.write(buf, 0, n);
                }
                fileoutputstream.close();
                zipinputstream.closeEntry();
                zipentry = zipinputstream.getNextEntry();
                // create object
                Object3DIntLoader loader = new Object3DIntLoader();
                // entryName = entryName.replace("-3droi.zip", "");
                obj = loader.loadObject(dir, entryName, false);
                addObject(obj);

                file.delete();
            }
            zipinputstream.close();
        } catch (IOException ex) {
            System.out.println(ex);
            return false;
        }

        return true;
    }

    /**
     * Save list of objects into a file.
     *
     * @param path the path
     * @return true if the objects were correctly saved
     */
    public boolean saveObjects(String path) {
        File ff = new File(path);
        final String dir = ff.getParent();
        final String fs = File.separator;
        final String ext = ".3droi";

        byte[] buf = new byte[1024];
        ZipOutputStream zip;
        //  ZIP
        try {
            zip = new ZipOutputStream(Files.newOutputStream(Paths.get(path)));
            objects3DInt.forEach(obj -> {
                // save object in zip file first
                String name = obj.getName();
                Object3DIntSaver saver = new Object3DIntSaver(obj);
                saver.saveObject(dir, name + ext, false);
                File file = new File(dir + fs + name + ext);
                FileInputStream in;
                // add this file to the zip
                try {
                    int len;
                    in = new FileInputStream(file);
                    zip.putNextEntry(new ZipEntry(name + ext));
                    while ((len = in.read(buf)) > 0) {
                        zip.write(buf, 0, len);
                    }
                    // close
                    zip.closeEntry();
                    in.close();
                } catch (IOException e) {
                    System.out.println("Pb saving population " + file.getAbsolutePath());
                    System.out.println(e.getMessage());
                }
                // delete temporary saved object file
                file.delete();
            });
            zip.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }
}
