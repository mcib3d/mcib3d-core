package mcib3d.geom2;

import mcib3d.geom.Point3DInt;

import java.util.Objects;

/**
 * Basic type for 3D Voxel with Integer coordinates
 */
public class VoxelInt extends Point3DInt {
    private float value;
    private double extra;

    /**
     * Instantiates a new Voxel int.
     */
    public VoxelInt() {
        super();
        value = 0;
    }

    /**
     * Instantiates a new Voxel int.
     *
     * @param x     the x coordinate
     * @param y     the y coordinate
     * @param z     the z coordinate
     * @param value the value of the voxel
     */
    public VoxelInt(int x, int y, int z, float value) {
        super(x, y, z);
        this.value = value;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public float getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(float value) {
        this.value = value;
    }

    /**
     * Gets extra value, for misc computation
     *
     * @return the extra
     */
    public double getExtra() {
        return extra;
    }

    /**
     * Sets extra value, for misc computation
     *
     * @param extra the extra
     */
    public void setExtra(double extra) {
        this.extra = extra;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoxelInt voxelInt = (VoxelInt) o;
        return getX() == voxelInt.getX() && getY() == voxelInt.getY() && getZ() == voxelInt.getZ();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getX(), getY(), getZ());
    }

    @Override
    public String toString() {
        return "VoxelInt{" +
                "x=" + getX() +
                ", y=" + getY() +
                ", z=" + getZ() +
                ", value=" + value +
                '}';
    }
}
