package mcib3d.geom2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * The class to load a 3D object from file.
 */
public class Object3DIntLoader {
    private boolean version4 = false;

    /**
     * Instantiates a new loader.
     */
    public Object3DIntLoader() {
    }

    /**
     * Load a 3D object from file.
     *
     * @param dir  the directory for the file
     * @param name the name of the file
     * @return the 3D object
     */
    public Object3DInt loadObject(String dir, String name, boolean zip) {
        Object3DInt object3DInt = new Object3DInt();
        object3DInt.setName(name.replace(".3droi",""));
        try {
            File file;
            if (zip) file = unzipFile(dir, name);
            else file = new File(dir+File.separator+name);
            List<String> lines = Files.readAllLines(file.toPath());
            loadInfo(object3DInt, lines); // read info, including version
            if (version4) {
                loadObjectVersion4(object3DInt, lines);
            } else {
                loadObjectVersion3(object3DInt,lines);
            }
            // delete unzipped
            Files.delete(Paths.get(dir, name));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return object3DInt;
    }

    private void loadObjectVersion4(Object3DInt object3DInt, List<String> lines) {
        final AtomicReference<Object3DPlane> plane = new AtomicReference<>(null);
        // read voxels in planes
        lines.stream().filter(line -> !line.contains("=")).filter(line -> !line.isEmpty())
                .forEach(line -> {
                    if (line.startsWith("plane")) {
                        String[] data = line.split(" ");
                        if (plane.get() == null) {
                            plane.set(new Object3DPlane(Integer.parseInt(data[1].trim())));
                        } else {
                            if (!plane.get().isEmpty()) {
                                object3DInt.addPlane(plane.get());
                                plane.set(new Object3DPlane(Integer.parseInt(data[1].trim())));
                            }
                        }
                    } else {
                        String[] data = line.split("\t");
                        int x = Integer.parseInt(data[0]);
                        int y = Integer.parseInt(data[1]);
                        int z = Integer.parseInt(data[2]);
                        float v = Float.parseFloat(data[3]);
                        plane.get().addVoxel(new VoxelInt(x, y, z, v));
                    }
                });
        // add final plane to object
        object3DInt.addPlane(plane.get());
    }

    private void loadObjectVersion3(Object3DInt object3DInt, List<String> lines) {
        final Map<Integer,Object3DPlane> planes = new HashMap<>();
        // read voxels in planes
        lines.stream().filter(line -> !line.contains("=")).filter(line -> !line.isEmpty())
                .forEach(line -> {
                    String[] data = line.split("\t");
                    int nb = (int) Float.parseFloat(data[0]);
                    int x = (int) Float.parseFloat(data[1]);
                    int y = (int) Float.parseFloat(data[2]);
                    int z = (int) Float.parseFloat(data[3]);
                    float v = Float.parseFloat(data[4]);
                    if(!planes.containsKey(z)) planes.put(z,new Object3DPlane(z));
                    planes.get(z).addVoxel(new VoxelInt(x, y, z, v));
                });
        // add  planes to object
        planes.values().forEach(object3DInt::addPlane);
    }

    private void loadInfo(Object3DInt object3DInt, List<String> lines) throws IOException {
        lines.stream().filter(line -> line.startsWith("cal=") || line.startsWith("comment=") || line.startsWith("type=") || line.startsWith("value=") || line.startsWith("version="))
                .forEach(line -> {
                    String[] coord = line.split("\t");
                    switch (coord[0]) {
                        case "version=":
                            version4 = true;
                            break;
                        case "cal=":
                            object3DInt.setVoxelSizeXY(Double.parseDouble(coord[1]));
                            object3DInt.setVoxelSizeZ(Double.parseDouble(coord[2]));
                            object3DInt.setUnit(coord[3]);
                            break;
                        case "comment=":
                            object3DInt.setComment(coord[1]);
                            break;
                        case "type=":
                            object3DInt.setType(Integer.parseInt(coord[1]));
                            break;
                        case "value=":
                            object3DInt.setLabel(Float.parseFloat(coord[1]));
                            if (object3DInt.getLabel() == 0) {
                                object3DInt.setLabel(1);
                            }
                            break;
                    }
                });
    }

    private File unzipFile(String dir, String name) {
        final String fs = File.separator;
        ZipInputStream zipinputstream;
        ZipEntry zipentry;
        FileOutputStream fileoutputstream;
        int n;
        byte[] buf = new byte[1024];
        File file = null;
        try {
            zipinputstream = new ZipInputStream(new FileInputStream(dir + fs + name + "-3droi.zip"));
            zipentry = zipinputstream.getNextEntry();
            if (zipentry != null) {
                //for each entry to be extracted
                String entryName = zipentry.getName();
                //IJ.log("entryname=" + entryName);
                fileoutputstream = new FileOutputStream(dir + fs + entryName);
                file = new File(dir + fs + entryName);
                while ((n = zipinputstream.read(buf, 0, 1024)) > -1) {
                    fileoutputstream.write(buf, 0, n);
                }
                fileoutputstream.close();
                zipinputstream.closeEntry();
            }
            zipinputstream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return file;
    }
}
