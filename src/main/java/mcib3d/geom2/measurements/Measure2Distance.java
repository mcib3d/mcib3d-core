package mcib3d.geom2.measurements;

import mcib3d.geom.Voxel3D;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.VoxelInt;
import mcib3d.utils.KDTreeC;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class Measure2Distance extends Measure2Abstract {
    public final static String DIST_CC_PIX = "DistCenterCenterPix";
    public final static String DIST_CC_UNIT = "DistCenterCenterUnit";
    public final static String DIST_CB_PIX = "DistCenterBorderPix";
    public final static String DIST_CB_UNIT = "DistCenterBorderUnit";
    public final static String DIST_BB_PIX = "DistBorderBorderPix";
    public final static String DIST_BB_UNIT = "DistBorderBorderUnit";
    public final static String DIST_HAUSDORFF_PIX = "DistHausdorffPix";
    public final static String DIST_HAUSDORFF_UNIT = "DistHausdorffUnit";
    public final static String DIST_MEANMINDIST_PIX = "DistMeanMinPix";
    public final static String DIST_MEANMINDIST_UNIT = "DistMeanMinUnit";
    private Voxel3D centroid1 = null;
    private Voxel3D centroid2 = null;
    private VoxelInt centerBorderPix = null;
    private VoxelInt centerBorderUnit = null;

    private VoxelInt border1Pix = null;
    private VoxelInt border2Pix = null;
    private VoxelInt border1Unit = null;
    private VoxelInt border2Unit = null;

    private VoxelInt hausdorff1Pix = null;
    private VoxelInt hausdorff2Pix = null;
    private VoxelInt hausdorff1Unit = null;
    private VoxelInt hausdorff2Unit = null;

    private double distMeanMin = -1;

    private boolean computeBBPix = false;
    private boolean computeBBUnit = false;

    public Measure2Distance(Object3DInt object3D1, Object3DInt object3D2) {
        super(object3D1, object3D2);
    }

    public Measure2Distance() {
    }

    @Override
    protected String[] getNames() {
        return new String[]{DIST_CC_PIX, DIST_CC_UNIT, DIST_BB_PIX, DIST_BB_UNIT, DIST_CB_PIX, DIST_CB_UNIT, DIST_HAUSDORFF_PIX, DIST_HAUSDORFF_UNIT, DIST_MEANMINDIST_PIX, DIST_MEANMINDIST_UNIT};
    }

    @Override
    protected void computeAll() {
        keysValues.put(DIST_CC_PIX, distanceCCPix());
        keysValues.put(DIST_CC_UNIT, distanceCCUnit());
        keysValues.put(DIST_BB_PIX, distanceBBPix());
        keysValues.put(DIST_BB_UNIT, distanceBBUnit());
        keysValues.put(DIST_CB_PIX, distanceCBPix());
        keysValues.put(DIST_CB_UNIT, distanceCBUnit());
        keysValues.put(DIST_HAUSDORFF_PIX, distanceHausdorffPix());
        keysValues.put(DIST_HAUSDORFF_UNIT, distanceHausdorffUnit());
        keysValues.put(DIST_MEANMINDIST_PIX, distanceMeanMinPix());
        keysValues.put(DIST_MEANMINDIST_UNIT, distanceMeanMinUnit());
    }

    @Override
    protected void compute(String measure) {
        switch (measure) {
            case DIST_CC_PIX:
                keysValues.put(DIST_CC_PIX, distanceCCPix());
            case DIST_CC_UNIT:
                keysValues.put(DIST_CC_UNIT, distanceCCUnit());
            case DIST_CB_PIX:
                keysValues.put(DIST_CB_PIX, distanceCBPix());
            case DIST_CB_UNIT:
                keysValues.put(DIST_CB_UNIT, distanceCBUnit());
            case DIST_BB_PIX:
                keysValues.put(DIST_BB_PIX, distanceBBPix());
            case DIST_BB_UNIT:
                keysValues.put(DIST_BB_UNIT, distanceBBUnit());
            case DIST_HAUSDORFF_PIX:
                keysValues.put(DIST_HAUSDORFF_PIX, distanceHausdorffPix());
            case DIST_HAUSDORFF_UNIT:
                keysValues.put(DIST_HAUSDORFF_UNIT, distanceHausdorffUnit());
            case DIST_MEANMINDIST_PIX:
                keysValues .put(DIST_MEANMINDIST_PIX, distanceMeanMinPix());
            case DIST_MEANMINDIST_UNIT:
                keysValues .put(DIST_MEANMINDIST_UNIT, distanceMeanMinUnit());    
        }
    }

    @Deprecated
    public double getValue(String measure) {
        switch (measure) {
            case DIST_CC_PIX:
                return distanceCCPix();
            case DIST_CC_UNIT:
                return distanceCCUnit();
            case DIST_CB_PIX:
                return distanceCBPix();
            case DIST_CB_UNIT:
                return distanceCBUnit();
            case DIST_BB_PIX:
                return distanceBBPix();
            case DIST_BB_UNIT:
                return distanceBBUnit();
            case DIST_HAUSDORFF_PIX:
                return distanceHausdorffPix();
            case DIST_HAUSDORFF_UNIT:
                return distanceHausdorffUnit();
            default:
                return Double.NaN;
        }
    }

    private double distanceCCPix() {
        if (centroid2 == null) {
            computeCentroids();
        }

        return centroid1.distance(centroid2);
    }

    private double distanceCCUnit() {
        if (centroid2 == null) {
            computeCentroids();
        }

        return centroid1.distance(centroid2, object3DInt1.getVoxelSizeXY(), object3DInt1.getVoxelSizeZ());
    }

    private double distanceCBPix() {
        if (centroid1 == null) computeCentroid1();
        if (centerBorderPix == null) centerBorderPix = computeCB(false);

        return centerBorderPix.distance(centroid1);
    }

    private double distanceCBUnit() {
        if (centroid1 == null) computeCentroid1();
        if (centerBorderUnit == null) centerBorderUnit = computeCB(true);

        return centerBorderUnit.distanceUnitScaled(centroid1, object3DInt1.getVoxelSizeXY(), object3DInt1.getVoxelSizeZ());
    }

    private void computeCentroids() {
        MeasureCentroid centre1 = new MeasureCentroid(object3DInt1);
        MeasureCentroid centre2 = new MeasureCentroid(object3DInt2);

        centroid1 = centre1.getCentroidAsVoxel();
        centroid2 = centre2.getCentroidAsVoxel();
    }

    private void computeCentroid1() {
        MeasureCentroid centre1 = new MeasureCentroid(object3DInt1);
        centroid1 = centre1.getCentroidAsVoxel();
    }

    private void setBordersPoints(VoxelInt[] borders, boolean useUnit) {
        if (useUnit) {
            border1Unit = borders[0];
            border2Unit = borders[1];
            hausdorff1Unit = borders[2];
            hausdorff2Unit = borders[3];
        } else {
            border1Pix = borders[0];
            border2Pix = borders[1];
            hausdorff1Pix = borders[2];
            hausdorff2Pix = borders[3];
        }
    }

    private double distanceBBPix() {
        if (!computeBBPix) setBordersPoints(computeBB(false), false);

        return border1Pix.distance(border2Pix);
    }

    private double distanceBBUnit() {
        if (!computeBBUnit) setBordersPoints(computeBB(true), true);

        return border1Unit.distanceUnitScaled(border2Unit, object3DInt1.getVoxelSizeXY(), object3DInt1.getVoxelSizeZ());
    }

    private double distanceHausdorffPix() {
        if (!computeBBPix) setBordersPoints(computeBB(false), false);

        return hausdorff1Pix.distance(hausdorff2Pix);
    }

    private double distanceHausdorffUnit() {
        if (!computeBBUnit) setBordersPoints(computeBB(true), true);

        return hausdorff1Unit.distanceUnitScaled(hausdorff2Unit, object3DInt1.getVoxelSizeXY(), object3DInt1.getVoxelSizeZ());
    }

    private Double distanceMeanMinUnit() {
         if (!computeBBPix) distMeanMin = computeBBMean(true);
 
         return distMeanMin;
    }

    private Double distanceMeanMinPix() {
        if (!computeBBPix) distMeanMin = computeBBMean(false);

        return distMeanMin;
    }

    private VoxelInt computeCB(boolean useUnit) {
        Voxel3D centroid = new Voxel3D(centroid1);
        // unit
        double resXY = 1;
        double resZ = 1;
        if (useUnit) {
            resXY = object3DInt1.getVoxelSizeXY();
            resZ = object3DInt1.getVoxelSizeZ();
            centroid.scale(resXY, resXY, resZ);
        }
        // kdTree for object2
        Object3DComputation computation2 = new Object3DComputation(object3DInt2);
        KDTreeC kdTreeC2 = new KDTreeC(3);
        kdTreeC2.setScale3(resXY, resXY, resZ);
        computation2.getContour().forEach(V2 -> kdTreeC2.add(new double[]{V2.getX(), V2.getY(), V2.getZ()}, V2));
        KDTreeC.Item item = kdTreeC2.getNearestNeighbor(new double[]{centroid.getX(), centroid.getY(), centroid.getZ()}, 1)[0];

        return (VoxelInt) item.obj;
    }

    private VoxelInt[] computeBB(boolean useUnit) {
        if (useUnit) computeBBUnit = true;
        else computeBBPix = true;
        // distance BB
        AtomicReference<Double> distMax = new AtomicReference<>(Double.POSITIVE_INFINITY);
        AtomicReference<VoxelInt> Vn1 = new AtomicReference<>();
        AtomicReference<KDTreeC.Item> Vn2 = new AtomicReference<>();
        // hausdorff
        AtomicReference<Double> distMin = new AtomicReference<>(Double.NEGATIVE_INFINITY);
        AtomicReference<VoxelInt> Wn1 = new AtomicReference<>();
        AtomicReference<KDTreeC.Item> Wn2 = new AtomicReference<>();
        // unit
        double resXY = 1;
        double resZ = 1;
        if (useUnit) {
            resXY = object3DInt1.getVoxelSizeXY();
            resZ = object3DInt1.getVoxelSizeZ();
        }
        // kdTree for object2
        Object3DComputation computation1 = new Object3DComputation(object3DInt1);
        Object3DComputation computation2 = new Object3DComputation(object3DInt2);
        KDTreeC kdTreeC2 = new KDTreeC(3);
        kdTreeC2.setScale3(resXY, resXY, resZ);
        computation2.getContour().forEach(V2 -> kdTreeC2.add(new double[]{V2.getX(), V2.getY(), V2.getZ()}, V2));
        computation1.getContour().forEach(V1 -> {
            KDTreeC.Item item = kdTreeC2.getNearestNeighbor(new double[]{V1.getX(), V1.getY(), V1.getZ()}, 1)[0];
            // distance BB
            if (item.distanceSq < distMax.get()) {
                distMax.set(item.distanceSq);
                Vn1.set(V1);
                Vn2.set(item);
            }
            // Hausdorff
            if (item.distanceSq > distMin.get()) {
                distMin.set(item.distanceSq);
                Wn1.set(V1);
                Wn2.set(item);
            }
        });

        return new VoxelInt[]{Vn1.get(), (VoxelInt) Vn2.get().obj, Wn1.get(), (VoxelInt) Wn2.get().obj};
    }

    private double computeBBMean(boolean useUnit) {
        if (useUnit) computeBBUnit = true;
        else computeBBPix = true;
        // MeanMin
        double sumMeanMin = 0;
        // unit
        double resXY = 1;
        double resZ = 1;
        if (useUnit) {
            resXY = object3DInt1.getVoxelSizeXY();
            resZ = object3DInt1.getVoxelSizeZ();
        }
        // kdTree for object2
        Object3DComputation computation1 = new Object3DComputation(object3DInt1);
        Object3DComputation computation2 = new Object3DComputation(object3DInt2);
        KDTreeC kdTreeC2 = new KDTreeC(3);
        kdTreeC2.setScale3(resXY, resXY, resZ);
        List<VoxelInt> Cont1 = computation1.getContour();
        List<VoxelInt> Cont2 = computation2.getContour();
        Cont2.forEach(V2 -> kdTreeC2.add(new double[]{V2.getX(), V2.getY(), V2.getZ()}, V2));
        sumMeanMin = Cont1.stream()
                .map(V1 -> kdTreeC2.getNearestNeighbor(new double[]{V1.getX(), V1.getY(), V1.getZ()}, 1)[0])
                .mapToDouble(it -> Math.sqrt(it.distanceSq))
                .sum();

        return sumMeanMin / Cont1.size();
    }

    public VoxelInt getCenterBorderPix() {
        if (centerBorderPix == null) centerBorderPix = computeCB(false);

        return centerBorderPix;
    }

    public VoxelInt getCenterBorderUnit() {
        if (centerBorderUnit == null) centerBorderUnit = computeCB(false);

        return centerBorderUnit;
    }


    public VoxelInt getBorder1Pix() {
        if (!computeBBPix) setBordersPoints(computeBB(false), false);

        return border1Pix;
    }

    public VoxelInt getBorder2Pix() {
        if (!computeBBPix) setBordersPoints(computeBB(false), false);

        return border2Pix;
    }

    public VoxelInt getBorder1Unit() {
        if (!computeBBUnit) setBordersPoints(computeBB(true), true);

        return border1Unit;
    }

    public VoxelInt getBorder2Unit() {
        if (!computeBBUnit) setBordersPoints(computeBB(true), true);

        return border2Unit;
    }

    public VoxelInt getHausdorff1Pix() {
        if (!computeBBPix) setBordersPoints(computeBB(false), false);

        return hausdorff1Pix;
    }

    public VoxelInt getHausdorff2Pix() {
        if (!computeBBPix) setBordersPoints(computeBB(false), false);

        return hausdorff2Pix;
    }

    public VoxelInt getHausdorff1Unit() {
        if (!computeBBUnit) setBordersPoints(computeBB(true), true);

        return hausdorff1Unit;
    }

    public VoxelInt getHausdorff2Unit() {
        if (!computeBBUnit) setBordersPoints(computeBB(true), true);

        return hausdorff2Unit;
    }
}
