package mcib3d.geom2.measurements;

import mcib3d.geom.Point3D;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DPlane;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

public class MeasureCenterOfMass extends MeasureAbstract {
    public static final String LABEL_OBJ = MeasureAbstract.LABEL_OBJ;
    public static final String MASS_CENTER_X_PIX = "CMX(pix)";
    public static final String MASS_CENTER_Y_PIX = "CMY(pix)";
    public static final String MASS_CENTER_Z_PIX = "CMZ(pix)";
    public static final String MASS_CENTER_X_UNIT = "CMX(unit)";
    public static final String MASS_CENTER_Y_UNIT = "CMY(unit)";
    public static final String MASS_CENTER_Z_UNIT = "CMZ(unit)";

    private ImageHandler intensityImage = null;

    public MeasureCenterOfMass(Object3DInt object3DInt, ImageHandler intensityImage) {
        super(object3DInt);
        this.intensityImage = intensityImage;
    }

    public MeasureCenterOfMass(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureCenterOfMass() {
    }

    public void setIntensityImage(ImageHandler image) {
        this.intensityImage = image;
        computeAll();
    }

    @Override
    protected String[] getNames() {
        return new String[]{MASS_CENTER_X_PIX, MASS_CENTER_Y_PIX, MASS_CENTER_Z_PIX, MASS_CENTER_X_UNIT, MASS_CENTER_Y_UNIT, MASS_CENTER_Z_UNIT};
    }

    @Override
    protected void computeAll() {
        if (intensityImage == null) return;

        Double[] sums = computeIntensityValues(intensityImage);

        keysValues.put(MASS_CENTER_X_PIX, sums[0]);
        keysValues.put(MASS_CENTER_Y_PIX, sums[1]);
        keysValues.put(MASS_CENTER_Z_PIX, sums[2]);

        double resXY = object3DInt.getResXY();
        double resZ = object3DInt.getResZ();
        keysValues.put(MASS_CENTER_X_UNIT, sums[0] * resXY);
        keysValues.put(MASS_CENTER_Y_UNIT, sums[1] * resXY);
        keysValues.put(MASS_CENTER_Z_UNIT, sums[2] * resZ);
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }

    public Point3D getCentroidAsPoint() {
        return new Point3D(getValueMeasurement(MASS_CENTER_X_PIX), getValueMeasurement(MASS_CENTER_Y_PIX), getValueMeasurement(MASS_CENTER_Z_PIX));
    }

    public Voxel3D getCentroidAsVoxel() {
        Point3D point3D = getCentroidAsPoint();
        return new Voxel3D(point3D.getX(), point3D.getY(), point3D.getZ(), object3DInt.getLabel());
    }

    public VoxelInt getCentroidRoundedAsVoxelInt() {
        Point3D voxel3D = getCentroidAsPoint();
        return new VoxelInt(voxel3D.getRoundX(), voxel3D.getRoundY(), voxel3D.getRoundZ(), object3DInt.getLabel());
    }

    private Double[] computeIntensityValues(ImageHandler ima) {
        if (ima == null) return new Double[]{Double.NaN, Double.NaN, Double.NaN, Double.NaN};

        double sumx = 0, sumy = 0, sumz = 0, sum = 0;
        Double[] values = new Double[]{sumx, sumy, sumz, sum};

        object3DInt.getObject3DPlanes().forEach(plane -> updateIntensityValues(plane, values));
        sumx = values[0];
        sumy = values[1];
        sumz = values[2];
        sum = values[3];

        return new Double[]{sumx / sum, sumy / sum, sumz / sum};
    }

    private void updateIntensityValues(Object3DPlane plane, Double[] values) {
        plane.getVoxels().forEach(V -> {
            float pix = intensityImage.getPixel(V.getX(), V.getY(), V.getZ());
            if (!Float.isNaN(pix)) {
                values[0] += pix * V.getX();
                values[1] += pix * V.getY();
                values[2] += pix * V.getZ();
                values[3] += pix;
            }
        });
    }
}
