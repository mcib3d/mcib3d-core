package mcib3d.geom2.measurements;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.VoxelInt;
import mcib3d.utils.Logger.AbstractLog;
import mcib3d.utils.Logger.NoLog;

import java.util.List;

public class MeasureFeret extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    // Feret in unit and pixel may have two different set of extremities pixels
    public static final String FERET_PIX = "Feret(pix)";
    public static final String FERET_UNIT = "Feret(unit)";
    public static final String FERET1_X_PIX = "Feret1X(pix)";
    public static final String FERET1_Y_PIX = "Feret1Y(pix)";
    public static final String FERET1_Z_PIX = "Feret1Z(pix)";
    public static final String FERET2_X_PIX = "Feret2X(pix)";
    public static final String FERET2_Y_PIX = "Feret2Y(pix)";
    public static final String FERET2_Z_PIX = "Feret2Z(pix)";
    public static final String FERET1_X_UNIT = "Feret1X(unit)";
    public static final String FERET1_Y_UNIT = "Feret1Y(unit)";
    public static final String FERET1_Z_UNIT = "Feret1Z(unit)";
    public static final String FERET2_X_UNIT = "Feret2X(unit)";
    public static final String FERET2_Y_UNIT = "Feret2Y(unit)";
    public static final String FERET2_Z_UNIT = "Feret2Z(unit)";


    // Log
    private AbstractLog log = new NoLog();

    private VoxelInt feret1Pix = null, feret2Pix = null;
    private double feretPix = Double.NaN;
    private VoxelInt feret1Unit = null, feret2Unit = null;
    private double feretUnit = Double.NaN;

    public MeasureFeret(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureFeret() {
    }

    public void setLog(AbstractLog log) {
        this.log = log;
    }

    @Override
    protected String[] getNames() {
        return new String[]{FERET_PIX, FERET_UNIT,
                FERET1_X_PIX, FERET1_Y_PIX, FERET1_Z_PIX, FERET2_X_PIX, FERET2_Y_PIX, FERET2_Z_PIX,
                FERET1_X_UNIT, FERET1_Y_UNIT, FERET1_Z_UNIT, FERET2_X_UNIT, FERET2_Y_UNIT, FERET2_Z_UNIT};
    }

    @Override
    protected void computeAll() {
        computeFeret();
        keysValues.put(FERET_PIX, feretPix);
        keysValues.put(FERET_UNIT, feretUnit);
        keysValues.put(FERET1_X_PIX, (double) feret1Pix.getX());
        keysValues.put(FERET1_Y_PIX, (double) feret1Pix.getY());
        keysValues.put(FERET1_Z_PIX, (double) feret1Pix.getZ());
        keysValues.put(FERET2_X_PIX, (double) feret2Pix.getX());
        keysValues.put(FERET2_Y_PIX, (double) feret2Pix.getY());
        keysValues.put(FERET2_Z_PIX, (double) feret2Pix.getZ());
        keysValues.put(FERET1_X_UNIT, (double) feret1Unit.getX());
        keysValues.put(FERET1_Y_UNIT, (double) feret1Unit.getY());
        keysValues.put(FERET1_Z_UNIT, (double) feret1Unit.getZ());
        keysValues.put(FERET2_X_UNIT, (double) feret2Unit.getX());
        keysValues.put(FERET2_Y_UNIT, (double) feret2Unit.getY());
        keysValues.put(FERET2_Z_UNIT, (double) feret2Unit.getZ());
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }

    public VoxelInt getFeret1Pix() {
        if (feret1Pix == null) computeFeret();
        return feret1Pix;
    }

    public VoxelInt getFeret2Pix() {
        if (feret2Pix == null) computeFeret();
        return feret2Pix;
    }

    public VoxelInt getFeret1Unit() {
        if (feret1Unit == null) computeFeret();
        return feret1Unit;
    }

    public VoxelInt getFeret2Unit() {
        if (feret2Unit == null) computeFeret();
        return feret2Unit;
    }

    private void computeFeret() {
        double distmaxPix = 0, distmaxUnit = 0, dist;
        double rx = object3DInt.getResXY();
        double rz = object3DInt.getResZ();
        VoxelInt p1, p2;
        List<VoxelInt> cont = new Object3DComputation(object3DInt).getContour();

        int s = cont.size();
        // case object only one voxel
        if (s == 1) {
            feret1Pix = cont.get(0);
            feret2Pix = cont.get(0);
            feretPix = 0;
            feret1Unit = cont.get(0);
            feret2Unit = cont.get(0);
            feretUnit = 0;
        }

        VoxelInt[] voxel3DS = new VoxelInt[cont.size()];
        voxel3DS = cont.toArray(voxel3DS);

        for (int i1 = 0; i1 < voxel3DS.length - 1; i1++) {
            log.log("Feret " + i1 + "/" + voxel3DS.length + "    ");
            p1 = voxel3DS[i1];
            for (int i2 = i1 + 1; i2 < voxel3DS.length; i2++) {
                p2 = voxel3DS[i2];
                // PIX
                dist = p1.distanceSquare(p2);
                if (dist > distmaxPix) {
                    distmaxPix = dist;
                    feret1Pix = p1;
                    feret2Pix = p2;
                }
                // UNIT
                dist = p1.distanceSquareScaled(p2, rx, rz);
                if (dist > distmaxUnit) {
                    distmaxUnit = dist;
                    feret1Unit = p1;
                    feret2Unit = p2;
                }
            }
        }
        feretPix = Math.sqrt(distmaxPix);
        feretUnit = Math.sqrt(distmaxUnit);
    }
}
