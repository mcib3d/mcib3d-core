package mcib3d.geom2.measurements;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DPlane;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

public class MeasureIntensity extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    public static final String INTENSITY_AVG = "IntensityAvg";
    public static final String INTENSITY_MIN = "IntensityMin";
    public static final String INTENSITY_MAX = "IntensityMax";
    public static final String INTENSITY_SD = "IntensityStdDev";
    public static final String INTENSITY_SUM = "IntensitySum";
    public static final String INTENSITY_CENTROID = "IntensityCentroid";

    public MeasureIntensity(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureIntensity() {
    }

    public MeasureIntensity(Object3DInt object3DInt, ImageHandler handler) {
        super(object3DInt);
        setIntensityImage(handler);
    }


    public void setIntensityImage(ImageHandler image) {
        valueImage = image;
        computeAll();
    }

    @Override
    protected String[] getNames() {
        return new String[]{INTENSITY_MIN, INTENSITY_MAX, INTENSITY_AVG, INTENSITY_SD, INTENSITY_SUM, INTENSITY_CENTROID};
    }

    @Override
    protected void computeAll() {
        if (valueImage == null) return;

        Double[] values = computeIntensityValues();
        keysValues.put(INTENSITY_MIN, values[0]);
        keysValues.put(INTENSITY_MAX, values[1]);
        keysValues.put(INTENSITY_AVG, values[2]);
        keysValues.put(INTENSITY_SD, values[3]);
        keysValues.put(INTENSITY_SUM, values[4]);
        keysValues.put(INTENSITY_CENTROID, values[5]);
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }

    public Double[] computeIntensityValues() {
        return computeIntensityValues(valueImage);
    }

    public Double[] computeIntensityValues(ImageHandler image) {
        if (image == null) return new Double[]{Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN};

        Double sum = 0.0, sum2 = 0.0, min = Double.POSITIVE_INFINITY, max = Double.NEGATIVE_INFINITY, nb = 0.0;
        final Double[] values = new Double[]{sum, min, max, nb};

        object3DInt.getObject3DPlanes().forEach(plane -> updateIntensityValues(plane, image, values));
        sum = values[0];
        min = values[1];
        max = values[2];
        nb = values[3];

        double integratedDensity = sum;
        double meanIntensity = integratedDensity / nb;
        if (min == Double.POSITIVE_INFINITY) min = Double.NaN;
        if (max == Double.NEGATIVE_INFINITY) max = Double.NaN;
        // standard dev
        double sigma = 0;
        if (nb > 1) {
            Double sumDiff = 0.0, nbDiff = 0.0;
            final Double[] valuesDiff = new Double[]{meanIntensity, sumDiff, nbDiff};
            object3DInt.getObject3DPlanes().forEach(plane -> updateSigmaComputation(plane, image, valuesDiff));
            sumDiff = valuesDiff[1];
            nbDiff = valuesDiff[2];
            sigma = Math.sqrt(sumDiff / nbDiff);
        }

        // at center
        VoxelInt centroid = new MeasureCentroid(object3DInt).getCentroidRoundedAsVoxelInt();
        double centroidPix = centroid == null ? Double.NaN : image.contains(centroid) ? image.getPixel(centroid) : Double.NaN;

        return new Double[]{min, max, meanIntensity, sigma, integratedDensity, centroidPix};
    }

    private void updateIntensityValues(Object3DPlane plane, ImageHandler signal, Double[] values) {
        final int offX = 0;
        final int offY = 0;
        final int offZ = 0;

        plane.getVoxels()
                .forEach(V -> {
                    int x = V.getX() - offX, y = V.getY() - offY, z = V.getZ() - offZ;
                    if (signal.contains(x, y, z)) {
                        float pix = signal.getPixel(x, y, z);
                        if (!Float.isNaN(pix)) {
                            values[0] += pix;
                            values[1] = Math.min(values[1], pix);
                            values[2] = Math.max(values[2], pix);
                            values[3]++;
                        }
                    }
                });
    }

    private void updateSigmaComputation(Object3DPlane plane, ImageHandler signal, Double[] values) {
        final int offX = 0;
        final int offY = 0;
        final int offZ = 0;

        plane.getVoxels()
                .forEach(V -> {
                    int x = V.getX() - offX, y = V.getY() - offY, z = V.getZ() - offZ;
                    if (signal.contains(x, y, z)) {
                        float pix = signal.getPixel(x, y, z);
                        if (!Float.isNaN(pix)) {
                            values[1] += (pix - values[0]) * (pix - values[0]);
                            values[2]++;
                        }
                    }
                });
    }


}
