package mcib3d.geom2.measurements;

import mcib3d.geom.Vector3D;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;

public class Measure2DistanceAdvanced {
    public final static String RADIUS_CENTRE_PIX = "RadiusCenterPix";
    public final static String RADIUS_CENTRE_UNIT = "RadiusCenterUnit";
    public final static String ECCENTRICITY_PIX = "EccentricityPix";
    public final static String ECCENTRICITY_UNIT = "EccentricityUnit";

    public final static String DISTBORDER_RADCENTER_PIX = "DistBBRadCenterPix";
    public final static String DISTBORDER_RADCENTER_UNIT = "DistBBRadCenterUnit";
    public final static String PERIPH_PIX = "PeriphericityPix";
    public final static String PERIPH_UNIT = "PeriphericityUnit";

    private final Object3DInt object3D1;
    private final Object3DInt object3D2;
    private Voxel3D centroid1 = null;
    private Voxel3D centroid2 = null;
    private Vector3D radiusCenCen = null;
    private Vector3D borBor1Radius = null;
    private Vector3D borBor2Radius = null;

    public Measure2DistanceAdvanced(Object3DInt object3D1, Object3DInt object3D2) {
        this.object3D1 = object3D1;
        this.object3D2 = object3D2;
    }

    public double getValue(String measure) {
        switch (measure) {
            case RADIUS_CENTRE_PIX:
                return radiusCenter(false);
            case RADIUS_CENTRE_UNIT:
                return radiusCenter(true);
            case ECCENTRICITY_PIX:
                return eccentricity(false);
            case ECCENTRICITY_UNIT:
                return eccentricity(true);
            case DISTBORDER_RADCENTER_PIX:
                return distBBRadiusCenter(false);
            case DISTBORDER_RADCENTER_UNIT:
                return distBBRadiusCenter(true);
            case PERIPH_PIX:
                return periph(false);
            case PERIPH_UNIT:
                return periph(true);
            default:
                return Double.NaN;
        }
    }

    private double radiusCenter(boolean useUnit) {
        if (centroid1 == null) computeCentroids();
        if (radiusCenCen == null) computeRadiusCenterCenter();
        if (useUnit) return radiusCenCen.getLength(object3D1.getVoxelSizeXY(), object3D1.getVoxelSizeZ());
        else return radiusCenCen.getLength();
    }

    private double eccentricity(boolean useUnit) {
        double radius = radiusCenter(useUnit);
        Vector3D cenCen = new Vector3D(centroid1, centroid2);
        if (useUnit) return cenCen.getLength(object3D1.getVoxelSizeXY(), object3D1.getVoxelSizeZ()) / radius;
        else return cenCen.getLength() / radius;
    }

    private double distBBRadiusCenter(boolean useUnit) {
        if (borBor1Radius == null) computeBBRadiusCenters();
        if (useUnit) return Math.min(borBor1Radius.getLength(object3D1.getVoxelSizeXY(), object3D1.getVoxelSizeZ())
                , borBor2Radius.getLength(object3D1.getVoxelSizeXY(), object3D1.getVoxelSizeZ()));
        else return Math.min(borBor1Radius.getLength(), borBor2Radius.getLength());
    }

    private double periph(boolean useUnit) {
        double radius = radiusCenter(useUnit);
        double bb = distBBRadiusCenter(useUnit);

        return bb / radius;
    }

    private void computeBBRadiusCenters() {
        borBor1Radius = new Object3DComputation(object3D1).vectorBorderBorder(object3D2, false);
        borBor2Radius = new Object3DComputation(object3D1).vectorBorderBorder(object3D2, true);
    }


    private void computeCentroids() {
        MeasureCentroid centre1 = new MeasureCentroid(object3D1);
        MeasureCentroid centre2 = new MeasureCentroid(object3D2);

        centroid1 = centre1.getCentroidAsVoxel();
        centroid2 = centre2.getCentroidAsVoxel();
    }

    private void computeRadiusCenterCenter() {
        Vector3D cenCen = new Vector3D(centroid1, centroid2);
        radiusCenCen = new Object3DComputation(object3D1).vectorPixelBorder(centroid1.getX(), centroid1.getY(), centroid1.getZ(), object3D2, cenCen);
        System.out.println(centroid1 + " " + radiusCenCen);
    }


}
