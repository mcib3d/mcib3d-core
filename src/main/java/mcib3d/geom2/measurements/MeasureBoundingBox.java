package mcib3d.geom2.measurements;

import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Object3DInt;

public class MeasureBoundingBox extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    public final static String XMIN = "Xmin";
    public final static String XMAX = "Xmax";
    public final static String YMIN = "Ymin";
    public final static String YMAX = "Ymax";
    public final static String ZMIN = "Zmin";
    public final static String ZMAX = "Zmax";

    public MeasureBoundingBox(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureBoundingBox() {
    }

    @Override
    protected String[] getNames() {
        return new String[]{XMIN, XMAX, YMIN, YMAX, ZMIN, ZMAX};
    }

    @Override
    protected void computeAll() {
        BoundingBox box = object3DInt.getBoundingBox();

        keysValues.put(XMIN, (double) box.xmin);
        keysValues.put(XMAX, (double) box.xmax);
        keysValues.put(YMIN, (double) box.ymin);
        keysValues.put(YMAX, (double) box.ymax);
        keysValues.put(ZMIN, (double) box.zmin);
        keysValues.put(ZMAX, (double) box.zmax);
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }
}
