package mcib3d.geom2.measurements;

import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class MeasureIntensityHist extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    public static final String INTENSITY_MEDIAN = "IntensityMedian";
    public static final String INTENSITY_MODE = "IntensityMode";
    public static final String INTENSITY_MODE_NONZERO = "IntensityModeNonZero";


    public MeasureIntensityHist(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureIntensityHist() {
    }

    public MeasureIntensityHist(Object3DInt object3DInt, ImageHandler handler) {
        super(object3DInt);
        setIntensityImage(handler);
    }


    public void setIntensityImage(ImageHandler image) {
        valueImage = image;
        computeAll();
    }

    @Override
    protected String[] getNames() {
        return new String[]{INTENSITY_MEDIAN, INTENSITY_MODE, INTENSITY_MODE_NONZERO};
    }

    @Override
    protected void computeAll() {
        if (valueImage == null) return;

        Double[] values = computeIntensityValues();
        keysValues.put(INTENSITY_MEDIAN, values[0]);
        keysValues.put(INTENSITY_MODE, values[1]);
        if (values[2] > 0) keysValues.put(INTENSITY_MODE_NONZERO, values[2]);
        else keysValues.put(INTENSITY_MODE_NONZERO, Double.NaN);
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }

    public double getMedianValue() {
        if (keysValues.isEmpty()) computeAll();

        return keysValues.get(INTENSITY_MEDIAN);
    }

    public double getModeValue() {
        if (keysValues.isEmpty()) computeAll();

        return keysValues.get(INTENSITY_MODE);
    }

    public double getModeNonZeroValue() {
        if (keysValues.isEmpty()) computeAll();

        return keysValues.get(INTENSITY_MODE_NONZERO);
    }

    public Map<Float, Integer> getHistogramFloat() {
        return getHistogramFloat(valueImage);
    }

    public Map<Float, Integer> getHistogramFloat(ImageHandler image) {
        if (image == null) return null;
        final Map<Float, Integer> hist = new TreeMap<>();

        // Bounding box of object completely contains the object bounding box
        if(image.getBoundingBox().contains(object3DInt.getBoundingBox())){
            object3DInt.getObject3DPlanes().forEach(P -> P.updateHist(image, hist));
        }
        else {
            object3DInt.getObject3DPlanes().forEach(P -> P.updateHistCheckVoxel(image, hist));
        }
        
        return hist;
    }

    public List<Float> listValuesUnique() {
        return listValuesUnique(valueImage);
    }

    public List<Float> listValuesUnique(ImageHandler image) {
        Map<Float, Integer> histogram = getHistogramFloat(image);

        return new ArrayList<>(histogram.keySet());
    }

    private Double[] computeIntensityValues() {
        if (valueImage == null) return new Double[]{Double.NaN, Double.NaN};
        final Map<Float, Integer> hist = getHistogramFloat();

        // mode
        AtomicInteger max0 = new AtomicInteger(0);
        AtomicReference<Float> maxV0 = new AtomicReference<>();
        maxV0.set(0f);
        // mode non zero
        AtomicInteger max1 = new AtomicInteger(0);
        AtomicReference<Float> maxV1 = new AtomicReference<>();
        maxV1.set(0f);
        // median
        AtomicInteger sum = new AtomicInteger(0);
        AtomicReference<Float> halfV = new AtomicReference<>();
        halfV.set(0f);

        final int half = (int) Math.ceil(object3DInt.size() / 2);

        hist.keySet().forEach(F -> {
            int nb = hist.get(F);
            // mode
            if (nb > max0.get()) {
                max0.set(nb);
                maxV0.set(F);
            }
            // mode non zero
            if ((F > 0) && (nb > max1.get())) {
                max1.set(nb);
                maxV1.set(F);
            }

            // median
            if (sum.get() < half) {
                sum.set(sum.get() + nb);
                halfV.set(F);
            }
        });

        return new Double[]{Double.valueOf(halfV.get()), Double.valueOf(maxV0.get()), Double.valueOf(maxV1.get())};
    }
}
