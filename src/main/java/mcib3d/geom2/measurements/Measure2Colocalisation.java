package mcib3d.geom2.measurements;

import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageHandler;

public class Measure2Colocalisation extends Measure2Abstract {
    public final static String COLOC_VOLUME = "ColocVolume";
    public final static String COLOC_PC = "ColocRatio";
    public final static String COLOC_IoU = "IoU";

    private int computeVolume = -1;


    public Measure2Colocalisation(Object3DInt object3D1, Object3DInt object3D2) {
        super(object3D1,  object3D2);
    }

    public Measure2Colocalisation() {
    }

    @Override
    protected String[] getNames() {
        return new String[]{COLOC_VOLUME, COLOC_PC, COLOC_IoU};
    }

    @Override
    protected void computeAll() {
        double coloc = colocVolume();
        keysValues.put(COLOC_VOLUME, coloc);
        keysValues.put(COLOC_PC, coloc / object3DInt1.size());
        keysValues.put(COLOC_IoU, colocVolume() / (object3DInt1.size() + object3DInt2.size() - colocVolume()));
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }


    @Deprecated
    public double getValue(String measure) {
        switch (measure) {
            case COLOC_VOLUME:
                return colocVolume();
            case COLOC_PC:
                return (double) computeVolume / object3DInt1.size();
            case COLOC_IoU:
                return colocVolume() / (object3DInt1.size() + object3DInt2.size() - colocVolume());
            default:
                return -1;
        }
    }

    private double colocVolume() {
        if (computeVolume < 0) computeColoc();

        return computeVolume;
    }

    private void computeColoc() {
        BoundingBox box1 = object3DInt1.getBoundingBox();
        BoundingBox box2 = object3DInt2.getBoundingBox();

        if (box1.disjoint(box2)) {
            computeVolume = 0;
            return;
        }

        BoundingBox inter = box1.intersection(box2);
        ImageHandler interImage1 = inter.createImage();
        object3DInt1.drawObjectUsingOffset(interImage1, 1);
        ImageHandler interImage2 = inter.createImage();
        object3DInt2.drawObjectUsingOffset(interImage2, 1);
        interImage1.intersectMask(interImage2);
        computeVolume = (int) interImage1.getImageStats(null).getSum();
    }
}
