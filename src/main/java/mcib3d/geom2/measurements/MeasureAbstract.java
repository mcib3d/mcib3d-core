package mcib3d.geom2.measurements;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageHandler;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * The abstract class for 3D object measurement.
 */
public abstract class MeasureAbstract {
    /**
     * The constant for the 3D object label value.
     */
    public static final String LABEL_OBJ = "LabelObj";
    /**
     * The 3D object to be measured.
     */
    protected final Object3DInt object3DInt;
    /**
     * The entries as (name of measurement, value of measurement)
     */
    protected final Map<String, Double> keysValues;
    /**
     * The image to be used to perform intensity measurement (otherwise null).
     */
    protected ImageHandler valueImage = null;

    /**
     * Instantiates a new Measure.
     *
     * @param object3DInt the 3D object
     */
    public MeasureAbstract(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
        keysValues = new HashMap<>();
        keysValues.put(LABEL_OBJ, (double) object3DInt.getLabel());
    }

    /**
     * Instantiates an empty new Measure.
     */
    public MeasureAbstract() {
        this.object3DInt = null;
        keysValues = new HashMap<>();
    }

    /**
     * Gets the image used for intensity measurements.
     *
     * @return the image
     */
    public ImageHandler getValueImage() {
        return valueImage;
    }

    /**
     * Sets the image used for intensity measurements.
     *
     * @param valueImage the image
     */
    public void setValueImage(ImageHandler valueImage) {
        this.valueImage = valueImage;
    }

    /**
     * Get names of the local measurements.
     *
     * @return the array of names.
     */
    protected abstract String[] getNames();

    /**
     * Get names of the measurement.
     *
     * @return the array of names
     */
    public String[] getNamesMeasurement() {
        String[] names = getNames();
        String[] namesMeasurement = new String[names.length + 1];
        namesMeasurement[0] = LABEL_OBJ;
        System.arraycopy(names, 0, namesMeasurement, 1, names.length);

        return namesMeasurement;
    }

    /**
     * Gets the measurement value for the specified measurement.
     * The names of measurements are available in the classes within this package.
     *
     * @param mes the measurement name
     * @return the value of the measurement
     */
    public Double getValueMeasurement(String mes) {
        if (Arrays.stream(getNamesMeasurement()).noneMatch(name -> name.equalsIgnoreCase(mes))) return null;
        if (keysValues.get(mes) == null) compute(mes);

        return keysValues.get(mes);
    }

    /**
     * Gets the measurement values for the specified measurements.
     * The names of measurements are available in the classes within this package.
     *
     * @param list the measurement names
     * @return the values of the measurement
     */
    public Double[] getValuesMeasurement(String[] list) {
        Double[] values = new Double[list.length];
        for (int i = 0; i < list.length; i++) {
            values[i] = getValueMeasurement(list[i]);
        }

        return values;
    }

    /**
     * Get all the measurement values for the instantiated class.
     * The label of the object is added at the beginning of the measurements.
     *
     * @return all the measurement values.
     */
    public Double[] getAllValuesMeasurement() {
        String[] names = getNamesMeasurement();
        Double[] values = new Double[names.length];
        for (int i = 0; i < names.length; i++) values[i] = getValueMeasurement(names[i]);

        return values;
    }

    /**
     * Compute the measurements.
     */
    protected abstract void computeAll();

    protected abstract void compute(String mes);

}
