package mcib3d.geom2.measurements;

import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageHandler;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class MeasureObject {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;

    private final static HashMap<String, String> measureIntensitySwitch = new HashMap<>();
    private final static HashMap<String, String> measureSwitch = new HashMap<>();

    private final static String CENTROID = "Centroid";
    private final static String COMPACTNESS = "Compactness";
    private final static String BOUNDS = "Bounds";
    private final static String DC = "DC";
    private static final String ELLIPSOID = "Ellipsoid";
    private static final String FERET = "Feret";
    private static final String SURFACE = "Surface";
    private static final String VOLUME = "Volume";

    private static final String INTENSITY = "Intensity";
    private static final String CENTREMASS = "CentreMass";
    private static final String NUMBERING = "Numbering";
    private static final String INTENSITYHIST = "Histogram";

    Object3DInt object3DInt;

    public MeasureObject(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
        if (measureSwitch.isEmpty()) init();
    }

    public MeasureObject() {
        if (measureSwitch.isEmpty()) init();
    }

    public Object3DInt getObject3DInt() {
        return object3DInt;
    }

    public void setObject3DInt(Object3DInt object3DInt) {
        this.object3DInt = object3DInt;
    }

    public void init() {
        // Centroid
        MeasureCentroid measure1 = new MeasureCentroid();
        for (String s : measure1.getNamesMeasurement()) {
            measureSwitch.put(s, CENTROID);
        }
        // Bounding
        MeasureBoundingBox measureBB = new MeasureBoundingBox();
        for (String s : measureBB.getNamesMeasurement()) {
            measureSwitch.put(s, BOUNDS);
        }
        // Compactness
        MeasureCompactness measure2 = new MeasureCompactness();
        for (String s : measure2.getNamesMeasurement()) {
            measureSwitch.put(s, COMPACTNESS);
        }
        // DistancesCenter
        MeasureDistancesCenter measure3 = new MeasureDistancesCenter();
        for (String s : measure3.getNamesMeasurement()) {
            measureSwitch.put(s, DC);
        }
        // Ellipsoid
        MeasureEllipsoid measure4 = new MeasureEllipsoid();
        for (String s : measure4.getNamesMeasurement()) {
            measureSwitch.put(s, ELLIPSOID);
        }
        // Feret
        MeasureFeret measure5 = new MeasureFeret();
        for (String s : measure5.getNamesMeasurement()) {
            measureSwitch.put(s, FERET);
        }
        // Surface
        MeasureSurface measure6 = new MeasureSurface();
        for (String s : measure6.getNamesMeasurement()) {
            measureSwitch.put(s, SURFACE);
        }
        // Volume
        MeasureVolume measure7 = new MeasureVolume();
        for (String s : measure7.getNamesMeasurement()) {
            measureSwitch.put(s, VOLUME);
        }
        // Intensity
        MeasureIntensity measure8 = new MeasureIntensity();
        for (String s : measure8.getNamesMeasurement()) {
            measureIntensitySwitch.put(s, INTENSITY);
        }
        // CenterOfMass
        MeasureCenterOfMass measure9 = new MeasureCenterOfMass();
        for (String s : measure9.getNamesMeasurement()) {
            measureIntensitySwitch.put(s, CENTREMASS);
        }
        // Numbering
        MeasureNumbering measure10 = new MeasureNumbering();
        for (String s : measure10.getNamesMeasurement()) {
            measureIntensitySwitch.put(s, NUMBERING);
        }
        // Intensity Hist
        MeasureIntensityHist measure11 = new MeasureIntensityHist();
        for (String s : measure11.getNamesMeasurement()) {
            measureIntensitySwitch.put(s, INTENSITYHIST);
        }
    }

    public List<String> listMeasures() {
        return new LinkedList<>(measureSwitch.keySet());
    }

    public List<String> listMeasuresIntensity() {
        return new LinkedList<>(measureIntensitySwitch.keySet());
    }

    public Double measure(String mes) {
        switch (measureSwitch.get(mes)) {
            case CENTROID:
                return new MeasureCentroid(object3DInt).getValueMeasurement(mes);
            case BOUNDS:
                return new MeasureBoundingBox(object3DInt).getValueMeasurement(mes);
            case COMPACTNESS:
                return new MeasureCompactness(object3DInt).getValueMeasurement(mes);
            case DC:
                return new MeasureDistancesCenter(object3DInt).getValueMeasurement(mes);
            case ELLIPSOID:
                return new MeasureEllipsoid(object3DInt).getValueMeasurement(mes);
            case FERET:
                return new MeasureFeret(object3DInt).getValueMeasurement(mes);
            case SURFACE:
                return new MeasureSurface(object3DInt).getValueMeasurement(mes);
            case VOLUME:
                return new MeasureVolume(object3DInt).getValueMeasurement(mes);
        }

        return null;
    }

    public Double measureIntensity(String mes, ImageHandler handler) {
        switch (measureIntensitySwitch.get(mes)) {
            case CENTREMASS:
                return new MeasureCenterOfMass(object3DInt, handler).getValueMeasurement(mes);
            case INTENSITY:
                return new MeasureIntensity(object3DInt, handler).getValueMeasurement(mes);
            case INTENSITYHIST:
                return new MeasureIntensityHist(object3DInt, handler).getValueMeasurement(mes);
            case NUMBERING:
                return new MeasureNumbering(object3DInt, handler).getValueMeasurement(mes);
        }

        return null;
    }

    public Double[] measureList(String[] mes) {
        Double[] results = new Double[mes.length];
        MeasureCentroid measureCentroid = null;
        MeasureBoundingBox measureBounding = null;
        MeasureCompactness measureCompactness = null;
        MeasureDistancesCenter measureDistancesCenter = null;
        MeasureEllipsoid measureEllipsoid = null;
        MeasureFeret measureFeret = null;
        MeasureSurface measureSurface = null;
        MeasureVolume measureVolume = null;

        for (int i = 0; i < mes.length; i++) {
            String m = mes[i];
            if ((m == null) || (measureSwitch.get(m) == null)) {
                results[i] = null;
                continue;
            }
            switch (measureSwitch.get(m)) {
                case CENTROID: {
                    if (measureCentroid == null) measureCentroid = new MeasureCentroid(object3DInt);
                    Double value = measureCentroid.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case BOUNDS: {
                    if (measureBounding == null) measureBounding = new MeasureBoundingBox(object3DInt);
                    Double value = measureBounding.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }

                case COMPACTNESS: {
                    if (measureCompactness == null) measureCompactness = new MeasureCompactness(object3DInt);
                    Double value = measureCompactness.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case DC: {
                    if (measureDistancesCenter == null)
                        measureDistancesCenter = new MeasureDistancesCenter(object3DInt);
                    Double value = measureDistancesCenter.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case ELLIPSOID: {
                    if (measureEllipsoid == null) measureEllipsoid = new MeasureEllipsoid(object3DInt);
                    Double value = measureEllipsoid.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case FERET: {
                    if (measureFeret == null) measureFeret = new MeasureFeret(object3DInt);
                    Double value = measureFeret.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case SURFACE: {
                    if (measureSurface == null) measureSurface = new MeasureSurface(object3DInt);
                    Double value = measureSurface.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case VOLUME: {
                    if (measureVolume == null) measureVolume = new MeasureVolume(object3DInt);
                    Double value = measureVolume.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                results[i] = null;
            }
        }

        return results;
    }

    public Double[] measureIntensityList(String[] mes, ImageHandler handler) {
        Double[] results = new Double[mes.length];
        MeasureCenterOfMass measureCenterOfMass = null;
        MeasureNumbering measureNumbering = null;
        MeasureIntensity measureIntensity = null;
        MeasureIntensityHist measureIntensityHist = null;

        for (int i = 0; i < mes.length; i++) {
            String m = mes[i];
            if ((m == null) || (measureIntensitySwitch.get(m) == null)) {
                results[i] = null;
                continue;
            }
            switch (measureIntensitySwitch.get(m)) {
                case CENTREMASS: {
                    if (measureCenterOfMass == null)
                        measureCenterOfMass = new MeasureCenterOfMass(object3DInt, handler);
                    Double value = measureCenterOfMass.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case NUMBERING: {
                    if (measureNumbering == null) measureNumbering = new MeasureNumbering(object3DInt, handler);
                    Double value = measureNumbering.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case INTENSITY: {
                    if (measureIntensity == null) measureIntensity = new MeasureIntensity(object3DInt, handler);
                    Double value = measureIntensity.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                case INTENSITYHIST: {
                    if (measureIntensityHist == null)
                        measureIntensityHist = new MeasureIntensityHist(object3DInt, handler);
                    Double value = measureIntensityHist.getValueMeasurement(m);
                    if (value != null) {
                        results[i] = value;
                        continue;
                    }
                }
                results[i] = null;
            }
        }

        return results;
    }
}
