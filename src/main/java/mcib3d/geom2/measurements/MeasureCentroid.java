package mcib3d.geom2.measurements;

import mcib3d.geom.Point3D;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DIntLabelImage;
import mcib3d.geom2.VoxelInt;

public class MeasureCentroid extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    public final static String CX_PIX = "CX(pix)";
    public final static String CY_PIX = "CY(pix)";
    public final static String CZ_PIX = "CZ(pix)";
    public final static String CX_UNIT = "CX(unit)";
    public final static String CY_UNIT = "CY(unit)";
    public final static String CZ_UNIT = "CZ(unit)";
    public final static String CENTROID_IN = "CentroidIN";

    public MeasureCentroid(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureCentroid() {
    }

    @Override
    protected String[] getNames() {
        return new String[]{CX_PIX, CY_PIX, CZ_PIX, CX_UNIT, CY_UNIT, CZ_UNIT, CENTROID_IN};
    }

    @Override
    protected void computeAll() {
        computeCentroid();
    }

    protected void compute(String mes) {
        computeAll();
    }

    private void computeCentroid() {
        // need double precision in sum
        Voxel3D sum =  new Object3DComputation(object3DInt).getSumCoordinates();
        double vol = object3DInt.size();
        // empty object
        if (vol == 0) {
            keysValues.put(CX_PIX, Double.NaN);
            keysValues.put(CY_PIX, Double.NaN);
            keysValues.put(CZ_PIX, Double.NaN);
            keysValues.put(CX_UNIT, Double.NaN);
            keysValues.put(CY_UNIT, Double.NaN);
            keysValues.put(CZ_UNIT, Double.NaN);
            keysValues.put(CENTROID_IN, Double.NaN);
            return;
        }

        // pix
        double cx = sum.getX() / vol, cy = sum.getY() / vol, cz = sum.getZ() / vol;
        keysValues.put(CX_PIX, cx);
        keysValues.put(CY_PIX, cy);
        keysValues.put(CZ_PIX, cz);

        // unit
        double xy = object3DInt.getVoxelSizeXY();
        double xz = object3DInt.getVoxelSizeZ();
        keysValues.put(CX_UNIT, cx * xy);
        keysValues.put(CY_UNIT, cy * xy);
        keysValues.put(CZ_UNIT, cz * xz);

        // check if centroid inside object
        boolean centreIn = new Object3DIntLabelImage(object3DInt).getCroppedLabelImage(1).getPixelUsingOffset(getCentroidRoundedAsVoxelInt()) > 0;
        keysValues.put(CENTROID_IN, (double) (centreIn ? 1 : 0));
    }

    public Point3D getCentroidAsPoint() {
        return new Point3D(getValueMeasurement(CX_PIX), getValueMeasurement(CY_PIX), getValueMeasurement(CZ_PIX));
    }

    public Voxel3D getCentroidAsVoxel() {
        Point3D point3D = getCentroidAsPoint();
        return new Voxel3D(point3D.getX(), point3D.getY(), point3D.getZ(), object3DInt.getLabel());
    }

    public VoxelInt getCentroidRoundedAsVoxelInt() {
        Point3D voxel3D = getCentroidAsPoint();
        if (voxel3D.isNotNaN())
            return new VoxelInt(voxel3D.getRoundX(), voxel3D.getRoundY(), voxel3D.getRoundZ(), object3DInt.getLabel());
        else return null;
    }

    public boolean isCentroidInsideObject() {
        return getValueMeasurement(CENTROID_IN) > 0;
    }
}
