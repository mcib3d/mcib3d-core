package mcib3d.geom2.measurements;

import mcib3d.Jama.EigenvalueDecomposition;
import mcib3d.Jama.Matrix;
import mcib3d.geom.ObjectCreator3D;
import mcib3d.geom.Point3D;
import mcib3d.geom.Vector3D;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;


public class MeasureEllipsoid extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    public final static String ELL_VOL_UNIT = "EllVol(unit)";
    public final static String ELL_SPARENESS = "EllSpareness";
    public final static String ELL_MAJOR_RADIUS_UNIT = "EllMajRad(unit)";
    public final static String ELL_ELONGATION = "EllElon";
    public final static String ELL_FLATNESS = "EllFlatness";

    private double radius1, radius2, radius3;
    private double[] eigenValues;
    private Vector3D Axis1, Axis2, Axis3;

    public MeasureEllipsoid(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureEllipsoid() {

    }

    @Override
    protected String[] getNames() {
        return new String[]{ELL_VOL_UNIT, ELL_SPARENESS, ELL_MAJOR_RADIUS_UNIT, ELL_ELONGATION, ELL_FLATNESS};
    }

    public Double[] getRadii() {
        return new Double[]{radius1, radius2, radius3};
    }

    public Vector3D getAxis1() {
        if (Axis1 == null) computeAll();
        return Axis1;
    }

    public Vector3D getAxis2() {
        if (Axis2 == null) computeAll();
        return Axis2;
    }

    public Vector3D getAxis3() {
        if (Axis3 == null) computeAll();
        return Axis3;
    }

    public double getValueAxis(int order) {
        if ((order > 2) || (order < 0)) return Double.NaN;
        if (eigenValues == null) computeAll();
        return eigenValues[order];
    }

    public Object3DInt getEllipsoid() {
        if (Axis1 == null) computeAll();
        Vector3D V = getAxis1();
        Vector3D W = getAxis2();
        double rad1 = getRadii()[0];
        double rad2 = getRadii()[1];
        double rad3 = getRadii()[2];

        int radius = (int) (rad1 / object3DInt.getVoxelSizeXY()) + 1;
        ObjectCreator3D ellipsoid = new ObjectCreator3D(2 * radius, 2 * radius, 2 * radius);
        ellipsoid.setResolution(object3DInt.getVoxelSizeXY(), object3DInt.getVoxelSizeZ(), object3DInt.getUnit());
        ellipsoid.createEllipsoidAxesUnit(radius * object3DInt.getVoxelSizeXY(), radius * object3DInt.getVoxelSizeXY(), radius * object3DInt.getVoxelSizeZ(), rad1, rad2, rad3, 1, V, W, false);
        Object3DInt ell = new Object3DInt(ellipsoid.getImageHandler(), 1);
        Point3D centroid = new MeasureCentroid(object3DInt).getCentroidAsPoint();
        ell.translate((int) (centroid.getX() - radius), (int) (centroid.getY() - radius), (int) (centroid.getZ() - radius));

        return ell;
    }

    @Override
    protected void computeAll() {
        // special case one voxel only
        if (object3DInt.size() == 1) {
            keysValues.put(ELL_VOL_UNIT, object3DInt.getVoxelSizeXY() * object3DInt.getVoxelSizeXY() * object3DInt.getVoxelSizeZ());
            keysValues.put(ELL_SPARENESS, 0.0);
            keysValues.put(ELL_MAJOR_RADIUS_UNIT, 0.0);
            keysValues.put(ELL_ELONGATION, 0.0);
            keysValues.put(ELL_FLATNESS, 0.0);
            radius1 = 0;
            radius2 = 0;
            radius3 = 0;
            Axis1 = new Vector3D(0, 0, 0);
            Axis2 = new Vector3D(0, 0, 0);
            Axis3 = new Vector3D(0, 0, 0);
        }
        Matrix mat = new Matrix(3, 3);
        MeasureCentroid centroid = new MeasureCentroid(object3DInt);
        Double[] sums =  new Object3DComputation(object3DInt).computeMoments2(centroid.getCentroidAsVoxel(), true);

        mat.set(0, 0, sums[0]); // xx
        mat.set(0, 1, sums[3]); // xy
        mat.set(0, 2, sums[4]); // xz
        mat.set(1, 0, sums[3]); // xy
        mat.set(1, 1, sums[1]); // yy
        mat.set(1, 2, sums[5]); // yz
        mat.set(2, 0, sums[4]); // xz
        mat.set(2, 1, sums[5]); // yz
        mat.set(2, 2, sums[2]); // zz

        EigenvalueDecomposition eigen = new EigenvalueDecomposition(mat);
        eigenValues = eigen.getRealEigenvalues();

        double R1 = Math.sqrt(5.0 * eigenValues[2]);
        double R2 = Math.sqrt(5.0 * eigenValues[1]);
        double R3 = Math.sqrt(5.0 * eigenValues[0]);
        double volEll = (4.0 / 3.0) * Math.PI * R1 * R2 * R3;
        MeasureVolume volume = new MeasureVolume(object3DInt);

        keysValues.put(ELL_VOL_UNIT, volEll);
        keysValues.put(ELL_SPARENESS, volume.getValueMeasurement(MeasureVolume.VOLUME_UNIT) / volEll);
        keysValues.put(ELL_MAJOR_RADIUS_UNIT, R1);
        keysValues.put(ELL_ELONGATION, R1 / R2);
        keysValues.put(ELL_FLATNESS, R2 / R3);

        radius1 = R1;
        radius2 = R2;
        radius3 = R3;

        // vectors
        Matrix eVectors = eigen.getV();
        Axis1 = new Vector3D(eVectors.get(0, 2), eVectors.get(1, 2), eVectors.get(2, 2));
        Axis2 = new Vector3D(eVectors.get(0, 1), eVectors.get(1, 1), eVectors.get(2, 1));
        Axis3 = new Vector3D(eVectors.get(0, 0), eVectors.get(1, 0), eVectors.get(2, 0));
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }
}
