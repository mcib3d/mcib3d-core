package mcib3d.geom2.measurements;

import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageHandler;

import java.util.BitSet;
import java.util.concurrent.atomic.AtomicInteger;

public class MeasureNumbering extends MeasureAbstract {
    public static final String LABEL = MeasureAbstract.LABEL_OBJ;
    public static final String OBJ_NUMBER = "NumberObj";
    public static final String OBJ_VOLUME = "NumberObjVol";
    public static final String OBJ_PERCENT = "NumberObjVolPerc";

    public MeasureNumbering(Object3DInt object3DInt) {
        super(object3DInt);
    }

    public MeasureNumbering() {
    }

    public MeasureNumbering(Object3DInt object3DInt, ImageHandler handler) {
        super(object3DInt);
        setIntensityImage(handler);
    }

    public void setIntensityImage(ImageHandler image) {
        valueImage = image;
        computeAll();
    }

    @Override
    protected String[] getNames() {
        return new String[]{OBJ_NUMBER, OBJ_VOLUME, OBJ_PERCENT};
    }

    @Override
    protected void computeAll() {
        if (valueImage == null) return;

        Double[] values = computeNumbering(valueImage);
        keysValues.put(OBJ_NUMBER, values[0]);
        keysValues.put(OBJ_VOLUME, values[1]);
        keysValues.put(OBJ_PERCENT, 100.0 * (values[1] / object3DInt.size()));
    }

    @Override
    protected void compute(String mes) {
        computeAll();
    }

    private Double[] computeNumbering(ImageHandler ima) {
        if (ima == null) return new Double[]{Double.NaN, Double.NaN};

        MeasureIntensity intensity = new MeasureIntensity(object3DInt, ima);
        int min = intensity.getValueMeasurement(MeasureIntensity.INTENSITY_MIN).intValue();
        int max = intensity.getValueMeasurement(MeasureIntensity.INTENSITY_MAX).intValue();
        BitSet bitSet = new BitSet(max - min + 1);
        AtomicInteger ai = new AtomicInteger(0);

        object3DInt.getObject3DPlanes().forEach(plane -> plane.getVoxels().forEach(V ->
        {
            int x = V.getX();
            int y = V.getY();
            int z = V.getZ();
            int pix = (int) ima.getPixel(x, y, z);
            if (pix > 0) {
                ai.incrementAndGet();
                bitSet.set(pix - min);
            }
        }));

        return new Double[]{(double) bitSet.cardinality(), (double) ai.get()};
    }
}
