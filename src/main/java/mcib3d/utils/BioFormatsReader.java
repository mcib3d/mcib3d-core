package mcib3d.utils;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;
import loci.common.services.DependencyException;
import loci.common.services.ServiceException;
import loci.common.services.ServiceFactory;
import loci.formats.ChannelSeparator;
import loci.formats.meta.IMetadata;
import loci.formats.services.OMEXMLService;
import loci.plugins.util.ImageProcessorReader;
import loci.plugins.util.LociPrefs;
import mcib3d.image3d.ImageDimension;
import mcib3d.image3d.ImageHandler;
import ome.units.quantity.Length;

import java.io.File;

public class BioFormatsReader {

    public static void main(String[] args) {
    }

    public BioFormatsReader() {
    }

    public static ImageHandler OpenImage(String file) {
        return OpenImage(new File(file), 0, 0, 0);
    }

    public static ImageHandler OpenImage(String file, int channel, int time) {
        return OpenImage(new File(file), channel, 0, time);
    }

    // to use with R
    public static ImageHandler OpenImageString(String file, String channel, String time){
        return OpenImage(new File(file), Integer.parseInt(channel), 0, Integer.parseInt(time));
    }

    public static ImagePlus OpenImagePlus(String file, int channel, int time) {
        return OpenImage(file, channel, time).getImagePlus();
    }

    public static ImageDimension getImageDimension(String file){
        return getImageDimension(new File(file));
    }

    public static ImageDimension getImageDimension(File file){
        int seriesNumber = 0;
        ImageDimension dimension = null;

        ImageProcessorReader reader = new ImageProcessorReader(new ChannelSeparator(LociPrefs.makeImageReader()));

        ServiceFactory factory;
        IMetadata meta = null;
        try {
            factory = new ServiceFactory();
            OMEXMLService service = factory.getInstance(OMEXMLService.class);
            try {
                meta = service.createOMEXMLMetadata();
                reader.setMetadataStore(meta);
            } catch (ServiceException ex) {
                IJ.log("An error occurred while analysing image: " + file.getName()  + " "+ex.getMessage());
            }
        } catch (DependencyException ex) {
            IJ.log("An error occurred while analysing image: " + file.getName()+" "+  ex.getMessage());
        }

        try {
            IJ.log("Examining BioFormats file : " + file.getAbsolutePath());
            reader.setId(file.getAbsolutePath());

            reader.setSeries(seriesNumber);
            int width = reader.getSizeX();
            int height = reader.getSizeY();
            int sizeZ = reader.getSizeZ();
            int sizeC = reader.getSizeC(); // check channel
            int sizeT = reader.getSizeT(); // check timepoint

             dimension = new ImageDimension(width,height,sizeZ,sizeC,sizeT);

            if (meta != null) {
                Length xy = meta.getPixelsPhysicalSizeX(0);
                Length z = meta.getPixelsPhysicalSizeZ(0);

                if (xy != null && z != null) {
                    dimension.setCalUnit(xy.unit().getSymbol());
                    dimension.setCalXY(xy.value().doubleValue());
                    dimension.setCalZ(z.value().doubleValue());

                } else IJ.log("no calibration found");
            }
            reader.close();


        } catch (Exception exc) {
            IJ.log("An error occurred while opening image: " + file.getName() +" s:" + seriesNumber + exc.getMessage());
        }
        return dimension;

    }

    /**
     * Main function to open images
     *
     * @param file         The file containing the image
     * @param channel      The channel to open, starts 0
     * @param seriesNumber The serie number, usually 0
     * @param timePoint    The time point, starts 0
     * @return the Image in ImageHandler format
     */
    public static ImageHandler OpenImage(File file, int channel, int seriesNumber, int timePoint) {
        ImageHandler res = null;

        ImageProcessorReader reader = new ImageProcessorReader(new ChannelSeparator(LociPrefs.makeImageReader()));

        ServiceFactory factory;
        IMetadata meta = null;
        try {
            factory = new ServiceFactory();
            OMEXMLService service = factory.getInstance(OMEXMLService.class);
            try {
                meta = service.createOMEXMLMetadata();
                reader.setMetadataStore(meta);
            } catch (ServiceException ex) {
                IJ.log("An error occurred while analysing image: " + file.getName() + " channel:" + channel + " t:" + timePoint + " s:" + seriesNumber + ex.getMessage());
            }
        } catch (DependencyException ex) {
            IJ.log("An error occurred while analysing image: " + file.getName() + " channel:" + channel + " t:" + timePoint + " s:" + seriesNumber + ex.getMessage());
        }

        try {
            IJ.log("Examining BioFormats file : " + file.getAbsolutePath());
            reader.setId(file.getAbsolutePath());

            reader.setSeries(seriesNumber);
            int width = reader.getSizeX();
            int height = reader.getSizeY();
            int sizeZ = reader.getSizeZ();
            int sizeC = reader.getSizeC(); // check channel
            int sizeT = reader.getSizeT(); // check timepoint
            ImageStack stack = new ImageStack(width, height);
            for (int z = 0; z < sizeZ; z++) {
                ImageProcessor ip = reader.openProcessors(reader.getIndex(z, channel, timePoint))[0];
                stack.addSlice("" + (z + 1), ip);
            }
            res = ImageHandler.wrap(stack);
            res.setGraysLut();

            //MetadataRetrieve meta=(MetadataRetrieve)r.getMetadataStore();
            if (meta != null) {
                Length xy = meta.getPixelsPhysicalSizeX(0);
                Length z = meta.getPixelsPhysicalSizeZ(0);

                if (xy != null && z != null) {
                    //ij.IJ.log("calibration: xy"+ xy.value()+" z:"+z.value()+ "  units:"+xy.unit().getSymbol());
                    res.setScale((Double) xy.value(), (Double) z.value(), xy.unit().getSymbol());
                } else IJ.log("no calibration found");
            }
            reader.close();


        } catch (Exception exc) {
            IJ.log("An error occurred while opening image: " + file.getName() + " channel:" + channel + " t:" + timePoint + " s:" + seriesNumber + exc.getMessage());
        }
        return res;
    }

    public static ImageHandler OpenImageCropping(File file, int channel, int seriesNumber, int timePoint, int startx, int starty, int startz, int sizex, int sizey, int sizez) {
        ImageHandler res = null;

        ImageProcessorReader r = new ImageProcessorReader(new ChannelSeparator(LociPrefs.makeImageReader()));

        ServiceFactory factory;
        IMetadata meta = null;
        try {
            factory = new ServiceFactory();
            OMEXMLService service = factory.getInstance(OMEXMLService.class);
            try {
                meta = service.createOMEXMLMetadata();
                r.setMetadataStore(meta);
            } catch (ServiceException ex) {
                IJ.log("An error occurred while analysing image: " + file.getName() + " channel:" + channel + " t:" + timePoint + " s:" + seriesNumber + ex.getMessage());
            }
        } catch (DependencyException ex) {
            IJ.log("An error occurred while analysing image: " + file.getName() + " channel:" + channel + " t:" + timePoint + " s:" + seriesNumber + ex.getMessage());
        }

        try {
            IJ.log("Examining BioFormats file : " + file.getAbsolutePath());
            r.setId(file.getAbsolutePath());

            r.setSeries(seriesNumber);
            int sizeC = r.getSizeC(); // check channel
            int sizeT = r.getSizeT(); // check timepoint
            ImageStack stack = new ImageStack(sizex, sizey);
            for (int z = startz; z < startz + sizez; z++) {
                ImageProcessor ip = r.openProcessors(r.getIndex(z, channel, timePoint), startx, starty, sizex, sizey)[0];
                stack.addSlice("" + (z + 1), ip);
            }
            res = ImageHandler.wrap(stack);
            res.setGraysLut();

            if (meta != null) {
                Length xy = meta.getPixelsPhysicalSizeX(0);
                Length z = meta.getPixelsPhysicalSizeZ(0);

                if (xy != null && z != null) {
                    res.setScale((Double) xy.value(), (Double) z.value(), xy.unit().getSymbol());
                } else IJ.log("no calibration found");
            }
            r.close();


        } catch (Exception exc) {
            IJ.log("An error occurred while opening image: " + file.getName() + " channel:" + channel + " t:" + timePoint + " s:" + seriesNumber + exc.getMessage());
        }
        return res;
    }
}
