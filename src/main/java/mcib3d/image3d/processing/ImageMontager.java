package mcib3d.image3d.processing;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.process.ImageProcessor;
import java.awt.Color;

public class ImageMontager {
    ImagePlus montage;
    int offX, offY, maxY;

    public ImageMontager(int sx, int sy) {
        montage = IJ.createImage("montage",sx,sy,1,16);
        offX = 0;
        offY = 0;
        maxY = 0;
    }

    public boolean addImage(ImagePlus img){
        return addImage(img, 1);
    }

    public boolean addImage(ImagePlus img, int border) {
        ImageProcessor imp = montage.getProcessor();
        int offX1 = offX + img.getWidth() + border;
        if (offX1 >= montage.getWidth()) {
            offX = img.getWidth() + border;
            int offY1 = offY + maxY + border;
            if (offY1 >= montage.getHeight()) return false;
            offY += maxY + border;
            maxY = img.getHeight();
            imp.insert(img.getProcessor(), 0, offY);
        } else {
            maxY = Math.max(maxY, img.getHeight());
            imp.insert(img.getProcessor(), offX, offY);
            offX += img.getWidth() + border;
        }

        return true;
    }

    public ImagePlus getMontage() {
        // crop blank bottom part
        montage.setRoi(new Roi(0,0,montage.getWidth(), offY + maxY));
        IJ.run(montage, "Enhance Contrast", "saturated=0.35");

        return montage.crop();

    }
}
