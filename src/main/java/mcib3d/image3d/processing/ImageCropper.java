package mcib3d.image3d.processing;

import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ImageCropper {
    ImageHandler image;

    public ImageCropper(ImageHandler image) {
        this.image = image;
    }

    public ImageHandler cropCoordinates(int xmin, int xmax, int ymin, int ymax, int zmin, int zmax) {
        return image.crop3D(image.getTitle() + "_crop_" + xmin, xmin, xmax, ymin, ymax, zmin, zmax);
    }

    public List<ImageHandler> cropSplit(int sizeX, int sizeY, int sizeZ, int overlapX, int overlapY,int overlapZ){
        int startX = 0, startY = 0, startZ = 0;
        // SPLIT
        boolean split = true;
        int c = 1;
        ArrayList<ImageHandler> crops = new ArrayList<>();
        while (split) {
            //System.out.println("\nCropping starts " + startX + " " + startY + " " + startZ);
            ImageHandler crop = image.crop3D("crop_" + c, startX, startX + sizeX - 1, startY, startY + sizeY - 1, startZ, startZ + sizeZ - 1);
            crop.setOffset(startX, startY, startZ);
            crops.add(crop);
            c++;
            // NEXT TILE
            startX += crop.sizeX - overlapX;
            if (startX + overlapX >= image.sizeX) {
                startX = 0;
                startY += crop.sizeY - overlapY;
                if (startY + overlapY >= image.sizeY) {
                    startY = 0;
                    startZ += crop.sizeZ - overlapZ;
                    if (startZ + overlapZ >= image.sizeZ)
                        split = false;
                }
            }
        }

        return crops;
    }

    public ImageHandler cropBoundingBox(BoundingBox box) {
        return cropCoordinates(box.xmin, box.xmax, box.ymin, box.ymax, box.zmin, box.zmax);
    }

    public ImageHandler cropBoundingBoxObject(Object3DInt object3DInt) {
        ImageHandler crop =cropBoundingBox(object3DInt.getBoundingBox());
        crop.setTitle("crop_"+object3DInt.getLabel());
        return crop;
    }

    public ImageHandler cropBoundingBoxMaskObject(Object3DInt object3DInt) {
        ImageHandler cropped = cropBoundingBoxObject(object3DInt);
        ImageHandler mask = cropped.createSameDimensions();
        mask.setOffset(cropped);
        mask.setTitle("crop_"+object3DInt.getLabel());
        object3DInt.drawObjectUsingOffset(mask, object3DInt.getLabel());
        for (int z = 0; z < mask.sizeZ; z++)
            for (int i = 0; i < mask.sizeXY; i++)
                if (mask.getPixel(i, z) > 0) mask.setPixel(i, z, cropped.getPixel(i, z));

        return mask;
    }

    public List<ImageHandler> cropBoundingBoxPopulation(Objects3DIntPopulation population) {
        List<ImageHandler> crops = new LinkedList<>();
        population.getObjects3DInt().forEach(obj -> crops.add(cropBoundingBoxObject(obj)));

        return crops;
    }

    public List<ImageHandler> cropBoundingBoxMaskPopulation(Objects3DIntPopulation population) {
        List<ImageHandler> crops = new LinkedList<>();
        population.getObjects3DInt().forEach(obj -> crops.add(cropBoundingBoxMaskObject(obj)));

        return crops;
    }

    public ImageHandler[] cropBoundingBoxPopulationArray(Objects3DIntPopulation population) {
        ImageHandler[] crops = new ImageHandler[population.getNbObjects()];
        return cropBoundingBoxPopulation(population).toArray(crops);
    }

    public ImageHandler[] cropBoundingBoxMaskPopulationArray(Objects3DIntPopulation population) {
        ImageHandler[] crops = new ImageHandler[population.getNbObjects()];
        return cropBoundingBoxMaskPopulation(population).toArray(crops);
    }




}
