package mcib3d.image3d.processing;

import ij.ImagePlus;
import ij.plugin.ZProjector;
import ij.process.ImageProcessor;
import mcib3d.image3d.ImageHandler;

public class ImageThumbnailer {
    ImageHandler raw;

    public ImageThumbnailer(ImageHandler raw) {
        this.raw = raw;
    }

    public ImagePlus getThumbnail(int sizeX, int sizeY) {
        return computeThumbnail(sizeX, sizeY);
    }

    public ImagePlus getThumbnailWidth(int sizeX) {
        int sizeY = (int) Math.ceil(raw.sizeY *((double)sizeX / (double) raw.sizeX));

        return computeThumbnail(sizeX, sizeY);
    }

    public ImagePlus getThumbnailHeight(int sizeY) {
        int sizeX = (int) Math.ceil(raw.sizeX *((double)sizeY / (double) raw.sizeY));

        return computeThumbnail(sizeX, sizeY);
    }

    private ImagePlus computeThumbnail(int sizeX, int sizeY) {
        ImagePlus im;
        // projection
        if (raw.sizeZ > 1) {
            ZProjector proj = new ZProjector(raw.getImagePlus());
            proj.setMethod(ZProjector.MAX_METHOD);
            proj.doProjection();
            im = proj.getProjection();
        } else {
            im = raw.getImagePlus();
        }
        // resize
        ImageProcessor resized = im.getProcessor().resize(sizeX, sizeY, false);
        im = new ImagePlus(im.getTitle()+"-thumb", resized);

        return im;
    }
}
