package mcib3d.image3d.processing;

import mcib3d.geom2.BoundingBox;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurementsPopulation.MeasurePopulationColocalisation;
import mcib3d.geom2.measurementsPopulation.PairObjects3DInt;
import mcib3d.image3d.ImageHandler;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ImageStitcher {
    List<ImageHandler> images;
    private final static int UNION = 1;
    private final static int INTERSECTION = 2;
    private int mergeMethod = INTERSECTION;


    public ImageStitcher(List<ImageHandler> images) {
        this.images = images;
    }

    public ImageHandler stitchImages() {
        // NEW IMAGE
        BoundingBox box = getMaxBoundingBox();
        int[] size = box.getSizes();
        ImageHandler stitched = images.get(0).createSameType(size[0], size[1], size[2]);
        // MERGE
        images.forEach(crop -> stitched.insert(crop, crop.offsetX, crop.offsetY, crop.offsetZ, false));

        return stitched;
    }

    public int getMergeMethod() {
        return mergeMethod;
    }

    public void setMergeMethod(int mergeMethod) {
        this.mergeMethod = mergeMethod;
    }

    public Objects3DIntPopulation stitchLabelImages() {
        return stitchLabelImages(0);
    }


    public Objects3DIntPopulation stitchLabelImages(double minRatioColo) {
        Objects3DIntPopulation population;
        population = images.stream().map(Objects3DIntPopulation::new).reduce((p1, p2) -> merge2Pop(p1, p2, minRatioColo)).get();

        return population;
    }


    private Objects3DIntPopulation merge2Pop(Objects3DIntPopulation pop1, Objects3DIntPopulation pop2, double minRatioColoc) {
        System.out.println("Merging " + pop1.getName() + " " + pop2.getName() + " " + pop1.getNbObjects() + " " + pop2.getNbObjects());
        System.out.println("Min ratio coloc " + minRatioColoc);
        MeasurePopulationColocalisation colocalisation = new MeasurePopulationColocalisation(pop1, pop2);

        Objects3DIntPopulation population = new Objects3DIntPopulation();
        population.setName(pop1.getName());

        // pop1
        AtomicReference<Float> count = new AtomicReference<>();
        count.set(1.0f);
        pop1.getObjects3DInt().forEach(obj -> {
            List<PairObjects3DInt> list = colocalisation.getPairsObject1(obj.getLabel(), false);
            if (list.isEmpty()) { // no coloc
                population.addObject(obj, count.getAndSet(count.get() + 1.0f));
            } else { // FIXME check first coloc only, if more colocs, largest should be the first one anyway
                Object3DInt obj2 =  list.get(0).getObject3D2();
                double coloc = list.get(0).getPairValue();
                if ((coloc > minRatioColoc * obj.size())&&(coloc > minRatioColoc * obj2.size())) {
                    population.addObject(mergeObjects(obj, obj2), count.getAndSet(count.get() + 1.0f));
                } else { // FIXME what to do if not enough coloc, keep object1 minus coloc ?
                    population.addObject(obj, count.getAndSet(count.get() + 1.0f));
                    obj2.setType(1);
                }
                // set type 1 to other colocs, less volume
                for (int i = 1; i < list.size(); i++) {
                    list.get(i).getObject3D2().setType(1);
                }
            }
        });
        // pop2
        pop2.getObjects3DInt().forEach(obj -> {
            List<PairObjects3DInt> list = colocalisation.getPairsObject2(obj.getLabel());
            if (list.isEmpty())
                population.addObject(obj, count.getAndSet(count.get() + 1.0f));
            else if (obj.getType() == 1) {
                double coloc = list.get(0).getPairValue();
                if (coloc < minRatioColoc * obj.size())
                    population.addObject(obj, count.getAndSet(count.get() + 1.0f));
            }
        });

        return population;
    }

    private Object3DInt mergeObjects(Object3DInt obj1, Object3DInt obj2) {
        if (mergeMethod == UNION)
            return new Object3DComputation(obj1).getUnion(obj2);
        else return new Object3DComputation(obj1).getIntersection(obj2);
    }

    public BoundingBox getMaxBoundingBox() {
        BoundingBox box = new BoundingBox();
        for (ImageHandler image : images) {
            box.adjustBounding(new BoundingBox(image));
        }

        return box;
    }
}
