package mcib3d.image3d.processing;

import mcib3d.geom2.*;
import mcib3d.geom2.measurements.MeasureCentroid;
import mcib3d.image3d.ImageHandler;

import java.util.Random;

public class ImageShuffler {
    private ImageHandler mask;
    private ImageHandler image;
    private Objects3DIntPopulation population;
    int maxTry;

    public ImageShuffler(ImageHandler image) {
        this.image = image;
        population = new Objects3DIntPopulation(image);
        mask = image.createSameDimensions();
        mask.fill(1);
        maxTry = (int) (0.1 * mask.sizeXYZ);
    }

    public void setMask(ImageHandler mask) {
        this.mask = mask;
        maxTry = (int) (0.1 * mask.sizeXYZ);
    }

    public ImageHandler shuffle() {
        ImageHandler copyMask = mask.duplicate();
        ImageHandler shuffled = image.createSameDimensions();

        for (Object3DInt object3D : population.getObjects3DInt()) {
            System.out.println("Shuffling " + object3D.getLabel() + " " + object3D.size());
            int tried = 0;
            Object3DInt objectTry = insertObject(object3D, copyMask);
            while ((tried < maxTry) && (objectTry == null)) {
                tried++;
                objectTry = insertObject(object3D, copyMask);
            }
            if(objectTry == null) return null;
            objectTry.drawObject(copyMask, 0);
            objectTry.drawObject(shuffled);
        }

        return shuffled;
    }

    private Object3DInt insertObject(Object3DInt object3D, ImageHandler mask) {
        Random random = new Random();
        VoxelInt voxelRand = new VoxelInt(random.nextInt(mask.sizeX), random.nextInt(mask.sizeY), random.nextInt(mask.sizeZ), 1);
        int tried = 0;
        while ((tried < maxTry) && (mask.getPixel(voxelRand) == 0)) {
            voxelRand = new VoxelInt(random.nextInt(mask.sizeX), random.nextInt(mask.sizeY), random.nextInt(mask.sizeZ), 1);
            tried++;
        }
        if (mask.getPixel(voxelRand) == 0) return null;
        VoxelInt centre = new MeasureCentroid(object3D).getCentroidRoundedAsVoxelInt();
        // check if one pixel is not in mask -> value 0
        Object3DInt copy = new Object3DComputation(object3D).getObject3DCopy();
        copy.translate(voxelRand.getX() - centre.getX(), voxelRand.getY() - centre.getY(), voxelRand.getZ() - centre.getZ());
        if (!new BoundingBox(mask).contains(copy.getBoundingBox())) return null;
        if (new Object3DComputation(copy).hasOneVoxelValueRange(mask, 0, 0)) return null;
        else return copy;
    }
}
