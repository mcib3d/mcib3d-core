package mcib3d.image3d.processing;

import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.distanceMap3d.EDT;
import mcib3d.utils.ThreadUtil;

import java.util.List;

public class BinaryMultiLabel {

    public static ImageHandler binaryOpen(ImageHandler in, float radius, float radiusZ) {
        return binaryOpen(in, radius, radiusZ, 0);
    }

    public static ImageHandler fillHoles2DMultiLabel(ImageHandler input){
        // NEW
        ImageHandler res = input.createSameDimensions();
        Objects3DIntPopulation population = new Objects3DIntPopulation(input);
        List<ImageHandler> crops = new ImageCropper(input).cropBoundingBoxMaskPopulation(population);
        crops.forEach(crop -> {
            FillHoles2D fillHoles2D = new FillHoles2D((int) crop.getMax(), 0);
            fillHoles2D.process(crop);
            res.insertMask(crop, crop.offsetX, crop.offsetY, crop.offsetZ);
        });
        res.setOffset(input);
        res.setVoxelSize(input);

        return res;
    }

    public  static ImageHandler binaryOpen(ImageHandler in, float radius, float radiusZ, int nbCPUs) {
        // use binary dilate
        ImageHandler eroded = binaryErode(in, radius, radiusZ, nbCPUs);
        ImageHandler open = binaryDilate(eroded, radius, radiusZ, nbCPUs, false);

        return open;
    }

    public static ImageHandler binaryErode(ImageHandler in, float radius, float radiusZ) {
        return binaryErode(in, radius, radiusZ, 0);
    }

    public static ImageHandler binaryErode(ImageHandler in, float radius, float radiusZ, int nbCPUs) {
        if (nbCPUs == 0) {
            nbCPUs = ThreadUtil.getNbCpus();
        }

        float value = (float) in.getMax();


        // resize
        int reZ = radiusZ > 0 ? 1:0;
        ImageHandler resize = in.enlarge(1, 1, reZ);

        float scaleZ = radiusZ > 0 ? radius/radiusZ : 0;

        ImageFloat edm = EDT.run(resize, 0, 1, scaleZ, false, nbCPUs);
        ImageHandler thresholded = edm.thresholdAboveExclusive(radius);
        edm.flush();
        ImageHandler eroded = resize.createSameDimensions();
        eroded = eroded.addImage(thresholded,1,1);
        eroded.replacePixelsValueFloat(255, value);
        eroded.setOffset(in.offsetX - 1, in.offsetY - 1, in.offsetZ - reZ);
        eroded.setVoxelSize(in);


        return eroded;
    }

    private static ImageHandler binaryDilate2D(ImageHandler in, float radius, boolean enlarge) {
        ImageHandler resize = in;
        // resize
        int reX = (int) (radius + 1);
        int reY = (int) (radius + 1);
        int reZ = 0;
        if (enlarge) resize = in.enlarge(reX, reY, reZ);
        ImageHandler temp = FastFilters3D.filterImage(resize, FastFilters3D.MAX, radius, radius, 0, 1, false);
        temp.setVoxelSize(in);
        if (enlarge)
            temp.setOffset(in.offsetX - reX, in.offsetY - reY, in.offsetZ - reZ);
        else
            temp.setOffset(in);

        return temp;
    }

    private static ImageHandler binaryErode2D(ImageHandler in, float radius) {
        ImageHandler temp =  FastFilters3D.filterImage(in, FastFilters3D.MIN, radius, radius, 0, 1, false);
        temp.setOffset(in);
        temp.setVoxelSize(in);

        return temp;
    }


    public static ImageHandler binaryDilate(ImageHandler in, float radius, float radiusZ) {
        return binaryDilate(in, radius, radiusZ, 0);
    }

    // if no resize of the image, object at the border may be truncated
    public static ImageHandler binaryDilate(ImageHandler in, float radius, float radiusZ, int nbCPUs, boolean enlarge) {
        if (nbCPUs == 0) {
            nbCPUs = ThreadUtil.getNbCpus();
        }

        float value = (float) in.getMax();
        ImageHandler resize = in.duplicate();

        // resize
        int reX = (int) (radius + 1);
        int reY = (int) (radius + 1);
        int reZ = (int) (radiusZ + 1);
        if (enlarge) resize = in.enlarge(reX, reY, reZ);

        float scaleZ = radiusZ > 0 ? radius/radiusZ : 0;

        ImageFloat edm = EDT.run(resize, 0, 1, scaleZ, true, nbCPUs);
        ImageHandler temp = edm.threshold(radius,true,false);
        edm.flush();
        resize = resize.addImage(temp, 0,1);
        resize.replacePixelsValueFloat(255, value);
        if (enlarge)
            resize.setOffset(in.offsetX - reX, in.offsetY - reY, in.offsetZ - reZ);
        else
            resize.setOffset(in);
        resize.setVoxelSize(in);

        return resize;
    }


    public static  ImageHandler binaryDilate(ImageHandler in, float radius, float radiusZ, int nbCPUs) {
        // use generic version of binaryDilate
        return binaryDilate(in, radius, radiusZ, nbCPUs, true);
    }

    public static ImageHandler binaryClose(ImageHandler in, float radius, float radiusZ) {
        return binaryClose(in, radius, radiusZ, 0);
    }

    private static ImageHandler binaryClose2D(ImageHandler in, float radius) {
        ImageHandler dilated = binaryDilate2D(in, radius, true);
        ImageHandler close = binaryErode2D(dilated, radius);
        // crop image
        int ox = in.offsetX - dilated.offsetX;
        int oy = in.offsetY - dilated.offsetY;
        int oz = in.offsetZ - dilated.offsetZ;

        return close.crop3D("binaryClose", ox, ox + in.sizeX - 1, oy, oy + in.sizeY - 1, oz, oz + in.sizeZ - 1);
    }

    public static ImageHandler binaryClose(ImageHandler in, float radius, float radiusZ, int nbCPUs) {
        // radiusz 0
        if(radiusZ <= 0){
            ImageHandler dilated = binaryDilate2D(in, radius,true);
            ImageHandler close = binaryErode2D(dilated, radius);

            // crop image
            int ox = in.offsetX - close.offsetX;
            int oy = in.offsetY - close.offsetY;
            int oz = in.offsetZ - close.offsetZ;

            ImageHandler res = close.crop3D("binaryClose", ox, ox + in.sizeX - 1, oy, oy+ in.sizeY - 1, oz, oz + in.sizeZ - 1);

            return res;
        }
        // use binary dilate
        ImageHandler dilated = binaryDilate(in, radius, radiusZ, nbCPUs, true);
        ImageHandler close = binaryErode(dilated, radius, radiusZ, nbCPUs);

        // crop image
        int ox = in.offsetX - close.offsetX;
        int oy = in.offsetY - close.offsetY;
        int oz = in.offsetZ - close.offsetZ;

        ImageHandler res = close.crop3D("binaryClose", ox, ox + in.sizeX - 1, oy, oy+ in.sizeY - 1, oz, oz + in.sizeZ - 1);

        return res;
    }


    public static ImageHandler binaryOpenMultilabel(ImageHandler in, float radius, float radiusZ) {
        return binaryOpenMultilabel(in, radius, radiusZ, 0);

    }

    public static ImageHandler binaryOpenMultilabel(ImageHandler input, float radiusXY, float radiusZ, int nbCPUs) {
        // NEW
        ImageHandler res = input.createSameDimensions();
        Objects3DIntPopulation population = new Objects3DIntPopulation(input);
        List<ImageHandler> crops = new ImageCropper(input).cropBoundingBoxMaskPopulation(population);
        crops.forEach(crop -> {
            ImageHandler close = binaryOpen(crop, radiusXY, radiusZ, nbCPUs);
            res.insertMask(close, close.offsetX, close.offsetY, close.offsetZ);
        });
        res.setOffset(input);
        res.setVoxelSize(input);

        return res;
    }

    public static ImageHandler binaryErodeMultiLabel(ImageHandler in, float radiusXY, float radiusZ){
        return binaryErodeMultiLabel(in, radiusXY, radiusZ, 0);
    }

    public static ImageHandler binaryErodeMultiLabel(ImageHandler input, float radiusXY, float radiusZ, int nbCPUs){
        // NEW
        ImageHandler res = input.createSameDimensions();
        Objects3DIntPopulation population = new Objects3DIntPopulation(input);
        List<ImageHandler> crops = new ImageCropper(input).cropBoundingBoxMaskPopulation(population);
        crops.forEach(crop -> {
            ImageHandler erode = binaryErode(crop, radiusXY, radiusZ, nbCPUs);
            res.insertMask(erode, erode.offsetX, erode.offsetY, erode.offsetZ);
        });
        res.setOffset(input);
        res.setVoxelSize(input);

        return res;
    }

    public static ImageHandler binaryCloseMultilabel(ImageHandler in, float radiusXY, float radiusZ) {
        return binaryCloseMultilabel(in, radiusXY, radiusZ, 0);
    }

    public static ImageHandler binaryCloseMultilabel(ImageHandler input, float radiusXY, float radiusZ, int nbCPUs) {
        // NEW
        ImageHandler res = input.createSameDimensions();
        Objects3DIntPopulation population = new Objects3DIntPopulation(input);
        List<ImageHandler> crops = new ImageCropper(input).cropBoundingBoxMaskPopulation(population);
        crops.forEach(crop -> {
            ImageHandler close = binaryClose(crop, radiusXY, radiusZ, nbCPUs);
            res.insertMask(close, close.offsetX, close.offsetY, close.offsetZ);
        });
        res.setOffset(input);
        res.setVoxelSize(input);

        return res;
    }

    public static ImageHandler binaryDilateMultilabel(ImageHandler in, float radiusXY, float radiusZ) {
        return binaryDilateMultilabel(in, radiusXY, radiusZ, 0);
    }

    public static ImageHandler binaryDilateMultilabel(ImageHandler input, float radiusXY, float radiusZ, int nbCPUs) {
        // NEW
        ImageHandler res = input.createSameDimensions();
        Objects3DIntPopulation population = new Objects3DIntPopulation(input);
        List<ImageHandler> crops = new ImageCropper(input).cropBoundingBoxMaskPopulation(population);
        crops.forEach(crop -> {
            ImageHandler close = binaryDilate(crop, radiusXY, radiusZ, nbCPUs);
            res.insertMask(close, close.offsetX, close.offsetY, close.offsetZ);
        });
        res.setOffset(input);
        res.setVoxelSize(input);

        return res;
    }
}
