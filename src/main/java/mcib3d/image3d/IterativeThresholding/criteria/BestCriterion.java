package mcib3d.image3d.IterativeThresholding.criteria;

import mcib3d.utils.ArrayUtil;

import java.util.ArrayList;

/**
 * Created by thomasb on 20/4/16.
 */
public interface BestCriterion {
    int computeBestCriterion(ArrayUtil list);
}
