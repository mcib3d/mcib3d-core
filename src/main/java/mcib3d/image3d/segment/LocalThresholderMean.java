package mcib3d.image3d.segment;

import mcib3d.utils.ArrayUtil;

public class LocalThresholderMean extends LocalThresholder{
    private final float radius0;
    private final float radius1;
    private final float radius2;
    private final double localWeight;

    public LocalThresholderMean(float r0, float r1, float r2, double weight){
        radius0 = r0;
        radius1 = r1;
        radius2 = r2;
        localWeight = weight;
    }

    @Override
    public float getLocalThreshold(int xc, int yc, int zc) {
        double mspot, mback;
        int sx = getRawImage().sizeX;
        int sy = getRawImage().sizeY;
        int sz = getRawImage().sizeZ;

        float rad0 = radius0;
        float rad1 = radius1;
        float rad2 = radius2;

        double weight = localWeight;

        boolean WATERSHED = getWatershedImage() != null;

        if (!WATERSHED) {
            // no watershed image
            // central value
            ArrayUtil neigh = getRawImage().getNeighborhood(xc, yc, zc, rad0, rad0, rad0);
            mspot = neigh.getMean();
            // background value
            neigh = getRawImage().getNeighborhoodLayer(xc, yc, zc, rad1, rad2);
            mback = neigh.getMean();
        } else {
            // take into account watershed image
            double rad02 = rad0 * rad0;
            double rad12 = rad1 * rad1;
            double rad22 = rad2 * rad2;
            int zd, zf, yd, yf, xd, xf;
            int nbspot = 0;
            int nbback = 0;
            double sumspot = 0;
            double sumback = 0;
            double dist;
            float pix;
            float water;
            float waterc = (int) getWatershedImage().getPixel(xc, yc, zc);

            // compute bounding
            zd = (int) (zc - rad2);
            if (zd < 0) {
                zd = 0;
            }
            zf = (int) (zc + rad2);
            if (zf >= sz) {
                zf = sz;
            }
            yd = (int) (yc - rad2);
            if (yd < 0) {
                yd = 0;
            }
            yf = (int) (yc + rad2);
            if (yf >= sy) {
                yf = sy;
            }
            xd = (int) (xc - rad2);
            if (xd < 0) {
                xd = 0;
            }
            xf = (int) (xc + rad2);
            if (xf >= sx) {
                xf = sx;
            }

            for (int z = zd; z < zf; z++) {
                for (int y = yd; y < yf; y++) {
                    for (int x = xd; x < xf; x++) {
                        dist = (x - xc) * (x - xc) + (y - yc) * (y - yc) + (z - zc) * (z - zc);
                        if (dist <= rad02) {
                            water = getWatershedImage().getPixel(x, y, z);
                            if (water == waterc) {
                                pix = getRawImage().getPixel(x, y, z);
                                sumspot += pix;
                                nbspot++;
                            }
                        } else if ((dist >= rad12) && (dist <= rad22)) {
                            water = (int) getWatershedImage().getPixel(x, y, z);
                            if (water == waterc) {
                                pix = getRawImage().getPixel(x, y, z);
                                sumback += pix;
                                nbback++;
                            }
                        }
                    }
                }
            }
            // test nb minimum of pixels in spot and background
            if (nbspot > 1) {
                mspot = sumspot / nbspot;
            } else {
                return -1;
            }
            if (nbback > 1) {
                mback = sumback / nbback;
            } else {
                return -1;
            }
        }

        return (float) (mspot * weight + (1.0 - weight) * mback);
    }
}
