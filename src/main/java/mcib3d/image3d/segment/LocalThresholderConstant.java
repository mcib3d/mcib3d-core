package mcib3d.image3d.segment;

public class LocalThresholderConstant extends LocalThresholder{
    float threshold;

    public LocalThresholderConstant() {
        super();
    }

    public LocalThresholderConstant(float value){
        threshold = value;
    }

    public float getThreshold() {
        return threshold;
    }

    public void setThreshold(float threshold) {
        this.threshold = threshold;
    }

    @Override
    public float getLocalThreshold(int xc, int yc, int zc) {
        return getThreshold();
    }
}
