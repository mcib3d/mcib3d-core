package mcib3d.image3d.segment;

import com.sun.xml.bind.v2.runtime.reflect.Lister;
import ij.IJ;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class SpotSegmenterBlock extends SpotSegmenter {
    public SpotSegmenterBlock() {
        super();
    }

    @Override
    public List<VoxelInt> segmentSpot(int xc, int yc, int zc, float localThreshold, float value) {
        boolean changement = true;
        int xfin = xc + 1;
        int yfin = yc + 1;
        int zfin = zc + 1;
        int sens = 1;

        if (getLabelImage() == null) {
            this.createLabelImage();
        }

        // pixel already segmented ?
        if (getLabelImage().getPixel(xc, yc, zc) > 0) {
            return null;
        }
        getLabelImage().setPixel(xc, yc, zc, value);
        LinkedList<VoxelInt> object = new LinkedList<>();
        object.add(new VoxelInt(xc, yc, zc, value));
        int volume = 1;
        int i;
        int j;
        int k;
        int l;
        int m;
        int n;
        int sx = getRawImage().sizeX;
        int sy = getRawImage().sizeY;
        int sz = getRawImage().sizeZ;

        ImageHandler original = getRawImage();

        ArrayList<VoxelInt> neigh;
        Iterator it;
        VoxelInt tmpneigh;
        float pixelCenter;
        float waterne = 0, water = 0;
        boolean ok;

        // watershed ?
        boolean WATERSHED = (getWatershedImage() != null);

        //int ite = 0;
        while (changement) {
            //IJ.log("Ite " + ite + " " + xdep + " " + xfin);
            //ite++;
            changement = false;
            for (k = sens == 1 ? zc : zfin; ((sens == 1 && k <= zfin) || (sens == -1 && k >= zc)); k += sens) {
                for (j = sens == 1 ? yc : yfin; ((sens == 1 && j <= yfin) || (sens == -1 && j >= yc)); j += sens) {
                    for (i = sens == 1 ? xc : xfin; ((sens == 1 && i <= xfin) || (sens == -1 && i >= xc)); i += sens) {
                        if (getLabelImage().contains(i, j, k) && getLabelImage().getPixel(i, j, k) == value) {
                            pixelCenter = original.getPixel(i, j, k);
                            if (WATERSHED) {
                                water = (int) getWatershedImage().getPixel(i, j, k);
                            }
                            // create neighbors list
                            neigh = new ArrayList<>();
                            for (n = k - 1; n < k + 2; n++) {
                                for (m = j - 1; m < j + 2; m++) {
                                    for (l = i - 1; l < i + 2; l++) {
                                        if ((l >= 0) && (l < sx) && (m >= 0) && (m < sy) && (n >= 0) && (n < sz)) {
                                            if (WATERSHED) {
                                                waterne = (int) getWatershedImage().getPixel(l, m, n);
                                            }
                                            if ((getLabelImage().getPixel(l, m, n) == 0) && (original.getPixel(l, m, n) >= localThreshold) && (waterne == water)) {
                                                neigh.add(new VoxelInt(l, m, n, original.getPixel(l, m, n)));
                                            }
                                        }
                                    } //l
                                } // m
                            } //n

                            // analyse list
                            // empty
                            ok = !neigh.isEmpty();
                            // test 1 neighbor
                            if (neigh.size() == 1) {
                                ok = false;
                            }
                            // test all neighbors
                            it = neigh.iterator();
                            while (it.hasNext() && ok) {
                                tmpneigh = (VoxelInt) it.next();
                                // BLOCK
                                if (tmpneigh.getValue() > pixelCenter) {
                                    ok = false;
                                    break;
                                }
                            }

                            if (ok) {
                                changement = true;
                                it = neigh.iterator();
                                while (it.hasNext()) {
                                    tmpneigh = (VoxelInt) it.next();
                                    l = tmpneigh.getX();
                                    m = tmpneigh.getY();
                                    n = tmpneigh.getZ();
                                    getLabelImage().setPixel(l, m, n, value);
                                    object.add(new VoxelInt(l, m, n, value));
                                    volume++;
                                    // update min-max
                                    if (l < xc) {
                                        xc--;
                                    }
                                    if (l > xfin) {
                                        xfin++;
                                    }
                                    if (m < yc) {
                                        yc--;
                                    }
                                    if (m > yfin) {
                                        yfin++;
                                    }
                                    if (n < zc) {
                                        zc--;
                                    }
                                    if (n > zfin) {
                                        zfin++;
                                    }
                                }
                            } // BLOCKING
                            // else {
                            //     it = neigh.iterator();
                            //     while ((it != null) && (it.hasNext())) {
                            //         tmpneigh = (Voxel3D) it.next();
                            //         l = (int) tmpneigh.getX();
                            //         m = (int) tmpneigh.getY();
                            //         n = (int) tmpneigh.getZ();
                            // 0 do not change 1 exclude from future seg
                            //labelImage.putPixel(l, m, n, 0);
                            //    }
                            //}
                        } // labelimage==value
                    } //i
                } // j
            }// k
            sens *= -1;
        }//while
        return object;
    }
}
