package mcib3d.image3d.segment;

import ij.IJ;
import ij.process.AutoThresholder;
import ij.util.ThreadUtil;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageInt;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.processing.FillHoles2D;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by Thomas Boudier on 22/8/16.
 */
public class SegNuclei {
    ImageHandler rawImage;
    ImageHandler watImage;
    //Objects3DIntPopulation population = null;
    //Map<Double, Object3DInt> object3DHashMap;
    // method to use for threshold
    AutoThresholder.Method method = AutoThresholder.Method.Otsu;
    // fill holes regions
    boolean fillHoles = true;

    public boolean verbose = false;

    public SegNuclei(ImageHandler rawImage, ImageHandler watImage) {
        this.rawImage = rawImage;
        this.watImage = watImage;
        //setPopulation(new Objects3DIntPopulation(watImage));
    }

    public void setThresholdMethod(AutoThresholder.Method method) {
        this.method = method;
    }

    //public void setPopulation(Objects3DIntPopulation pop) {
        //population = pop;
    //}

    public ImageInt getSeg() {
        return process();
    }

    private ImageInt process() {
        TreeMap<Float, int[]> bounds = watImage.getBounds(false);
        final ImageHandler[] watershedRegions = watImage.crop3D(bounds);
        final ImageHandler[] rawRegions = rawImage.crop3D(bounds);
        final ImageByte[] thresholdedRegions = new ImageByte[rawRegions.length];
        final int nbRegions = watershedRegions.length;
        if (verbose) IJ.log("Nb regions " + rawRegions.length);
        Instant time0 = Instant.now();

        // link population and watershed
        /*
        object3DHashMap = new HashMap<>(population.getNbObjects());
        for (Object3DInt object3D : population.getObjects3DInt()) {
            MeasureIntensityHist intensity = new MeasureIntensityHist(object3D, watImage);
            object3DHashMap.put(intensity.getModeValue(), object3D);
        }

         */

        final AtomicInteger ai = new AtomicInteger(0);

        final int nbCPUs = ThreadUtil.getNbCpus();
        final int dec = (int) Math.ceil((double) nbRegions / (double) nbCPUs);
        Thread[] threads = ThreadUtil.createThreadArray(nbCPUs);
        for (int iThread = 0; iThread < nbCPUs; iThread++) {
            threads[iThread] = new Thread(() -> {
                for (int i = ai.getAndIncrement(); i < nbCPUs; i = ai.getAndIncrement()) {
                    for (int r = i * dec; r < (i + 1) * dec; r++) {
                        if (r >= nbRegions) {
                            break;
                        }
                        ImageHandler wa = watershedRegions[r];
                        ImageHandler ra = rawRegions[r];
                        if (verbose) IJ.log("Processing region " + r + " with cpu " + i);
                        thresholdedRegions[r] = processRegion(wa, ra);
                        if (thresholdedRegions[r] == null) IJ.log("process null region " + r);
                    }
                }
            });
        }
        ThreadUtil.startAndJoin(threads);

        FillHoles2D fillHoles2D = new FillHoles2D();
        // copy offset to thresholdedRegions
        if (verbose) IJ.log("Reassembling the regions");
        for (int i = 0; i < nbRegions; i++) {
            if (rawRegions[i] == null) {
                IJ.log("raw " + i + " " + null);
            }
            if (thresholdedRegions[i] == null) {
                IJ.log("thres " + i + " " + null);
            }
            if ((rawRegions[i] != null) && (thresholdedRegions[i] != null)) {
                thresholdedRegions[i].setOffset(rawRegions[i]);
                // fill holes here  (pb with multi-threading)
                if (fillHoles) fillHoles2D.process(thresholdedRegions[i]);
            }
        }

        ImageInt mergedImage = ImageShort.merge3DBinary(thresholdedRegions, watImage.sizeX, watImage.sizeY, watImage.sizeZ);

        Objects3DIntPopulation population = new Objects3DIntPopulation(mergedImage);

        int max = (int) mergedImage.getMax() + 1;
        for (Object3DInt object3D : population.getObjects3DInt()) {
            List<Object3DInt> conn = new Object3DComputation(object3D).getConnexComponents();
            if (conn.size() > 1) {
                IJ.log("not connex " + object3D);
                AtomicInteger aj = new AtomicInteger(max);
                // not connex, draw other objects or keep biggest ?
                conn.forEach(C -> C.drawObject(mergedImage, aj.getAndIncrement()));
            }
        }
        Instant time1 = Instant.now();
        if (verbose) IJ.log("done in " + Duration.between(time0, time1));

        return mergedImage;
    }


    private ImageByte processRegion(ImageHandler watershed, ImageHandler raw) {
        ImageByte thresholded;
        AutoThresholder thresholder = new AutoThresholder();
        int value = (int) watershed.getPixel(watershed.sizeX / 2, watershed.sizeY / 2, watershed.sizeZ / 2);
        ImageByte mask = watershed.thresholdRangeInclusive(value, value);
        int[] hist = raw.getHistogram(mask, 256, raw.getMin(), raw.getMax());
        double threshold8bits = thresholder.getThreshold(method, hist);
        double step = (raw.getMax() - raw.getMin() + 1) / 256.0;
        thresholded = raw.thresholdAboveExclusive((float) (threshold8bits * step + raw.getMin()));
        thresholded.intersectMask(mask);

        Object3DInt object3D = new Object3DInt(thresholded);
        List<Object3DInt> conn = new Object3DComputation(object3D).getConnexComponents();
        if (conn.size() > 1) { // keep biggest object
            final AtomicReference<Double> max = new AtomicReference<>(); max.set(0.0);
            final AtomicReference<Float> maxi = new AtomicReference<>(); maxi.set(0.0f);
            conn.forEach(C -> {
                if (C.size() > max.get()) {
                    max.set(C.size());
                    maxi.set(C.getLabel());
                }
            });
            conn.stream().filter(C -> C.getLabel() != maxi.get()).forEach(C -> C.drawObject(thresholded, 0));
        }

        return thresholded;
    }
}
