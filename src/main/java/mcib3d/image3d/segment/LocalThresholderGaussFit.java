package mcib3d.image3d.segment;

import ij.gui.Plot;
import ij.measure.CurveFitter;
import mcib3d.geom.Object3D;
import mcib3d.utils.ArrayUtil;

public class LocalThresholderGaussFit extends LocalThresholder {
    private final int maxRadius;
    private final double gaussPC;

   public LocalThresholderGaussFit(int maxRadius, double gaussPC){
       this.maxRadius = maxRadius;
       this.gaussPC = gaussPC;
   }

    @Override
    public float getLocalThreshold(int xc, int yc, int zc) {
       double[] gauss = gaussianFit(xc,yc,zc);
        if (gauss != null) {
            double thresh = CurveFitter.f(CurveFitter.GAUSSIAN, gauss, gaussPC * gauss[3]);
            return (float) thresh;
        }

        return 0;
    }

    private  double[] gaussianFit(int x, int y, int z) {
        double[] gaussFit;
        double[] params;
        boolean WATERSHED = getWatershedImage() != null;
        if (WATERSHED) {
            gaussFit = getRawImage().radialDistribution(x, y, z, maxRadius, Object3D.MEASURE_INTENSITY_AVG, getWatershedImage());
        } else {
            gaussFit = getRawImage().radialDistribution(x, y, z, maxRadius);
        }
        params = ArrayUtil.fitGaussian(gaussFit, 3, maxRadius);

        return params;
    }
}
