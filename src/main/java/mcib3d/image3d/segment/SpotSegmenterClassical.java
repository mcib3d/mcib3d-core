package mcib3d.image3d.segment;

import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

import java.util.LinkedList;
import java.util.List;

public class SpotSegmenterClassical extends SpotSegmenter{
    public SpotSegmenterClassical() {
        super();
    }

    @Override
    public List<VoxelInt> segmentSpot(int x, int y, int z, float localThreshold, float value) {
        boolean change = true;
        int xEnd = x + 1;
        int yEnd = y + 1;
        int zEnd = z + 1;
        int sens = 1;

        if (getLabelImage() == null) {
            this.createLabelImage();
        }

        // pixel already segmented ?
        if (getLabelImage().getPixel(x, y, z) > 0) {
            return null;
        }
        // watershed ?
        boolean WATERSHED = (getWatershedImage() != null);
        getLabelImage().setPixel(x, y, z, value);
        List<VoxelInt> object = new LinkedList<>();
        object.add(new VoxelInt(x, y, z, value));
        int i;
        int j;
        int k;
        int l;
        int m;
        int n;

        ImageHandler original = getRawImage();
        float waterCenter = 0;
        float water = 0;

        while (change) {
            change = false;
            for (k = sens == 1 ? z : zEnd; ((sens == 1 && k <= zEnd) || (sens == -1 && k >= z)); k += sens) {
                for (j = sens == 1 ? y : yEnd; ((sens == 1 && j <= yEnd) || (sens == -1 && j >= y)); j += sens) {
                    for (i = sens == 1 ? x : xEnd; ((sens == 1 && i <= xEnd) || (sens == -1 && i >= x)); i += sens) {
                        if (getLabelImage().contains(i, j, k) && getLabelImage().getPixel(i, j, k) == value) {
                            if (WATERSHED) {
                                waterCenter = getWatershedImage().getPixel(i, j, k);
                            }
                            for (n = k - 1; n < k + 2; n++) {
                                for (m = j - 1; m < j + 2; m++) {
                                    for (l = i - 1; l < i + 2; l++) {
                                        if (getLabelImage().contains(l, m, n)) {
                                            if (WATERSHED) {
                                                water = getWatershedImage().getPixel(l, m, n);
                                            }
                                            if ((getLabelImage().getPixel(l, m, n) == 0) && (original.getPixel(l, m, n) >= localThreshold) && (water == waterCenter)) {
                                                getLabelImage().setPixel(l, m, n, value);
                                                // original.putPixel(l, m, n, 0);
                                                // add voxel to object
                                                object.add(new VoxelInt(l, m, n, value));
                                                // update min-max
                                                if (l < x) {
                                                    x--;
                                                }
                                                if (l > xEnd) {
                                                    xEnd++;
                                                }
                                                if (m < y) {
                                                    y--;
                                                }
                                                if (m > yEnd) {
                                                    yEnd++;
                                                }
                                                if (n < z) {
                                                    z--;
                                                }
                                                if (n > zEnd) {
                                                    zEnd++;
                                                }
                                                change = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            sens *= -1;
        }

        return object;
    }
}
