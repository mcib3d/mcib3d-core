package mcib3d.image3d.segment;

import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;

import java.util.LinkedList;
import java.util.List;

public class SpotSegmenterMax extends SpotSegmenter {

    public SpotSegmenterMax() {
        super();
    }

    @Override
    public List<VoxelInt> segmentSpot(int xc, int yc, int zc, float localThreshold, float value) {
        boolean changement = true;
        int xfin = xc + 1;
        int yfin = yc + 1;
        int zfin = zc + 1;

        int sens = 1;

        if (getLabelImage() == null) {
            this.createLabelImage();
        }

        // pixel already segmented ?
        if (getLabelImage().getPixel(xc, yc, zc) > 0) {
            return null;
        }
        // watershed ?
        boolean WATERSHED = (getWatershedImage() != null);

        getLabelImage().setPixel(xc, yc, zc, value);

        List<VoxelInt> object = new LinkedList<>();
        object.add(new VoxelInt(xc, yc, zc, value));

        int i;
        int j;
        int k;
        int l;
        int m;
        int n;

        ImageHandler original = getRawImage();
        float pixelCenter;
        float waterCenter = 0;
        float water = 0;

        while (changement) {
            changement = false;
            for (k = sens == 1 ? zc : zfin; ((sens == 1 && k <= zfin) || (sens == -1 && k >= zc)); k += sens) {
                for (j = sens == 1 ? yc : yfin; ((sens == 1 && j <= yfin) || (sens == -1 && j >= yc)); j += sens) {
                    for (i = sens == 1 ? xc : xfin; ((sens == 1 && i <= xfin) || (sens == -1 && i >= xc)); i += sens) {
                        if (getLabelImage().contains(i, j, k) && getLabelImage().getPixel(i, j, k) == value) {
                            if (WATERSHED) {
                                waterCenter = (int) getWatershedImage().getPixel(i, j, k);
                            }
                            pixelCenter = original.getPixel(i, j, k);
                            for (n = k - 1; n < k + 2; n++) {
                                for (m = j - 1; m < j + 2; m++) {
                                    for (l = i - 1; l < i + 2; l++) {
                                        if (getLabelImage().contains(l, m, n)) {
                                            if (WATERSHED) {
                                                water = (int) getWatershedImage().getPixel(l, m, n);
                                            }
                                            if ((getLabelImage().getPixel(l, m, n) == 0) && (original.getPixel(l, m, n) >= localThreshold) && (original.getPixel(l, m, n) <= pixelCenter) && (water == waterCenter)) {
                                                getLabelImage().setPixel(l, m, n, value);
                                                //original.putPixel(l, m, n, 0);
                                                // add voxel to object
                                                object.add(new VoxelInt(l, m, n, value));
                                                // update min-max
                                                if (l < xc) {
                                                    xc--;
                                                }
                                                if (l > xfin) {
                                                    xfin++;
                                                }
                                                if (m < yc) {
                                                    yc--;
                                                }
                                                if (m > yfin) {
                                                    yfin++;
                                                }
                                                if (n < zc) {
                                                    zc--;
                                                }
                                                if (n > zfin) {
                                                    zfin++;
                                                }
                                                changement = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            sens *= -1;
        }

        return object;
    }
}
