package mcib3d.image3d.segment;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.image3d.ImageByte;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;
import mcib3d.image3d.ImageShort;

import java.util.List;

public class HysteresisSegment {
    private final float minThreshold;
    private final float maxThreshold;

    private static final float HIGH = 255;
    private static final float LOW = 128;

    public HysteresisSegment(float minThreshold, float maxThreshold) {
        this.minThreshold = minThreshold;
        this.maxThreshold = maxThreshold;
    }

    public ImageHandler hysteresis(ImageHandler img, boolean label) {
        // first threshold the image
        ImageByte multi = new ImageByte(img.getTitle() + "_Multi", img.sizeX, img.sizeY, img.sizeZ);
        for (int z = 0; z < img.sizeZ; z++) {
            for (int xy = 0; xy < img.sizeXY; xy++) {
                if (img.getPixel(xy, z) > maxThreshold) {
                    multi.setPixel(xy, z, HIGH);
                } else if (img.getPixel(xy, z) > minThreshold) {
                    multi.setPixel(xy, z, LOW);
                }
            }
        }

        ImageHandler thresholded = multi.thresholdAboveInclusive(LOW);
        ImageLabeller labeller = new ImageLabeller();
        List<Object3DInt> objects = labeller.getObjects3D(thresholded);

        ImageHandler hyst;
        if(label) hyst = new ImageShort("HystLabel_" + img.getTitle(), multi.sizeX, multi.sizeY, multi.sizeZ);
        else hyst = new ImageByte("HystBin_" + img.getTitle(), multi.sizeX, multi.sizeY, multi.sizeZ);
        hyst.setScale(img);
        int val = 1;
        for (Object3DInt object3D : objects) {
            Object3DComputation computation = new Object3DComputation(object3D);
            if(computation.hasOneVoxelValueRange(multi,HIGH,HIGH)){
                if (label)
                    object3D.drawObject(hyst, val++);
                else object3D.drawObject(hyst, 255);
            }
        }

        return hyst;
    }
}
