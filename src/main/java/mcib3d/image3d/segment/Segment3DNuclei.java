package mcib3d.image3d.segment;

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.Binary;
import ij.plugin.filter.EDM;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.AutoThresholder;
import ij.process.ByteProcessor;
import mcib3d.image3d.*;

import java.util.stream.IntStream;

public class Segment3DNuclei {
    private AutoThresholder.Method method = AutoThresholder.Method.Otsu;
    private boolean separate = true;
    private float manual = 0;
    ImageHandler input;

    boolean verboseDetails = false;

    public Segment3DNuclei(ImageHandler image) {
        input = image;
    }

    public void setMethod(AutoThresholder.Method method) { // AutoThreshold method
        this.method = method;
    }

    public void setSeparate(boolean separate) { // use IJ watershed
        this.separate = separate;
    }

    public void setManual(float manual) { // value for thresholding (0 = automatic)
        this.manual = manual;
    }

    private float getThreshold(ImagePlus plus) {
        if (manual > 0) return manual;
        // compute histogram
        ImageHandler imageHandler = ImageHandler.wrap(plus);
        ImageStats stat = imageHandler.getImageStats(null);
        int[] histogram = stat.getHisto256();
        double binSize = stat.getHisto256BinSize();
        double min = stat.getMin();

        AutoThresholder at = new AutoThresholder();
        float threshold = at.getThreshold(method, histogram);
        if (plus.getBitDepth() > 8)
            threshold = (float) (threshold * binSize + min);

        IJ.log(method.name() + " threshold (2D) :" + threshold);

        return threshold;
    }

    public ImageHandler segment() {
        // do projection
        IJ.log("Performing maximum Z-projection");
        ZProjector zProjector = new ZProjector();
        zProjector.setMethod(ZProjector.MAX_METHOD);
        zProjector.setStartSlice(1);
        zProjector.setStopSlice(input.sizeZ);
        zProjector.setImage(input.getImagePlus());
        zProjector.doProjection();
        ImagePlus plus = zProjector.getProjection();

        // threshold
        IJ.log("Performing 2D thresholding");
        float threshold = getThreshold(plus);
        plus.getProcessor().threshold((int) threshold);
        Duplicator duplicator = new Duplicator();
        ImagePlus bin2 = duplicator.run(plus);
        ByteProcessor byteProcessor = bin2.getProcessor().convertToByteProcessor();
        bin2.setProcessor(byteProcessor);

        // fill holes
        IJ.log("Performing Fill Holes");
        IJ.run("Options...", "iterations=1 count=1 black");
        ij.plugin.filter.Binary binary = new Binary();
        binary.setup("fill", bin2);
        binary.run(bin2.getProcessor());

        // watershed IJ = separate
        if (separate) {
            IJ.log("Performing IJ Watershed separate");
            ij.plugin.filter.EDM edm = new EDM();
            edm.setup("watershed", bin2);
            edm.toWatershed(bin2.getProcessor());
        }
        // count mask
        IJ.log("Performing IJ Analyze Particles");
        ParticleAnalyzer particleAnalyzer = new ParticleAnalyzer(ParticleAnalyzer.SHOW_ROI_MASKS, ParticleAnalyzer.AREA, null, 10, 1000000, 0, 1);
        particleAnalyzer.setHideOutputImage(true);
        particleAnalyzer.analyze(bin2);
        ImagePlus seg2D = particleAnalyzer.getOutputImage();

        // expand 3D and do deep segmentation
        IJ.log("Expanding in 3D and performing 3D segmentation on each region");
        ImageInt seg3D = expand3D(ImageInt.wrap(seg2D), input.sizeZ);
        // perform deep segmentation
        SegNuclei deepSeg = new SegNuclei(input, seg3D);
        deepSeg.setThresholdMethod(method);
        deepSeg.verbose = verboseDetails;
        // result
        ImageInt result = deepSeg.getSeg();
        result.setScale(input);

        return result;
    }

    private ImageInt expand3D(ImageInt imageInt, int nbZ) {
        ImageInt expand = new ImageShort("expand", imageInt.sizeX, imageInt.sizeY, nbZ);
        IntStream.range(0, nbZ).forEach(z -> expand.insert(imageInt, 0, 0, z, false));

        return expand;
    }
}
