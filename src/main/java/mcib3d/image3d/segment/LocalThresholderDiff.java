package mcib3d.image3d.segment;

import mcib3d.image3d.ImageHandler;

public class LocalThresholderDiff extends LocalThresholder {
    private float diff;

    public LocalThresholderDiff(float diff){
        super();
        this.diff = diff;
    }

    @Override
    public float getLocalThreshold(int xc, int yc, int zc) {
        return Math.max(1, getRawImage().getPixel(xc, yc, zc) - diff);
    }
}
