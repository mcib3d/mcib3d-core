package mcib3d.image3d.segment;

import mcib3d.geom.Voxel3D;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;

import java.util.List;

public abstract class SpotSegmenter {
    private ImageHandler rawImage;
    private ImageHandler labelImage = null;
    private ImageHandler watershedImage = null;
    private boolean floatSegmenter = false;

    public SpotSegmenter() {
    }

    public void setRawImage(ImageHandler rawImage) {
        this.rawImage = rawImage;
    }

    public ImageHandler getRawImage() {
        return rawImage;
    }

    public ImageHandler getLabelImage() {
        return labelImage;
    }

    public ImageHandler getWatershedImage() {
        return watershedImage;
    }

    public void setLabelImage(ImageHandler labelImage) {
        this.labelImage = labelImage;
    }

    public void setWatershedImage(ImageHandler watershedImage) {
        this.watershedImage = watershedImage;
    }

    public boolean isFloatSegmenter() {
        return floatSegmenter;
    }

    public void setFloatSegmenter(boolean floatSegmenter) {
        this.floatSegmenter = floatSegmenter;
    }

    public abstract List<VoxelInt>  segmentSpot(int x, int y, int z, float localThreshold, float value);

    protected void createLabelImage() {
        if (!isFloatSegmenter()) {
            labelImage = new ImageShort("Label", rawImage.sizeX, rawImage.sizeY, rawImage.sizeZ);
        } else {
            labelImage = new ImageFloat("Label", rawImage.sizeX, rawImage.sizeY, rawImage.sizeZ);
        }
    }

    public static VoxelInt getLocalMaximum(ImageHandler image, int x, int y, int z, float radx, float rady, float radz) {
        VoxelInt v = null;

        int sizex = image.sizeX;
        int sizey = image.sizeY;
        int sizez = image.sizeZ;

        double vmax = Double.NEGATIVE_INFINITY;
        double pix;

        double rx2;

        if (radx != 0) {
            rx2 = radx * radx;
        } else {
            rx2 = 1;
        }
        double ry2;
        if (rady != 0) {
            ry2 = rady * rady;
        } else {
            ry2 = 1;
        }
        double rz2;
        if (radz != 0) {
            rz2 = radz * radz;
        } else {
            rz2 = 1;
        }
        double dist;

        int vx = (int) Math.ceil(radx);
        int vy = (int) Math.ceil(rady);
        int vz = (int) Math.ceil(radz);

        for (int k = z - vz; k <= z + vz; k++) {
            for (int j = y - vy; j <= y + vy; j++) {
                for (int i = x - vx; i <= x + vx; i++) {
                    if (i >= 0 && j >= 0 && k >= 0 && i < sizex && j < sizey && k < sizez) {
                        dist = ((x - i) * (x - i)) / rx2 + ((y - j) * (y - j)) / ry2 + ((z - k) * (z - k)) / rz2;
                        if (dist <= 1.0) {
                            pix = image.getPixel(i, j, k);
                            if (pix > vmax) {
                                v = new VoxelInt(i, j, k, (float) pix);
                                vmax = pix;
                            }
                        }
                    }
                }
            }
        }

        return v;
    }
}
