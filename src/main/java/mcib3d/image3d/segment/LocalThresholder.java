package mcib3d.image3d.segment;

import mcib3d.image3d.ImageHandler;

public abstract class LocalThresholder {
    private ImageHandler rawImage;
    private ImageHandler watershedImage = null;

    public LocalThresholder() {
    }

    public void setRawImage(ImageHandler rawImage) {
        this.rawImage = rawImage;
    }

    protected ImageHandler getWatershedImage() {
        return watershedImage;
    }

    protected ImageHandler getRawImage() {
        return rawImage;
    }

    public void setWatershedImage(ImageHandler watershedImage) {
        this.watershedImage = watershedImage;
    }

    public abstract float getLocalThreshold(int xc, int yc, int zc);
}
