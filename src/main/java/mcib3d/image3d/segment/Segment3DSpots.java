/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcib3d.image3d.segment;

import ij.IJ;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Object3DPlane;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.*;
import mcib3d.image3d.distanceMap3d.EDT;
import mcib3d.image3d.regionGrowing.Watershed3D;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author thomas
 */
public class Segment3DSpots {
    ImageHandler rawImage; // raw image
    ImageHandler seedsImage; // positions of seeds
    ImageHandler watershedImage = null; // watershed from seeds
    ImageHandler labelImage = null; // labelled image with objects
    ImageHandler indexImage = null; // indexed image with objects
    float seedsThreshold = -1; // global threshold for seeds
    List<Object3DInt> segmentedObjects = null;
    // options
    public boolean floatLabel = false;
    public boolean useWatershed = true;
    // VOLUME
    private int volMax = Integer.MAX_VALUE;
    private int volMin = 2;
    // METHODS
    LocalThresholder localThresholder;
    SpotSegmenter spotSegmenter;

    /**
     * @param image raw image
     * @param seeds seeds image
     */
    public Segment3DSpots(ImageHandler image, ImageHandler seeds) {
        this.rawImage = image;
        this.seedsImage = seeds;
    }

    public void setLocalThresholder(LocalThresholder localThresholder) {
        this.localThresholder = localThresholder;
    }

    public void setSpotSegmenter(SpotSegmenter spotSegmenter) {
        this.spotSegmenter = spotSegmenter;
    }

    /**
     * @return global threshold
     */
    public float getSeedsThreshold() {
        return seedsThreshold;
    }

    /**
     * @param globalThreshold
     */
    public void setSeedsThreshold(float globalThreshold) {
        this.seedsThreshold = globalThreshold;
    }

    public void setSeedsThreshold(int globalThreshold) {
        this.seedsThreshold = (float)globalThreshold;
    }



    /**
     * @return raw image
     */
    public ImageHandler getRawImage() {
        return rawImage;
    }

    /**
     * @param image
     */
    public void setRawImage(ImageHandler image) {
        this.rawImage = image;
    }

    public ImageHandler getLabelImage() {
        return labelImage;
    }

    public void setLabelImage(ImageHandler labelImage) {
        this.labelImage = labelImage;
    }

    /**
     * @return seeds image
     */
    public ImageHandler getSeeds() {
        return seedsImage;
    }

    /**
     * @param seeds image
     */
    public void setSeeds(ImageInt seeds) {
        this.seedsImage = seeds;
    }

    /**
     * @return
     */
    public List<Object3DInt> getObjects() {
        return segmentedObjects;
    }

    public int getNbObjects(){
        return segmentedObjects.size();
    }

    /**
     * @return
     */
    public ImageHandler getInternalLabelImage() {

        return labelImage;
    }

    public void setBigLabel(boolean bigLabel) {
        this.floatLabel = bigLabel;
    }

    public void setUseWatershed(boolean useWatershed) {
        this.useWatershed = useWatershed;
    }

    public ImageHandler getLabeledImage() {
        IJ.log("Create label image with " + segmentedObjects.size() + " objects");
        if (indexImage == null) {
            if (!floatLabel) {
                indexImage = new ImageShort("Index", rawImage.sizeX, rawImage.sizeY, rawImage.sizeZ);
                indexImage.setScale(rawImage);
            } else {
                indexImage = new ImageFloat("Index", rawImage.sizeX, rawImage.sizeY, rawImage.sizeZ);
                indexImage.setScale(rawImage);
            }
        }
        for (Object3DInt obj : segmentedObjects) {
            obj.drawObject(indexImage);
        }
        return indexImage;
    }

    public void segmentAll() {
        createLabelImage();
        segmentedObjects = new LinkedList<>();
        AtomicReference<Float> value = new AtomicReference<Float>(1.0f);

        if (labelImage == null) {
            this.createLabelImage();
        }
        if (useWatershed && watershedImage == null)
            computeWatershed();
        // set up thresholder and segmenter
        spotSegmenter.setRawImage(rawImage);
        spotSegmenter.setLabelImage(labelImage);
        spotSegmenter.setWatershedImage(watershedImage);
        localThresholder.setRawImage(rawImage);
        localThresholder.setWatershedImage(watershedImage);
        // locate seeds
        for (int z = 0; z < seedsImage.sizeZ; z++) {
            IJ.showStatus("Segmenting slice " + (z + 1));
            for (int y = 0; y < seedsImage.sizeY; y++) {
                for (int x = 0; x < seedsImage.sizeX; x++) {
                    if (seedsImage.getPixel(x, y, z) > seedsThreshold) {
                        segmentLocalSeed(x, y, z, value);
                    }
                }
            }
        }
    }

    public Object3DInt segmentSpot(int x, int y, int z){
        float localThreshold;
        List<VoxelInt> obj;

        // LOCAL THRESHOLD
        localThreshold = localThresholder.getLocalThreshold(x, y, z);
        // TODO use value FIXME
        // SEGMENT LOCAL SPOTS
        float value = (float) (labelImage.getMax()+1);
        obj = spotSegmenter.segmentSpot(x, y, z, localThreshold,value);
        if ((obj != null) && (obj.size() >= volMin) && (obj.size() <= volMax)) {
            Object3DInt object3DInt = new Object3DInt(value);
            object3DInt.addVoxels(obj);
            return object3DInt;
        }
        else {
            return null;
        }
    }

    private void segmentLocalSeed(int x, int y, int z, AtomicReference<Float> value) {
        float localThreshold;
        List<VoxelInt> obj;

        // LOCAL THRESHOLD
        localThreshold = localThresholder.getLocalThreshold(x, y, z);
        // SEGMENT LOCAL SPOTS
        obj = spotSegmenter.segmentSpot(x, y, z, localThreshold, value.get());

        if ((obj != null) && (obj.size() >= volMin) && (obj.size() <= volMax)) {
            Object3DInt object3DInt = new Object3DInt(value.get());
            object3DInt.addVoxels(obj);
            segmentedObjects.add(object3DInt);
            value.getAndAccumulate(1.0f, Float::sum);
        } else if (obj != null) {
            // erase from label image
            for (VoxelInt vo : obj) {
                labelImage.setPixel(vo.getX(), vo.getY(), vo.getZ(), 0);
            }
        }
    }

    /**
     * @return
     */
    public int getVolumeMax() {
        return volMax;
    }

    /**
     * @param volMax
     */
    public void setVolumeMax(int volMax) {
        this.volMax = volMax;
        segmentedObjects = null;
    }

    /**
     * @return
     */
    public int getVolumeMin() {
        return volMin;
    }

    /**
     * @param volMin
     */
    public void setVolumeMin(int volMin) {
        this.volMin = volMin;
        segmentedObjects = null;
    }

    private void computeWatershed() {
        // watershed is used to separate spots, not for segmentation
        // so based on edt of background
        ImageByte seedsTh = seedsImage.thresholdAboveExclusive(seedsThreshold);
        ImageFloat edt = EDT.run(seedsTh, 0, (float) rawImage.getVoxelSizeXY(), (float) rawImage.getVoxelSizeZ(), true, 0);
        ImageShort edt16 = edt.convertToShort(true);
        edt16.invert();
        Watershed3D wat3D = new Watershed3D(edt16, seedsImage, 0, 0);
        watershedImage = wat3D.getWatershedImage3D();
    }

    private void createLabelImage() {
        if (!floatLabel) {
            labelImage = new ImageShort("Label", rawImage.sizeX, rawImage.sizeY, rawImage.sizeZ);
        } else {
            labelImage = new ImageFloat("Label", rawImage.sizeX, rawImage.sizeY, rawImage.sizeZ);
        }
    }
}
