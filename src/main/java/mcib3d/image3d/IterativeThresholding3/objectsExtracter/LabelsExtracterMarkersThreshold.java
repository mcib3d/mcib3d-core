package mcib3d.image3d.IterativeThresholding3.objectsExtracter;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;

public class LabelsExtracterMarkersThreshold implements LabelsExtracter {
    private final int volMin;
    private final int volMax;
    private ImageHandler image;
    private ImageHandler markers;

    public LabelsExtracterMarkersThreshold(int volMin, int volMax) {
        this.volMin = volMin;
        this.volMax = volMax == -1 ? Integer.MAX_VALUE : volMax;
    }

    public LabelsExtracterMarkersThreshold(int volMin, int volMax, ImageHandler handler, ImageHandler markers) {
        this.volMin = volMin;
        this.volMax = volMax == -1 ? Integer.MAX_VALUE : volMax;
        setImage(handler);
        setMarkers(markers);
    }

    @Override
    public ImageHandler extractLabels(float currentLevel) {
        ImageLabeller labeller = new ImageLabeller(volMin, volMax);
        final ImageHandler labels = labeller.getLabels(image.thresholdAboveInclusive(currentLevel));
        Objects3DIntPopulation population = new Objects3DIntPopulation(labels);
        population.getObjects3DInt().stream()
                .filter(obj -> !new Object3DComputation(obj).hasOneVoxelValueAboveStrict(markers, 0))
                .forEach(obj -> obj.drawObjectUsingOffset(labels, 0));

        return labels;
    }

    @Override
    public void setImage(ImageHandler image) {
        this.image = image;
    }

    public void setMarkers(ImageHandler markers) {
        this.markers = markers;
    }
}
