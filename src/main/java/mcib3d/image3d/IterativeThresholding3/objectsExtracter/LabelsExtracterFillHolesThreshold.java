package mcib3d.image3d.IterativeThresholding3.objectsExtracter;

import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;
import mcib3d.image3d.processing.FillHoles2D;

public class LabelsExtracterFillHolesThreshold implements LabelsExtracter {
    private final int volMin;
    private final int volMax;
    private ImageHandler image;
    private ImageHandler markers;

    public LabelsExtracterFillHolesThreshold(int volMin, int volMax) {
        this.volMin = volMin;
        this.volMax = volMax == -1 ? Integer.MAX_VALUE : volMax;
    }

    public LabelsExtracterFillHolesThreshold(int volMin, int volMax, ImageHandler handler, ImageHandler markers) {
        this.volMin = volMin;
        this.volMax = volMax == -1 ? Integer.MAX_VALUE : volMax;
        setImage(handler);
        setMarkers(markers);
    }

    @Override
    public ImageHandler extractLabels(float currentLevel) {
        ImageLabeller labeller = new ImageLabeller(volMin, volMax);
        ImageHandler bin = image.thresholdAboveInclusive(currentLevel);
        FillHoles2D fillHoles2D = new FillHoles2D();
        fillHoles2D.process(bin);
        ImageHandler labels = labeller.getLabels(bin);

        return labels;
    }

    @Override
    public void setImage(ImageHandler image) {
        this.image = image;
    }

    public void setMarkers(ImageHandler markers) {
        this.markers = markers;
    }
}
