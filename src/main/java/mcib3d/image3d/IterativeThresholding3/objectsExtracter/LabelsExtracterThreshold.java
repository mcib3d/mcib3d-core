package mcib3d.image3d.IterativeThresholding3.objectsExtracter;

import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;

public class LabelsExtracterThreshold implements LabelsExtracter {
    private final int volMin;
    private final int volMax;

    private ImageHandler image;

    public LabelsExtracterThreshold(int volMin, int volMax) {
        this.volMin = volMin;
        this.volMax = volMax == -1 ? Integer.MAX_VALUE : volMax;
    }

    public LabelsExtracterThreshold(int volMin, int volMax, ImageHandler handler) {
        this.volMin = volMin;
        this.volMax = volMax == -1 ? Integer.MAX_VALUE : volMax;
        setImage(handler);
    }



    @Override
    public ImageHandler extractLabels(float currentLevel) {
        ImageLabeller labeller = new ImageLabeller(volMin, volMax);
        return labeller.getLabels(image.thresholdAboveInclusive(currentLevel));
    }

    @Override
    public void setImage(ImageHandler image) {
        this.image = image;
    }
}
