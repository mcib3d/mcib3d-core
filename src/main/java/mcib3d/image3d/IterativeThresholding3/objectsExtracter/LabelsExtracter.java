package mcib3d.image3d.IterativeThresholding3.objectsExtracter;

import mcib3d.image3d.ImageHandler;

public interface LabelsExtracter {
    ImageHandler extractLabels(float currentLevel);
    void setImage(ImageHandler image);

}
