package mcib3d.image3d.IterativeThresholding3;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;
import mcib3d.geom2.measurements.MeasureIntensityHist;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.IterativeThresholding3.criteria.Criterion;
import mcib3d.image3d.IterativeThresholding3.levels.Level;
import mcib3d.image3d.IterativeThresholding3.objectsExtracter.LabelsExtracter;
import mcib3d.utils.ArrayUtil;

import java.util.*;

public class TrackThreshold3 {
    private Level level;
    private LabelsExtracter labelsExtracter;
    private Criterion criterion;
    private boolean criteriaMax;

    private final ImageHandler image;
    private int idobj;

    public TrackThreshold3(ImageHandler image) {
        this.image = image;
        idobj = 1;
    }


    public Criterion getCriterion() {
        return criterion;
    }

    public void setCriterion(Criterion criterion) {
        this.criterion = criterion;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public LabelsExtracter getLabelsExtracter() {
        return labelsExtracter;
    }

    public void setLabelsExtracter(LabelsExtracter labelsExtracter) {
        this.labelsExtracter = labelsExtracter;
    }

    public boolean isCriteriaMax() {
        return criteriaMax;
    }

    public void setCriteriaMax(boolean criteriaMax) {
        this.criteriaMax = criteriaMax;
    }

    public ImageHandler[] process() {
        int globalLevelIndex = 0;
        // get levels
        List<Float> levels = level.getLevels(image);
        // first frame
        float level0 = levels.get(globalLevelIndex);
        System.out.println("Processing first " + globalLevelIndex + " " + levels.get(globalLevelIndex));
        ImageHandler labels0 = labelsExtracter.extractLabels(level0);
        Objects3DIntPopulation pop0 = new Objects3DIntPopulation(labels0);
        List<ObjectTrack> objectsTrack0 = computeFrame(image, pop0.getObjects3DInt(), level0);
        List<ObjectTrack> allFrames = new ArrayList<>(objectsTrack0);

        // next levels
        while (globalLevelIndex < levels.size()) {
            globalLevelIndex++;
            if (globalLevelIndex < levels.size()) {
                //System.out.println("Processing " + globalLevelIndex + " " + levels.get(globalLevelIndex));
                float level1 = levels.get(globalLevelIndex);
                ImageHandler labels1 = labelsExtracter.extractLabels(level1);
                Objects3DIntPopulation pop1 = new Objects3DIntPopulation(labels1);
                //System.out.println("Found " + pop1.getNbObjects() + " objects");
                List<ObjectTrack> objectsTrack1 = computeFrame(image, pop1.getObjects3DInt(), level1);
                // association-tracking
                objectsTrack0 = computeAssociation(objectsTrack0, objectsTrack1, allFrames, labels1);
            }
        }

        // retrieve best objects
        List<ObjectTrack> bestObjects = computeResultsBest(allFrames);

        return drawResults(bestObjects);

//        // retrieve all objects
//        List<ObjectTrack> leaves = computeResultLeaves(allFrames);
//        while (!leaves.isEmpty()) {
//            System.out.println("LEAVES " + leaves.size() + " " + allFrames.size());
//            ImageHandler[] draws = drawResults(leaves);
//            draws[0].show();
//            leaves = computeResultLeaves(allFrames);
//        }
    }

    private ImageHandler[] drawResults(List<ObjectTrack> objectTracks) {
        // drawing object, need to call LabelsExtracter
        ImageHandler draw = new ImageShort("Results", image.sizeX, image.sizeY, image.sizeZ);
        ImageHandler drawLevel = new ImageFloat("Levels", image.sizeX, image.sizeY, image.sizeZ);
        int idDraw = 1;
        int idObj = 0;
        // draw first object
        float level0 = objectTracks.get(idObj).getLevel();
        ImageHandler labels = labelsExtracter.extractLabels(level0);
        Objects3DIntPopulation population = new Objects3DIntPopulation(labels);

        while (idObj < objectTracks.size()) {
            while (objectTracks.get(idObj).getLevel() == level0) {
                Object3DInt object3DInt = population.getObjectByLabel(objectTracks.get(idObj).label);
                object3DInt.drawObject(draw, idDraw++);
                object3DInt.drawObject(drawLevel, level0);
                idObj++;
                if (idObj >= objectTracks.size()) break;
            }
            if (idObj >= objectTracks.size()) break;
            level0 = objectTracks.get(idObj).getLevel();
            labels = labelsExtracter.extractLabels(level0);
            population = new Objects3DIntPopulation(labels);
        }

        return new ImageHandler[]{draw, drawLevel};
    }

    private List<ObjectTrack> computeFrame(ImageHandler img, List<Object3DInt> objects, float threshold) {
        List<ObjectTrack> frame1 = new ArrayList<>();
        for (Object3DInt ob : objects) {
            ObjectTrack obt = new ObjectTrack();
            obt.setObject3D(ob);
            obt.setFrame(threshold);
            // add measurements
            obt.computeCriterion(criterion);
            obt.volume = ob.size();
            obt.seed = ob.getFirstVoxel();
            obt.level = threshold;
            obt.rawImage = img;
            obt.label = ob.getLabel();
            obt.id = idobj++;
            frame1.add(obt);

        }
        return frame1;
    }

    private List<ObjectTrack> computeAssociation(List<ObjectTrack> frame1, List<ObjectTrack> frame2, List<ObjectTrack> allFrames, ImageHandler labels2) {
        Map<Float, ObjectTrack> hashObjectsT2 = new HashMap<>();
        for (ObjectTrack objectTrack : frame2) {
            hashObjectsT2.put(objectTrack.getObject3D().getLabel(), objectTrack);
        }

        // create association
        List<ObjectTrack> newListTrack = new ArrayList<>();

        for (ObjectTrack obt : frame1) {
            //System.out.println("testing asso " + obt + " " + obt.getObject3D());
            if (obt.getObject3D() == null) continue; // FIXME
            List<Float> uniques = new MeasureIntensityHist(obt.getObject3D(), labels2).listValuesUnique();
            // no association
            if (!uniques.isEmpty()) {
                // association 1-->1
                if (uniques.size() == 1) {
                    ObjectTrack child = hashObjectsT2.get(uniques.get(0));
                    if ((child != null) && (obt.volume >= child.volume)) { // was >
                        //IJ.log("adding one child " + obt + " " + child);
                        obt.addChild(child);
                        child.setParent(obt);
                        allFrames.add(child);
                        // test
                        newListTrack.add(child);
                        frame2.remove(child);
                        obt.setObject3D(null);
                    }
                }
                // association 1-->n
                else {
                    uniques.forEach(val -> {
                        ObjectTrack child = hashObjectsT2.get(val);
                        if ((child != null)) {
                            //IJ.log("adding two child " + obt + " " + child);
                            obt.addChild(child);
                            child.setParent(obt);
                            newListTrack.add(child);
                            allFrames.add(child);
                            frame2.remove(child);
                            obt.setObject3D(null);
                        }
                    });
                }
            }
        }
        // add all new objects from T2
        allFrames.addAll(frame2);
        newListTrack.addAll(frame2);

        return newListTrack;
    }

    private List<ObjectTrack> computeResultsBest(List<ObjectTrack> allFrames) {
        allFrames.sort(new ComparatorObjectTrack(!criteriaMax));
        List<ObjectTrack> bestObjects = new LinkedList<>();
        while (!allFrames.isEmpty()) {
            ObjectTrack objectTrack = allFrames.get(0);
            allFrames.remove(0);
            while ((!allFrames.isEmpty()) && (!objectTrack.isValid())) {
                objectTrack = allFrames.get(0);
                allFrames.remove(0);
            }
            if (objectTrack.isValid()) {
                //IJ.log("testing " + objectTrack + " " + objectTrack.getParent() + " " + objectTrack.getNbChildren());
                bestObjects.add(objectTrack);
                // remove up
                ObjectTrack root = objectTrack.getRoot();
                List<ObjectTrack> list = objectTrack.getLineageTo(root);
                for (ObjectTrack objectInvalid : list) {
                    if (objectInvalid.isValid()) {
                        objectInvalid.VALID = false;
                        //IJ.log("disabling up " + objectInvalid + " " + objectTrack);
                    }
                    allFrames.remove(objectInvalid);
                }
                // remove down
                list = objectTrack.getAllDescendantsToEnd();
                for (ObjectTrack objectInvalid : list) {
                    if (objectInvalid.isValid()) {
                        //IJ.log("disabling down " + objectInvalid + " " + objectTrack);
                        objectInvalid.VALID = false;
                    }
                    allFrames.remove(objectInvalid);
                }
            }
        }

        return bestObjects;
    }

    private List<ObjectTrack> computeResultLeaves(List<ObjectTrack> allFrames) {
        List<ObjectTrack> toKeep = new ArrayList<>();
        List<ObjectTrack> toRemove = new ArrayList<>();

        for (ObjectTrack obt : allFrames) {
            if (obt.getState() == ObjectTrack.STATE_END) {
                ObjectTrack anc = obt.getFirstMove();
                if (anc == null) {
                    anc = obt;
                }
                List<ObjectTrack> lineage = obt.getLineageTo(anc);
                ObjectTrack bestObject = computeBestObject(lineage);
                // object to keep
                toKeep.add(bestObject);
                // change state
                toRemove.addAll(lineage);
            }
        }
        // process nodes to be removed
        allFrames.removeAll(toRemove);
        for (ObjectTrack obt : allFrames) {
            ObjectTrack par = obt.getParent();
            if (par != null) {
                par.removeChild(obt);
            }
        }

        return toKeep;
    }

    private ObjectTrack computeBestObject(List<ObjectTrack> list) {
        // get all values
        ArrayUtil valueCriterion = new ArrayUtil(list.size());
        for (int i = 0; i < valueCriterion.size(); i++) {
            valueCriterion.putValue(i, list.get(i).valueCriteria);
        }
        int idx = criteriaMax ? valueCriterion.getMaximumIndex() : valueCriterion.getMinimumIndex();

        return list.get(idx);
    }
}
