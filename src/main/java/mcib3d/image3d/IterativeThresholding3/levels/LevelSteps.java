package mcib3d.image3d.IterativeThresholding3.levels;

import mcib3d.image3d.ImageHandler;

import java.util.ArrayList;
import java.util.List;

public class LevelSteps implements Level {
    private final float minLevel;
    private final float maxLevel;
    private final float step;

    public LevelSteps(float minLevel, float maxLevel, float step) {
        this.minLevel = minLevel;
        this.maxLevel = maxLevel;
        this.step = step;
    }

    @Override
    public List<Float> getLevels(ImageHandler handler) {
        List<Float> floats = new ArrayList<>();

        if(step>0) {
            for (float f = minLevel; f < maxLevel; f += step)
                floats.add(f);
            floats.add(maxLevel);
        }
        else {
            for (float f = maxLevel; f > minLevel; f -= step)
                floats.add(f);
            floats.add(minLevel);
        }

        return floats;
    }
}
