package mcib3d.image3d.IterativeThresholding3.levels;

import mcib3d.image3d.ImageHandler;

import java.util.List;

public interface Level {
    public List<Float> getLevels(ImageHandler handler);
}
