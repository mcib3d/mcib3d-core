package mcib3d.image3d.IterativeThresholding3;

import java.util.Comparator;

public class ComparatorObjectTrack implements Comparator<ObjectTrack> {
    private final boolean ascending;

    @Override
    public int compare(ObjectTrack o1, ObjectTrack o2) {
        int compare = 0;
        if (o1.valueCriteria < o2.valueCriteria) compare = -1;
        else if (o1.valueCriteria > o2.valueCriteria) compare = +1;
        else {
            if (o1.volume < o2.volume) compare = -1;
            else if (o1.volume > o2.volume) compare = +1;
            else {
                compare = Integer.compare(o1.id, o2.id);
            }
        }
        if (ascending) return compare;
        else return -compare;
    }

    public ComparatorObjectTrack(boolean ascending) {
        this.ascending = ascending;
    }
}
