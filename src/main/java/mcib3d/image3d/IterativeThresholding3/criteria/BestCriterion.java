package mcib3d.image3d.IterativeThresholding3.criteria;

import mcib3d.utils.ArrayUtil;

/**
 * Created by thomasb on 20/4/16.
 */
public interface BestCriterion {
    int computeBestCriterion(ArrayUtil list);
}
