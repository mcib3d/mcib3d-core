package mcib3d.image3d.IterativeThresholding3.criteria;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.measurements.MeasureEllipsoid;

/**
 * Created by thomasb on 20/4/16.
 */
public class CriterionElongation implements Criterion {
    @Override
    public double computeCriterion(Object3DInt object3D) {
        return new MeasureEllipsoid(object3D).getValueMeasurement(MeasureEllipsoid.ELL_ELONGATION);
    }
}
