/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mcib3d.image3d.IterativeThresholding2;

import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.VoxelInt;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.IterativeThresholding2.criteria.Criterion;

import java.util.ArrayList;
import java.util.List;

public class ObjectTrack {
    public static int STATE_UNKNOWN = 0;
    public static int STATE_DIE = 1;
    public static int STATE_DIVIDE = 2;
    public static int STATE_MOVE = 3;
    // measurements
    public double valueCriteria;
    public double volume;
    // first point for segment + threshold
    public VoxelInt seed;
    public float threshold;
    public ImageHandler rawImage;
    public boolean VALID = true;
    public int id;
    private Object3DInt object = null;
    //private double time = 0;
    private float frame = 0;
    private List<ObjectTrack> children = null;
    private ObjectTrack parent = null;
    private int state = STATE_UNKNOWN;

    public ObjectTrack() {
    }

    public ObjectTrack getParent() {
        return parent;
    }

    public void setParent(ObjectTrack parent) {
        this.parent = parent;
        state = STATE_UNKNOWN;
    }

    public float getFrame() {
        return frame;
    }

    public void setFrame(float frame) {
        this.frame = frame;
    }

    public int getState() {
        if (state == STATE_UNKNOWN) {
            computeState();
        }
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public boolean isNew() {
        return parent == null;
    }

    public boolean isRoot() {
        return parent == null;
    }

    public boolean isEnd() {
        return (getNbChildren() == 0);
    }


    public int getNbChildren() {
        if (children == null) {
            return 0;
        } else {
            return children.size();
        }
    }

    public List<ObjectTrack> getChildren() {
        return children;
    }

    public void addChild(ObjectTrack child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
        state = STATE_UNKNOWN;
    }

    public void removeAllChildren() {
        children = null;
        state = STATE_UNKNOWN;
    }

    public void removeChild(ObjectTrack child) {
        children.remove(child);
        if (children.isEmpty()) {
            state = STATE_UNKNOWN;
        }
    }

    private void computeState() {
        if (children == null) {
            state = STATE_DIE;
        } else {
            if (children.size() == 1) {
                state = STATE_MOVE;
            } else if (children.size() > 1) {
                state = STATE_DIVIDE;
            } else { // 0 children
                state = STATE_DIE;
            }
        }
    }

    public boolean hasBrothers() {
        if (parent == null) {
            return false;
        }
        return parent.getNbChildren() > 1;
    }

    public ObjectTrack getAncestor() {
        ObjectTrack par = this;
        while (!par.hasBrothers()) {
            if (par.getParent() == null) {
                return par;
            } else {
                par = par.getParent();
            }
        }
        return par;
    }

    public ObjectTrack getRoot() {
        ObjectTrack par = this;
        while (par.getParent() != null) {
            par = par.getParent();
        }

        return par;
    }


    public List<ObjectTrack> getLineageTo(ObjectTrack anc) {
        List<ObjectTrack> list = new ArrayList<>();
        ObjectTrack par = this;
        list.add(par);
        while (!par.equals(anc)) {
            if (par.getParent() == null) {
                return null;
            } else {
                par = par.getParent();
                list.add(par);
            }
        }

        return list;
    }

    public List<ObjectTrack> getAllDescendantsToEnd() {
        List<ObjectTrack> list = new ArrayList<>();
        ObjectTrack par = this;
        list.add(par);
        if (par.getNbChildren() == 0) return list;
        for (ObjectTrack child : children) {
            list.addAll(child.getAllDescendantsToEnd());
        }
        return list;
    }

    public boolean isValid() {
        return VALID;
    }

    public Object3DInt getObject3D() {
        return object;
    }

    public void setObject3D(Object3DInt object3) {
        this.object = object3;
    }

    public void computeCriterion(Criterion criterion) {
        valueCriteria = criterion.computeCriterion(getObject3D());
    }

    @Override
    public String toString() {
        return id + "(" + seed+") "+threshold;
    }
}
