package mcib3d.image3d.IterativeThresholding2.criteria;

import mcib3d.geom2.Object3DInt;

public interface Criterion {
    double computeCriterion(Object3DInt object3D);
}
