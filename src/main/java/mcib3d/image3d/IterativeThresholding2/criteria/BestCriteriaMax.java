package mcib3d.image3d.IterativeThresholding2.criteria;

import mcib3d.utils.ArrayUtil;

/**
 * Created by thomasb on 20/4/16.
 */
public class BestCriteriaMax implements BestCriterion {
    @Override
    public int computeBestCriterion(ArrayUtil list) {
        return list.getMaximumIndex();
    }
}
