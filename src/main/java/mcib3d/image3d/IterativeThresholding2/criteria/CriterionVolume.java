package mcib3d.image3d.IterativeThresholding2.criteria;

import mcib3d.geom.Object3D;
import mcib3d.geom2.Object3DInt;

/**
 * Created by thomasb on 20/4/16.
 */
public class CriterionVolume implements Criterion {

    @Override
    public double computeCriterion(Object3DInt object3D) {
        return object3D.size();
    }
}
