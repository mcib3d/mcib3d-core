package mcib3d.image3d.IterativeThresholding2.criteria;

import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.measurements.MeasureIntensity;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.processing.CannyEdge3D;

/**
 * Created by thomasb on 20/4/16.
 */
public class CriterionEdge implements Criterion {
    ImageHandler edges;

    public CriterionEdge(ImageHandler imageHandler, double alpha) {
        // compute edge image
        CannyEdge3D cannyEdge3D = new CannyEdge3D(imageHandler, alpha);
        edges = cannyEdge3D.getEdge();

    }

    @Override
    public double computeCriterion(Object3DInt object3D) {
        Object3DInt edgeObject = new Object3DComputation(object3D).getObjectEdgeMorpho(2,2,2,true);
        return new MeasureIntensity(edgeObject, edges).getValueMeasurement(MeasureIntensity.INTENSITY_AVG);
    }
}
