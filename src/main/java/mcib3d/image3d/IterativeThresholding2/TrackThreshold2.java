package mcib3d.image3d.IterativeThresholding2;

import ij.IJ;
import ij.ImagePlus;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.VoxelInt;
import mcib3d.geom2.measurements.MeasureIntensityHist;
import mcib3d.image3d.ImageFloat;
import mcib3d.image3d.ImageHandler;
import mcib3d.image3d.ImageLabeller;
import mcib3d.image3d.ImageShort;
import mcib3d.image3d.IterativeThresholding2.criteria.*;
import mcib3d.image3d.segment.SpotSegmenter;
import mcib3d.image3d.segment.SpotSegmenterClassical;
import mcib3d.utils.ArrayUtil;
import mcib3d.utils.Chrono;
import mcib3d.utils.Logger.AbstractLog;
import mcib3d.utils.Logger.IJLog;

import java.util.*;

public class TrackThreshold2 {

    public static final byte THRESHOLD_METHOD_STEP = 1;
    public static final byte THRESHOLD_METHOD_VOLUME = 3;
    // measure to use
    public static final byte CRITERIA_METHOD_MIN_ELONGATION = 1;
    public static final byte CRITERIA_METHOD_MAX_VOLUME = 2;
    public static final byte CRITERIA_METHOD_MSER = 3; // min variation of volume
    public static final byte CRITERIA_METHOD_MAX_COMPACTNESS = 4;
    public static final byte CRITERIA_METHOD_MAX_EDGES = 5;
    @Deprecated
    public boolean verbose = true;
    @Deprecated
    public boolean status = true;
    AbstractLog log = new IJLog();
    // use tree set
    TreeSet<ObjectTrack> treeSet;
    int idobj = 1;
    // volume range in voxels
    private final double volMax;
    private final double volMin;
    private float contrastMin = 100;
    private int threshold_method = THRESHOLD_METHOD_STEP;
    private final float step;
    private final int nbClasses;
    private final float startThreshold;
    private int stopThreshold = Integer.MAX_VALUE;
    private int criteria_method = CRITERIA_METHOD_MIN_ELONGATION;
    private int Globalindex;
    private boolean use32bits = false;
    private BestCriterion bestCriterion;

    public TrackThreshold2(double vmin, double vmax, float step, int nbClass, float start) {
        if (vmax >= vmin) {
            volMax = vmax;
            volMin = vmin;
        } else {
            volMax = vmin;
            volMin = vmax;
        }
        this.step = step;
        nbClasses = nbClass;
        startThreshold = start;

        ComparatorObjectTrack comp = new ComparatorObjectTrack();
        treeSet = new TreeSet(comp);
    }

    public TrackThreshold2(double vmin, double vmax, float contrast, int st, int nbCla, float sta) {
        if (vmax >= vmin) {
            volMax = vmax;
            volMin = vmin;
        } else {
            volMax = vmin;
            volMin = vmax;
        }
        step = st;
        nbClasses = nbCla;
        startThreshold = sta;
        contrastMin = contrast;

        ComparatorObjectTrack comp = new ComparatorObjectTrack();
        treeSet = new TreeSet(comp);
    }

    public void setUse32bits(boolean use32bits) {
        this.use32bits = use32bits;
    }

    private List<Float> initConstantVoxelsHistogram(ImageHandler img, int nbLevels, float startThreshold, Map<Float, Integer> histogram) {
        List<Float> levels = new ArrayList<>();
        float max = Math.min(stopThreshold, (float) img.getMax());
        float level = startThreshold;
        Float[] values = histogram.keySet().toArray(new Float[0]);

        int VolPerLevel = Math.round(img.sizeXYZ / nbLevels);

        int idx = 0;
        int sum = 0;

        while ((level < max) && (idx < values.length)) {
            while (sum < VolPerLevel) {
                if (idx >= values.length) {
                    break;
                }
                sum += histogram.get(values[idx]);
                idx++;
            }
            if (idx < values.length) {
                level = values[idx];
                levels.add(level);
                sum = 0;
            }
        }

        return levels;
    }

    public void setStopThreshold(int stopThreshold) {
        this.stopThreshold = stopThreshold;
    }

    public void setMethodThreshold(int meth) {
        threshold_method = meth;
    }

    public void setCriteriaMethod(int criteria_method) {
        this.criteria_method = criteria_method;
    }


    private List<Float> initHistogram(ImageHandler img) {
        Map<Float, Integer> histogram = new HistogramImage(img).getHistogramFloat();
        if (threshold_method == THRESHOLD_METHOD_STEP) {
            return initHistogramStep(step, img, histogram);
        } else if (threshold_method == THRESHOLD_METHOD_VOLUME)
            return initConstantVoxelsHistogram(img, nbClasses, startThreshold, histogram);

        return null;
    }

    private List<Float> initHistogramStep(float step, ImageHandler img, Map<Float, Integer> histogram) {
        List<Float> levels = new ArrayList<>();
        float max = Math.min(stopThreshold, (float) img.getMax());
        float level = startThreshold;
        Float[] values = histogram.keySet().toArray(new Float[0]);
        // first level
        int idx = 0;
        while (level < max) {
            while (values[idx] < level) idx++;
            level = values[idx];
            levels.add(level);
            level += step;
        }
        // last level

        return levels;
    }

    private List<ObjectTrack> computeAssociation(List<ObjectTrack> frame1, List<ObjectTrack> frame2, List<ObjectTrack> allFrames, ImageHandler labels2) {
        Map<Float, ObjectTrack> hashObjectsT2 = new HashMap<>();
        for (ObjectTrack objectTrack : frame2) {
            hashObjectsT2.put(objectTrack.getObject3D().getLabel(), objectTrack);
        }

        // create association
        List<ObjectTrack> newListTrack = new ArrayList<>();

        for (ObjectTrack obt : frame1) {
            //IJ.log("testing asso " + obt + " " + obt.getObject3D());
            List<Float> uniques = new MeasureIntensityHist(obt.getObject3D(), labels2).listValuesUnique();
            //ArrayUtil list = obt.getObject3D().listValues(labels2, 0);
            // no association
            if (!uniques.isEmpty()) {
                //list = list.distinctValues();
                // association 1<->1
                if (uniques.size() == 1) {
                    ObjectTrack child = hashObjectsT2.get(uniques.get(0));
                    if ((child != null) && (obt.volume > child.volume)) { // was >
                        //IJ.log("adding one child " + obt + " " + child);
                        obt.addChild(child);
                        child.setParent(obt);
                        allFrames.add(child);
                        // test
                        newListTrack.add(child);
                        frame2.remove(child);
                        obt.setObject3D(null);
                    }

                    if ((child != null) && (obt.volume == child.volume)) {
                        //IJ.log("adding same volume " + obt + " " + child);
                        newListTrack.add(obt);
                        frame2.remove(child);
                        treeSet.remove(child);

                    }
                }
                // association 1<->n
                else {
                    uniques.forEach(val -> {
                        ObjectTrack child = hashObjectsT2.get(val);
                        if ((child != null)) {
                            //IJ.log("adding two child " + obt + " " + child);
                            obt.addChild(child);
                            child.setParent(obt);
                            newListTrack.add(child);
                            allFrames.add(child);
                            frame2.remove(child);
                            obt.setObject3D(null);
                        }
                    });
                }
            }
        }
        // add all new objects from T2
        allFrames.addAll(frame2);
        newListTrack.addAll(frame2);

        return newListTrack;
    }

    private List<ObjectTrack> computeFrame(ImageHandler img, List<Object3DInt> objects, float threshold, Criterion criterion) {
        List<ObjectTrack> frame1 = new ArrayList<>();
        for (Object3DInt ob : objects) {
            // if (checkMarkers(ob)) {
            ObjectTrack obt = new ObjectTrack();
            obt.setObject3D(ob);
            obt.setFrame(threshold);
            // add measurements
            obt.computeCriterion(criterion);
            obt.volume = ob.size();
            obt.seed = ob.getFirstVoxel();
            obt.threshold = threshold;
            obt.rawImage = img;
            obt.id = idobj++;
            frame1.add(obt);
            treeSet.add(obt);

        }
        return frame1;
    }

    private List<ObjectTrack> process(ImageHandler img) {
        // test weka for Jaza dataset
        //criteria_method=CRITERIA_METHOD_MAX_CLASSIFICATION;

        Criterion criterion;
        switch (criteria_method) {
            case CRITERIA_METHOD_MIN_ELONGATION:
                criterion = new CriterionElongation();
                bestCriterion = new BestCriteriaMin();
                break;
            case CRITERIA_METHOD_MAX_COMPACTNESS:
                criterion = new CriterionCompactness();
                bestCriterion = new BestCriteriaMax();
                break;
            case CRITERIA_METHOD_MAX_VOLUME:
                criterion = new CriterionVolume();
                bestCriterion = new BestCriteriaMax();
                break;
            case CRITERIA_METHOD_MSER:
                criterion = new CriterionVolume();
                bestCriterion = new BestCriteriaStable();
                break;
            case CRITERIA_METHOD_MAX_EDGES:
                criterion = new CriterionEdge(img, 0.5);
                bestCriterion = new BestCriteriaMax();
                break;
//            case CRITERIA_METHOD_MAX_CLASSIFICATION:
//                criterion = new CriterionClassification("/home/thomasb/App/ImageJ/testWEKA.arff"); // test
//                bestCriterion = new BestCriteriaMax();
//                break;
            default: // MSER
                criterion = new CriterionVolume();
                bestCriterion = new BestCriteriaStable();
                break;
        }

        float T0, TMaximum, T1;
        List<Float> histogramThreshold;
        ImageLabeller labeler = new ImageLabeller(volMin, volMax);
        TMaximum = (int) img.getMax();

        if (log != null) log.log("Analysing histogram ...");
        histogramThreshold = initHistogram(img);
        if (histogramThreshold.size() == 0) return null;
        T0 = histogramThreshold.get(0);

        // first frame
        T1 = T0;
        if (log != null) log.log("Computing frame for first threshold " + T1);
        List<ObjectTrack> frame1 = computeFrame(img, labeler.getObjects3D(img.thresholdAboveInclusive(T1)), T1, criterion);

        if (log != null) log.log("Starting iterative thresholding ... ");

        List<ObjectTrack> allFrames = new ArrayList<>(frame1);
        // use histogram and unique values to loop over pixel values
        Globalindex = 0;
        //
        Chrono chrono = new Chrono(TMaximum);
        chrono.start();
        String S; // go update mode
        if (log instanceof IJLog) {
            ((IJLog) (log)).setUpdate(true);// was true
        }

        /// LOOP
        while (T1 <= TMaximum) {
            float T2 = computeNextThreshold(T1, TMaximum, histogramThreshold);
            //IJ.log("" + T1 + " " + T2 + " " + TMaximum);
            if (T2 < 0) break;

            if (log != null) {
                S = chrono.getFullInfo(T2 - T1);
                if ((S != null)) log.log(" task " + S+" Histogram "+T1+ " / "+TMaximum);

            }
            //if (status) IJ.showStatus("Computing frame for threshold " + T2 + "                   ");

            // Threshold T2
            ImageHandler bin2 = img.thresholdAboveInclusive(T2);
            ImageHandler labels2 = labeler.getLabels(bin2);
            List<ObjectTrack> frame2 = computeFrame(img, labeler.getObjects3D(bin2), T2, criterion);

            System.gc();
            // T2--> new T1
            T1 = T2;
            frame1 = computeAssociation(frame1, frame2, allFrames, labels2);

        } // thresholding
        if (log instanceof IJLog) { // stop update mode
            ((IJLog) (log)).setUpdate(false);
        }
        if (log != null) {
            log.log("Iterative Thresholding finished");
        }

        return allFrames;

        //return computeResults(allFrames, img, bestCriterion);
    }

    private List<ImageHandler> computeResults(List<ObjectTrack> allFrames, ImageHandler img) {
        //printTree();
        // get results from the tree with different levels of objects
        int level = 1;
        int maxLevel = 10;
        List<ImageHandler> drawsReconstruct = new ArrayList<ImageHandler>();

        // delete low contrast

        while (deleteLowContrastTracks(allFrames, contrastMin)) ;

        while ((!allFrames.isEmpty()) && (level < maxLevel)) {

            ImageHandler drawIdx;
            // 32-bits case
            //if (allFrames.size() < 65535) {
            if (!use32bits) {
                drawIdx = new ImageShort("Objects", img.sizeX, img.sizeY, img.sizeZ);
                //drawContrast = new ImageShort("Contrast", img.sizeX, img.sizeY, img.sizeZ);
            } else {
                drawIdx = new ImageFloat("Objects", img.sizeX, img.sizeY, img.sizeZ);
                //drawContrast = new ImageFloat("Contrast", img.sizeX, img.sizeY, img.sizeZ);
            }
            drawsReconstruct.add(drawIdx);
            //drawIdx.show("level " + level);
            //drawsReconstruct.add(drawContrast);
            int idx = 1;
            List<ObjectTrack> toBeRemoved = new ArrayList<>();
            for (ObjectTrack obt : allFrames) {
                //IJ.log("testing " + obt + " " + obt.getState());
                if (obt.getState() ==  ObjectTrack.STATE_DIE) {
                    ObjectTrack anc = obt.getAncestor();
                    if (anc == null) {
                        anc = obt;
                    }
                    List<ObjectTrack> list = obt.getLineageTo(anc);
                    // compute contrast, max threshold - min threshold

                    ObjectTrack bestObject = computeBestObject(list, bestCriterion);

                    // segment spot
                    VoxelInt seed = anc.seed;
                    float threshold = anc.threshold;
                    ObjectTrack objectSegment = anc;
                    if (bestObject != null) {
                        objectSegment = bestObject;
                        seed = objectSegment.seed;
                        threshold = objectSegment.threshold;
                    }
                    SpotSegmenter SegmentSpot = new SpotSegmenterClassical();
                    SegmentSpot.setRawImage(objectSegment.rawImage);
                    final float val = idx;
                    List<VoxelInt> spot = SegmentSpot.segmentSpot(seed.getX(), seed.getY(), seed.getZ(), threshold, idx);
                    spot.forEach(V -> drawIdx.setPixel(V, val));
                    //object3D.draw(drawContrast, contrast);
                    // set to remove all objects in list
                    toBeRemoved.addAll(list);
                    idx++;
                }
            }
            if (log != null) {
                log.log("Nb total objects level " + level + " : " + (idx - 1));
            }
            level++;
            // really remove all objects set to be removed
            if (toBeRemoved.isEmpty()) {
                break;
            }
            for (ObjectTrack obt : toBeRemoved) {
                ObjectTrack par = obt.getParent();
                if (par != null) {
                    par.removeChild(obt);
                }
            }
            allFrames.removeAll(toBeRemoved);
        }

        allFrames = null;
        System.gc();

        return drawsReconstruct;
    }

    private void printTree() {
        IJ.log("TREE");
        for (ObjectTrack root : treeSet) {
            IJ.log(root + " " + root.getObject3D() + " " + root.getParent() + " " + root.getNbChildren());
            if (root.getNbChildren() > 0) {
                for (ObjectTrack child : root.getChildren()) {
                    IJ.log("--->" + child + " " + child.getObject3D() + " " + child.getParent() + " " + child.getNbChildren());
                }
            }
        }

    }

    private ImageHandler computeResultsBest(List<ObjectTrack> allFrames, ImageHandler img) {
        // TEST
        //printTree();
        // TEST

        int idx = 1;
        ImageHandler drawsReconstruct;
        //if (allFrames.size() < 65535) {
        if (!use32bits) {
            drawsReconstruct = new ImageShort("Objects", img.sizeX, img.sizeY, img.sizeZ);
            //drawContrast = new ImageShort("Contrast", img.sizeX, img.sizeY, img.sizeZ);
        } else {
            drawsReconstruct = new ImageFloat("Objects", img.sizeX, img.sizeY, img.sizeZ);
            //drawContrast = new ImageFloat("Contrast", img.sizeX, img.sizeY, img.sizeZ);
        }
        // delete low contrast
        while (deleteLowContrastTracks(allFrames, contrastMin)) ;

        while (!treeSet.isEmpty()) {
            ObjectTrack objectTrack = treeSet.pollFirst();
            while ((!treeSet.isEmpty()) && (!objectTrack.isValid())) objectTrack = treeSet.pollFirst();
            if (objectTrack.isValid()) {
                //IJ.log("testing " + objectTrack + " " + objectTrack.getParent() + " " + objectTrack.getNbChildren());
                SpotSegmenter SegmentSpot = new SpotSegmenterClassical();
                SegmentSpot.setRawImage(objectTrack.rawImage);
                VoxelInt seed = objectTrack.seed;
                float threshold = objectTrack.threshold;
                // remove up
                ObjectTrack root = objectTrack.getRoot();
                List<ObjectTrack> list = objectTrack.getLineageTo(root);
                for (ObjectTrack objectInvalid : list) {
                    if (objectInvalid.isValid()) {
                        objectInvalid.VALID = false;
                        //IJ.log("disabling up " + objectInvalid + " " + objectTrack);
                    }
                    treeSet.remove(objectInvalid);
                }
                // remove down
                list = objectTrack.getAllDescendantsToEnd();
                for (ObjectTrack objectInvalid : list) {
                    if (objectInvalid.isValid()) {
                        //IJ.log("disabling down " + objectInvalid + " " + objectTrack);
                        objectInvalid.VALID = false;
                    }
                    treeSet.remove(objectInvalid);
                }
                final int val = idx;
                List<VoxelInt> spot = SegmentSpot.segmentSpot(seed.getX(), seed.getY(), seed.getZ(), threshold, idx);
                spot.forEach(V -> drawsReconstruct.setPixel(V, val));
                //IJ.log("New object 3D " + object3D);
                idx++;
            }
        }

        return drawsReconstruct;
    }

    private ObjectTrack computeBestObject(List<ObjectTrack> list, BestCriterion bestCriterion) {
        // test maximal volume //or one with minimal elongation
        // get all values
        ArrayUtil valueCriterion = new ArrayUtil(list.size());
        for (int i = 0; i < valueCriterion.size(); i++) {
            valueCriterion.putValue(i, list.get(i).valueCriteria);
        }

        return list.get(bestCriterion.computeBestCriterion(valueCriterion));
    }

    private float computeNextThreshold(float T1, float Tmax, List<Float> histogram) {
        float T2 = T1;
        if (T2 < Tmax) {
            Globalindex++;
            if (Globalindex < histogram.size())
                T2 = histogram.get(Globalindex);
            else T2 = Tmax + 1;
        } else if (T2 == Tmax) {
            // use extra threshold to finalize all objects without any child
            T2 = Tmax + 1;
        }

        return T2;
    }

    private boolean deleteLowContrastTracks(List<ObjectTrack> allFrames, float contrastMin) {
        boolean change = false;
        if (contrastMin <= 0) return false;
        List<ObjectTrack> toBeRemoved = new ArrayList<>();
        for (ObjectTrack objectTrack : allFrames) {
            if (objectTrack.getState() == mcib3d.image3d.IterativeThresholding.ObjectTrack.STATE_DIE) {
                ObjectTrack anc = objectTrack.getAncestor();
                if (anc == null) {
                    anc = objectTrack;
                }
                // compute contrast, max threshold - min threshold
                float contrast = 0;
                if (anc != null)
                    contrast = objectTrack.getFrame() - anc.getFrame() + 1;
                if (contrast < contrastMin) {
                    toBeRemoved.addAll(objectTrack.getLineageTo(anc));
                    change = true;
                }
            }
        }
        allFrames.removeAll(toBeRemoved);

        return change;
    }

    public ImageHandler segment(ImageHandler input, boolean verbose) {
        setVerbose(verbose);
        List<ObjectTrack> frames = process(input);
        if (frames == null) return null;
        List<ImageHandler> res = computeResults(frames, input);
        if (!res.isEmpty()) {
            return res.get(0);
        } else {
            return null;
        }
    }

    public ImageHandler segmentBest(ImageHandler input, boolean verbose) {
        setVerbose(verbose);
        List<ObjectTrack> frames = process(input);
        if (frames == null) return null;
        List<ImageHandler> res = computeResults(frames, input);
        if (!res.isEmpty()) {
            return res.get(0);
        } else {
            return null;
        }
    }

    public List<ImageHandler> segmentAll(ImageHandler input, boolean verbose) {
        setVerbose(verbose);
        List<ObjectTrack> frames = process(input);
        if (frames == null) return null;
        return computeResults(frames, input);
    }

    public ImagePlus segment(ImagePlus input, boolean show) {
        setVerbose(show);
        ImageHandler img = ImageHandler.wrap(input);
        List<ObjectTrack> frames = process(img);
        if (frames == null) return null;
        List<ImageHandler> drawsReconstruct = computeResults(frames, img);

        // no results
        if (drawsReconstruct.size() == 0) return null;
        // drawing results
        ImageHandler[] drawsTab = new ImageHandler[drawsReconstruct.size()];
        for (int i = 0; i < drawsTab.length; i++) {
            drawsTab[i] = drawsReconstruct.get(i);
        }
        return ImageHandler.getHyperStack("draw", drawsTab);
    }

    public ImagePlus segmentBest(ImagePlus input, boolean show) {
        setVerbose(show);
        ImageHandler img = ImageHandler.wrap(input);
        List<ObjectTrack> frames = process(img);
        if (frames == null) return null;
        ImageHandler drawsReconstruct = computeResultsBest(frames, img);
        return drawsReconstruct.getImagePlus();
    }


    @Deprecated
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }

    public void setLog(AbstractLog abstractLog) {
        log = abstractLog;
    }

}
