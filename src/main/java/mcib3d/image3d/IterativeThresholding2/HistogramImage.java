package mcib3d.image3d.IterativeThresholding2;

import mcib3d.image3d.ImageHandler;

import java.util.Map;
import java.util.TreeMap;
import java.util.stream.IntStream;

public class HistogramImage {
    final ImageHandler image;


    public HistogramImage(ImageHandler image) {
        this.image = image;
    }

    public Map<Float, Integer> getHistogramFloat() {
        if(image == null) return null;
        Map<Float, Integer> histogram = new TreeMap<>();

        IntStream.range(0, image.sizeZ).forEach(z ->
                IntStream.range(0, image.sizeXY).forEach(xy ->
                        histogram.merge(image.getPixel(xy, z), 1, Integer::sum)));

        return histogram;
    }
}
