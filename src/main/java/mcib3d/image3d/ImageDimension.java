package mcib3d.image3d;

public class ImageDimension {
    int sizeX;
    int sizeY;
    int sizeZ;
    int nbChannels = 1;
    int nbFrames = 1;
    double calXY = 1;
    double calZ = 1;
    String calUnit = "pix";

    public ImageDimension(int sizeX, int sizeY, int sizeZ) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
    }

    public ImageDimension(int sizeX, int sizeY, int sizeZ, int nbChannels, int nbFrames) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.sizeZ = sizeZ;
        this.nbChannels = nbChannels;
        this.nbFrames = nbFrames;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public int getSizeZ() {
        return sizeZ;
    }

    public void setSizeZ(int sizeZ) {
        this.sizeZ = sizeZ;
    }

    public int getNbChannels() {
        return nbChannels;
    }

    public void setNbChannels(int nbChannels) {
        this.nbChannels = nbChannels;
    }

    public int getNbFrames() {
        return nbFrames;
    }

    public void setNbFrames(int nbFrames) {
        this.nbFrames = nbFrames;
    }

    public double getCalXY() {
        return calXY;
    }

    public void setCalXY(double calXY) {
        this.calXY = calXY;
    }

    public double getCalZ() {
        return calZ;
    }

    public void setCalZ(double calZ) {
        this.calZ = calZ;
    }

    public String getCalUnit() {
        return calUnit;
    }

    public void setCalUnit(String calUnit) {
        this.calUnit = calUnit;
    }
}
