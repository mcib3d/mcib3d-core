package mcib3d.image3d;

import mcib3d.geom.Object3DVoxels;
import mcib3d.geom.Voxel3D;
import mcib3d.geom2.Object3DComputation;
import mcib3d.geom2.Object3DInt;
import mcib3d.geom2.Objects3DIntPopulation;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ImageLabeller {

    protected Map<Integer, Spot3D> listSpots = null;
    int[][] labels;
    boolean debug = false;
    double minSize = 0;
    double maxSize = Double.POSITIVE_INFINITY;
    // current mask
    protected ImageHandler currentMask = null;

    public ImageLabeller(boolean debug) {
        this.debug = debug;
    }

    public ImageLabeller() {
    }

    public ImageLabeller(double min, double max) {
        minSize = min;
        maxSize = max;
    }

    public double getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public void setMinSizeCalibrated(double minSize, ImageHandler ima) {
        this.minSize = Math.floor(minSize / (ima.getVoxelSizeXY() * ima.getVoxelSizeXY() * ima.getVoxelSizeZ()));
    }

    @Deprecated
    public double getMaxsize() {
        return maxSize;
    }

    @Deprecated
    public void setMaxsize(int maxSize) {
        this.maxSize = maxSize;
    }

    public double getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public void setMaxSizeCalibrated(double maxSize, ImageHandler ima) {
        this.maxSize = Math.ceil(maxSize / (ima.getVoxelSizeXY() * ima.getVoxelSizeXY() * ima.getVoxelSizeZ()));
    }

    private void labelSpots6(ImageHandler mask) {
        currentMask = mask;
        //label objects
        labels = new int[mask.sizeZ][mask.sizeXY];
        int sizeX = mask.sizeX;
        listSpots = new HashMap<>();
        int currentLabel = (short) 1;
        Spot3D currentSpot;
        Vox3D v;
        int nextLabel;
        int xy;
        if (debug) {
            System.out.println("Labelling...");
        }
        for (int z = 0; z < mask.sizeZ; z++) {
            for (int y = 0; y < mask.sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    xy = x + y * sizeX;
                    if (mask.getPixel(xy, z) != 0) {
                        currentSpot = null;
                        v = new Vox3D(xy, z);
                        /*if (x<limX) {
                         nextLabel = labels[z][xy+1];
                         if (nextLabel!=0) {
                         if (currentSpot==null) {
                         currentSpot = listSpots.get(nextLabel);
                         currentSpot.addVox(v);
                         } else if (nextLabel!=currentSpot.label) {
                         currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                         currentSpot.addVox(v);
                         }
                         }
                         }
                         *
                         */
                        if (x > 0) {
                            nextLabel = labels[z][xy - 1];
                            if (nextLabel != 0) {
                                if (currentSpot == null) {
                                    currentSpot = listSpots.get(nextLabel);
                                    currentSpot.addVox(v);
                                } else if (nextLabel != currentSpot.label) {
                                    currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                    currentSpot.addVox(v);
                                }
                            }
                        }
                        /*
                         if (y<limY) {
                         nextLabel = labels[z][xy+sizeX];
                         if (nextLabel!=0) {
                         if (currentSpot==null) {
                         currentSpot = listSpots.get(nextLabel);
                         currentSpot.addVox(v);
                         } else if (nextLabel!=currentSpot.label) {
                         currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                         currentSpot.addVox(v);
                         }
                         }
                         }
                         *
                         */
                        if (y > 0) {
                            nextLabel = labels[z][xy - sizeX];
                            if (nextLabel != 0) {
                                if (currentSpot == null) {
                                    currentSpot = listSpots.get(nextLabel);
                                    currentSpot.addVox(v);
                                } else if (nextLabel != currentSpot.label) {
                                    currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                    currentSpot.addVox(v);
                                }
                            }
                        }

                        /*
                         if (z<limZ) {
                         nextLabel = labels[z+1][xy];
                         if (nextLabel!=0) {
                         if (currentSpot==null) {
                         currentSpot = listSpots.get(nextLabel);
                         currentSpot.addVox(v);
                         } else if (nextLabel!=currentSpot.label) {
                         currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                         currentSpot.addVox(v);
                         }
                         }
                         }
                         *
                         */
                        if (z > 0) {
                            nextLabel = labels[z - 1][xy];
                            if (nextLabel != 0) {
                                if (currentSpot == null) {
                                    currentSpot = listSpots.get(nextLabel);
                                    currentSpot.addVox(v);
                                } else if (nextLabel != currentSpot.label) {
                                    currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                    currentSpot.addVox(v);
                                }
                            }
                        }

                        if (currentSpot == null) {
                            listSpots.put(currentLabel, new Spot3D(currentLabel++, v));
                        }
                    }
                }
            }
        }
    }


    private void labelSpots26(ImageHandler mask) {
        currentMask = mask;
        //label objects
        labels = new int[mask.sizeZ][mask.sizeXY];
        int sizeX = mask.sizeX;
        listSpots = new HashMap<>();
        int currentLabel = 1;
        Spot3D currentSpot;
        Vox3D v;
        int nextLabel;
        int xy;
        if (debug) {
            System.out.println("Labelling...");
        }
        for (int z = 0; z < mask.sizeZ; z++) {
            for (int y = 0; y < mask.sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    xy = x + y * sizeX;
                    if (mask.getPixel(xy, z) != 0) {
                        currentSpot = null;
                        v = new Vox3D(xy, z);
                        for (int k = -1; k <= 1; k++) {
                            for (int j = -1; j <= 1; j++) {
                                for (int i = -1; i <= 1; i++) {
                                    if ((mask.contains(x + i, y + j, z + k)) && (i * i + j * j + k * k != 0) && ((i < 0) || (j < 0) || (k < 0))) {
                                        nextLabel = labels[z + k][xy + i + j * sizeX];
                                        if (nextLabel != 0) {
                                            if (currentSpot == null) {
                                                currentSpot = listSpots.get(nextLabel);
                                                currentSpot.addVox(v);
                                            } else if (nextLabel != currentSpot.label) {
                                                currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                                currentSpot.addVox(v);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (currentSpot == null) {
                            listSpots.put(currentLabel, new Spot3D(currentLabel++, v));
                        }
                    }
                }
            }
        }
    }

    // NOT USED, IN TEST; avoid fusion when one object is too big
    private void labelSpotsCheckSize(ImageHandler mask) {
        currentMask = mask;
        //label objects
        labels = new int[mask.sizeZ][mask.sizeXY];
//        int limX = mask.sizeX - 1;
//        int limY = mask.sizeY - 1;
//        int limZ = mask.sizeZ - 1;
        int sizeX = mask.sizeX;
        listSpots = new HashMap<>();
        int currentLabel = (short) 1;
        Spot3D currentSpot;
        Vox3D v;
        int nextLabel;
        int xy;
        if (debug) {
            System.out.println("Labelling...");
        }
        for (int z = 0; z < mask.sizeZ; z++) {
            for (int y = 0; y < mask.sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    xy = x + y * sizeX;
                    if (mask.getPixel(xy, z) != 0) {
                        currentSpot = null;
                        v = new Vox3D(xy, z);
                        /*if (x<limX) {
                         nextLabel = labels[z][xy+1];
                         if (nextLabel!=0) {
                         if (currentSpot==null) {
                         currentSpot = listSpots.get(nextLabel);
                         currentSpot.addVox(v);
                         } else if (nextLabel!=currentSpot.label) {
                         currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                         currentSpot.addVox(v);
                         }
                         }
                         }
                         *
                         */
                        if (x > 0) {
                            nextLabel = labels[z][xy - 1];
                            if (nextLabel != 0) {
                                if (currentSpot == null) {
                                    currentSpot = listSpots.get(nextLabel);
                                    if (!currentSpot.tooBig) {
                                        currentSpot.addVox(v);
                                        if (currentSpot.getSize() > maxSize) {
                                            currentSpot.tooBig = true;
                                        }
                                    }
                                } else if (nextLabel != currentSpot.label) {
                                    if (!currentSpot.tooBig) {
                                        if (!listSpots.get(nextLabel).tooBig) {
                                            currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                            currentSpot.addVox(v);
                                            if (currentSpot.getSize() > maxSize) {
                                                currentSpot.tooBig = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        /*
                         if (y<limY) {
                         nextLabel = labels[z][xy+sizeX];
                         if (nextLabel!=0) {
                         if (currentSpot==null) {
                         currentSpot = listSpots.get(nextLabel);
                         currentSpot.addVox(v);
                         } else if (nextLabel!=currentSpot.label) {
                         currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                         currentSpot.addVox(v);
                         }
                         }
                         }
                         *
                         */
                        if (y > 0) {
                            nextLabel = labels[z][xy - sizeX];
                            if (nextLabel != 0) {
                                if (currentSpot == null) {
                                    currentSpot = listSpots.get(nextLabel);
                                    if (!currentSpot.tooBig) {
                                        currentSpot.addVox(v);
                                        if (currentSpot.getSize() > maxSize) {
                                            currentSpot.tooBig = true;
                                        }
                                    }
                                } else if (nextLabel != currentSpot.label) {
                                    if (!currentSpot.tooBig) {
                                        if (!listSpots.get(nextLabel).tooBig) {
                                            currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                            currentSpot.addVox(v);
                                            if (currentSpot.getSize() > maxSize) {
                                                currentSpot.tooBig = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        /*
                         if (z<limZ) {
                         nextLabel = labels[z+1][xy];
                         if (nextLabel!=0) {
                         if (currentSpot==null) {
                         currentSpot = listSpots.get(nextLabel);
                         currentSpot.addVox(v);
                         } else if (nextLabel!=currentSpot.label) {
                         currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                         currentSpot.addVox(v);
                         }
                         }
                         }
                         *
                         */
                        if (z > 0) {
                            nextLabel = labels[z - 1][xy];
                            if (nextLabel != 0) {
                                if (currentSpot == null) {
                                    currentSpot = listSpots.get(nextLabel);
                                    if (!currentSpot.tooBig) {
                                        currentSpot.addVox(v);
                                        if (currentSpot.getSize() > maxSize) {
                                            currentSpot.tooBig = true;
                                        }
                                    }
                                } else if (nextLabel != currentSpot.label) {
                                    if (!currentSpot.tooBig) {
                                        if (!listSpots.get(nextLabel).tooBig) {
                                            currentSpot = currentSpot.fusion(listSpots.get(nextLabel));
                                            currentSpot.addVox(v);
                                            if (currentSpot.getSize() > maxSize) {
                                                currentSpot.tooBig = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (currentSpot == null) {
                            listSpots.put(currentLabel, new Spot3D(currentLabel++, v));
                        }
                    }
                }
            }
        }
    }

    private void labelIndividualVoxel(ImageHandler mask) {
        currentMask = mask;
        labels = new int[mask.sizeZ][mask.sizeXY];
        int sizeX = mask.sizeX;
        listSpots = new HashMap<>();
        int currentLabel = 1;
        Vox3D v;
        int xy;
        if (debug) {
            System.out.println("Labelling...");
        }
        for (int z = 0; z < mask.sizeZ; z++) {
            for (int y = 0; y < mask.sizeY; y++) {
                for (int x = 0; x < sizeX; x++) {
                    xy = x + y * sizeX;
                    if (mask.getPixel(xy, z) != 0) {
                        v = new Vox3D(xy, z);
                        listSpots.put(currentLabel, new Spot3D(currentLabel++, v));
                    }
                }
            }
        }
    }

    private ImageHandler getLabelsGeneric(ImageHandler mask, boolean floatResult, boolean connex6) {
        if ((listSpots == null) || (mask != currentMask)) {
            if (connex6) {
                this.labelSpots6(mask);
            } else {
                this.labelSpots26(mask);
            }
        }
        ImageHandler res = floatResult ?
                new ImageFloat(mask.getTitle() + "::segmented", mask.sizeX, mask.sizeY, mask.sizeZ) :
                new ImageShort(mask.getTitle() + "::segmented", mask.sizeX, mask.sizeY, mask.sizeZ);
        res.setVoxelSize(mask);
        final AtomicInteger label = new AtomicInteger(1);
        for (Spot3D S : listSpots.values()) {
            if (S.voxels.size() >= minSize) {
                if (S.voxels.size() <= maxSize) {
                    float labelFloat = label.getAndIncrement();
                    for (Vox3D vox : S.voxels) res.setPixel(vox.xy, vox.z, labelFloat);
                }
            }
        }

        return res;
    }

    public ImageInt getLabels(ImageHandler mask, boolean connex6) {
        return (ImageInt) getLabelsGeneric(mask, false, connex6);
    }

    public ImageFloat getLabelsFloat(ImageHandler mask) {
        return getLabelsFloat(mask, false);
    }

    public ImageFloat getLabelsFloat(ImageHandler mask, boolean connex6) {
        return (ImageFloat) getLabelsGeneric(mask, true, connex6);

    }

    // classical default neighborhood for segmentation is 26
    public ImageInt getLabels(ImageHandler mask) {
        return getLabels(mask, false);
    }


    public ImageInt getLabelsWithSeeds(ImageHandler mask, ImageHandler markers) {
        return (ImageInt) getLabelsWithSeedsGeneric(mask, markers, false, false);
    }

    public ImageFloat getLabelsWithSeedsFloat(ImageHandler mask, ImageHandler markers) {
        return (ImageFloat) getLabelsWithSeedsGeneric(mask, markers, true, false);
    }

    private ImageHandler getLabelsWithSeedsGeneric(ImageHandler mask, ImageHandler markers, boolean floatResult, boolean connex6) {
        ImageHandler draw = floatResult ?
                new ImageFloat(mask.getTitle() + "::segmented", mask.sizeX, mask.sizeY, mask.sizeZ) :
                new ImageShort(mask.getTitle() + "::segmented", mask.sizeX, mask.sizeY, mask.sizeZ);
        AtomicInteger ai = new AtomicInteger(0);
        getObjects3D(mask, connex6).stream().filter(obj -> new Object3DComputation(obj).hasOneVoxelValueAboveStrict(markers, 0))
                .forEach(obj -> obj.drawObject(draw, ai.incrementAndGet()));
        return draw;
    }

    public int getNbObjectsTotal(ImageHandler mask, boolean connex6) {
        if ((listSpots == null) || (mask != currentMask)) {
            if (connex6) {
                this.labelSpots6(mask);
            } else {
                this.labelSpots26(mask);
            }
        }

        return listSpots.size();
    }

    public ImageInt getLabelsIndividualVoxels(ImageHandler mask) {
        if ((listSpots == null) || (mask != currentMask)) {
            labelIndividualVoxel(mask);
        }
        ImageShort res = new ImageShort(mask.getTitle() + "::segmented", mask.sizeX, mask.sizeY, mask.sizeZ);
        short label = 1;
        for (Spot3D s : listSpots.values()) {
            Vox3D vox = s.voxels.get(0);
            res.pixels[vox.z][vox.xy] = label;
            label++;
        }
        return res;
    }

    public ImageFloat getLabelsIndividualVoxelsFloat(ImageHandler mask) {
        if ((listSpots == null) || (mask != currentMask)) {
            labelIndividualVoxel(mask);
        }
        ImageFloat res = new ImageFloat(mask.getTitle() + "::segmented", mask.sizeX, mask.sizeY, mask.sizeZ);
        short label = 1;
        for (Spot3D s : listSpots.values()) {
            Vox3D vox = s.voxels.get(0);
            res.pixels[vox.z][vox.xy] = label;
            label++;
        }
        return res;
    }

    // classical default neighborhood for segmentation is 26
    public int getNbObjectsTotal(ImageHandler mask) {
        return getNbObjectsTotal(mask, false);
    }

    public int getNbObjectsInSizeRange(ImageHandler mask, boolean connex6) {
        // check if labelling needed
        if ((listSpots == null) || (mask != currentMask)) {
            if (connex6) {
                this.labelSpots6(mask);
            } else {
                this.labelSpots26(mask);
            }
        }
        // count nb objects
        int nbObj = 0;
        int sizeX = mask.sizeX;
        short label = 1;
        for (Spot3D s : listSpots.values()) {
            List<Vox3D> a = s.voxels;
            // check size
            if ((a.size() >= minSize) && (a.size() <= maxSize)) {
                nbObj++;
            }
        }

        return nbObj;
    }

    @Deprecated
    public int getNbObjectsinSizeRange(ImageHandler mask, boolean connex6) {
        // check if labelling needed
        if ((listSpots == null) || (mask != currentMask)) {
            if (connex6) {
                this.labelSpots6(mask);
            } else {
                this.labelSpots26(mask);
            }
        }
        // count nb objects
        int nbObj = 0;
        for (Spot3D s : listSpots.values()) {
            List<Vox3D> a = s.voxels;
            // check size
            if ((a.size() >= minSize) && (a.size() <= maxSize)) {
                nbObj++;
            }
        }

        return nbObj;
    }

    public int getNbObjectsInSizeRange(ImageHandler mask) {
        return getNbObjectsInSizeRange(mask, false);
    }

    // classical default neighborhood for segmentation is 26
    @Deprecated
    public int getNbObjectsinSizeRange(ImageHandler mask) {
        return getNbObjectsinSizeRange(mask, false);
    }

    public List<Object3DInt> getObjects3D(ImageHandler mask, boolean connex6) {
        ImageHandler handler = getLabelsFloat(mask, connex6);
        Objects3DIntPopulation population = new Objects3DIntPopulation(handler);

        return population.getObjects3DInt();
    }

    public List<Object3DInt> getObjects3D(ImageHandler mask) {
        return getObjects3D(mask, false);
    }

    // classical default neighborhood for segmentation is 26
    @Deprecated
    public List<Object3DVoxels> getObjects(ImageHandler mask, boolean connex6) {
        if ((listSpots == null) || (mask != currentMask)) {
            if (connex6) {
                this.labelSpots6(mask);
            } else {
                this.labelSpots26(mask);
            }
        }
        List<Object3DVoxels> objects = new ArrayList<>();
        int sizeX = mask.sizeX;
        short label = 1;
        for (Spot3D s : listSpots.values()) {
            List<Vox3D> a = s.voxels;
            // check size
            if ((a.size() >= minSize) && (a.size() <= maxSize)) {
                List<Voxel3D> voxels3D = new LinkedList<>();
                for (Vox3D vox : a) {
                    voxels3D.add(new Voxel3D(vox.xy % sizeX, vox.xy / sizeX, vox.z, label));
                }
                List noDuplicate = new LinkedList(new HashSet(voxels3D));
                // set calibration
                Object3DVoxels object3DVoxels = new Object3DVoxels(noDuplicate);
                object3DVoxels.setCalibration(mask.getVoxelSizeXY(), mask.getVoxelSizeZ(), mask.getUnit());
                objects.add(object3DVoxels);
                label++;
            }
        }
        return objects;
    }


    // classical default neighborhood for segmentation is 26
    @Deprecated
    public List<Object3DVoxels> getObjects(ImageHandler mask) {
        return getObjects(mask, false);
    }

    protected class Spot3D {
        List<Vox3D> voxels;
        int label;
        boolean tooBig = false;

        public Spot3D(int label, Vox3D v) {
            this.label = label;
            this.voxels = new LinkedList<>();
            voxels.add(v);
            v.setLabel(label);
        }

        public void addVox(Vox3D vox) {
            voxels.add(vox);
            vox.setLabel(label);
        }

        public void setLabel(int label) {
            this.label = label;
            for (Vox3D v : voxels) {
                v.setLabel(label);
            }
        }

        public Spot3D fusion(Spot3D other) {
            if (other.label < label) {
                return other.fusion(this);
            }

            listSpots.remove(other.label);
            // FIXME pb if size >= integer max size
            voxels.addAll(other.voxels);

            other.setLabel(label);

            return this;
        }

        public long getSize() {
            return voxels.size();
        }
    }

    protected class Vox3D {
        public int xy, z;

        public Vox3D(int xy, int z) {
            this.xy = xy;
            this.z = z;
        }

        public void setLabel(int label) {
            labels[z][xy] = label;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Vox3D) {
                return xy == ((Vox3D) o).xy && z == ((Vox3D) o).z;
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 3;
            hash = 47 * hash + this.xy;
            hash = 47 * hash + this.z;
            return hash;
        }
    }
}
