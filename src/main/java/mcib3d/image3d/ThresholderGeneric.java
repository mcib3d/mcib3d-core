package mcib3d.image3d;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ij.IJ;
import ij.process.AutoThresholder;

/**
 * Class to compute threshold
 * From a text dexcription
 * Value, %, auto
 */
public class ThresholderGeneric {
 public static Map<String, AutoThresholder.Method> methods = Collections.unmodifiableMap(new HashMap<String, AutoThresholder.Method>() {{
        put("isodata", AutoThresholder.Method.IsoData);
        put("otsu", AutoThresholder.Method.Otsu);
        //put("MINIMUM", AutoThresholder.Method.Minimum);
        //put("MINERROR", AutoThresholder.Method.MinError);
        //put("MOMENTS", AutoThresholder.Method.Moments);
        put("intermodes", AutoThresholder.Method.Intermodes);
        put("yen", AutoThresholder.Method.Yen);
        put("triangle", AutoThresholder.Method.Triangle);
        put("mean", AutoThresholder.Method.Mean);
        //put("RENYIENTROPY", AutoThresholder.Method.RenyiEntropy);
        //put("SHANBHAG", AutoThresholder.Method.Shanbhag);
        //put("YEN", AutoThresholder.Method.Yen);
        put("huang", AutoThresholder.Method.Huang);
        //put("PERCENTILE", AutoThresholder.Method.Percentile);
        //put("MAXENTROPY", AutoThresholder.Method.MaxEntropy);
        put("ij_isodata", AutoThresholder.Method.IJ_IsoData);
    }});


    public static float getThreshold(ImageHandler image, String thS){
        // check no text
        try{
            float th = Float.parseFloat(thS);
            return th;
        }
        catch(NumberFormatException e){
            // check contain %
            if(thS.contains("%")){
                String percentS = thS.split("%")[0].trim();
                double percent = Double.parseDouble(percentS)/100.0;
                return (float)image.getPercentile(percent, null);
            }
            else { // autoThreshold
                return autoThreshold(image, thS);
            }
        }
    }
    
    public static void main(String[] args) {
        ImageHandler image = ImageHandler.wrap(IJ.openImage("/home/thomas/AMU/DATA/Misc/t1-head.tif"));
        float a = ThresholderGeneric.getThreshold(image, "otsu");
        System.out.println("th="+a);
    }

    private static float autoThreshold(ImageHandler image, String thresholdS){
        // compute histogram
        ImageStats stat = image.getImageStats(null);
        int[] histo = stat.getHisto256();
        double binSize = stat.getHisto256BinSize();
        double min = stat.getMin();

        AutoThresholder at = new AutoThresholder();
        double thld = at.getThreshold(methods.get(thresholdS.toLowerCase()), histo);
        float threshold;
        if ((image instanceof ImageShort)||(image instanceof ImageFloat))
            threshold = (float) (thld * binSize + min);
        else
            threshold = (float) thld;
        return threshold;
    }
}
