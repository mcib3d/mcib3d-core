package mcib3d.tracking_dev;

import mcib3d.geom2.Object3DInt;


public class AssociationPairInt {
    private final Object3DInt object3D1;
    private final Object3DInt object3D2;
    private final double asso;

    public AssociationPairInt(Object3DInt object3D1, Object3DInt object3D2, double asso) {
        this.object3D1 = object3D1;
        this.object3D2 = object3D2;
        this.asso = asso;
    }

    public Object3DInt getObject3D1() {
        return this.object3D1;
    }

    public Object3DInt getObject3D2() {
        return this.object3D2;

    }

    public double getAsso() {
        return this.asso;
    }

}