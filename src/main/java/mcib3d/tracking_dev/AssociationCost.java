package mcib3d.tracking_dev;

import mcib3d.geom.Object3D;
import mcib3d.geom2.Object3DInt;

@Deprecated
public interface AssociationCost {
     double cost(Object3D object3D1, Object3D object3D2);
}
