package mcib3d.tracking_dev;

import mcib3d.geom.Object3D;
import mcib3d.geom.Objects3DPopulation;
import mcib3d.geom.Objects3DPopulationColocalisation;
import mcib3d.geom2.Object3DInt;

import java.util.HashMap;

@Deprecated
public class CostColocalisationDistanceBB implements AssociationCost {
    private final Objects3DPopulationColocalisation colocalisation;
    private final HashMap<Object3D, Double> objectsMaxRadialDist1; // Max distance center to border for objects population1
    private final HashMap<Object3D, Double> objectsMaxRadialDist2; // Max distance center to border for objects population2
    private final double distMax; // max distance BB (pixel)
    private final double pcColocMin;

    public CostColocalisationDistanceBB(Objects3DPopulation population1, Objects3DPopulation population2, double distMax, double colocMin) {
        this.distMax = distMax;
        this.pcColocMin = colocMin;
        this.colocalisation = new Objects3DPopulationColocalisation(population1, population2);
        objectsMaxRadialDist1 = new HashMap<>(population1.getNbObjects());
        for (Object3D object3D : population1.getObjectsList()) {
            objectsMaxRadialDist1.put(object3D, object3D.getDistCenterMaxPixel());
        }
        objectsMaxRadialDist2 = new HashMap<>(population2.getNbObjects());
        for (Object3D object3D : population2.getObjectsList()) {
            objectsMaxRadialDist2.put(object3D, object3D.getDistCenterMaxPixel());
        }
    }

    @Override
    public double cost(Object3D object3D1, Object3D object3D2) {
        double V1 = object3D1.getVolumePixels();
        double V2 = object3D2.getVolumePixels();
        // check if coloc mode or distance mode
        if(distMax == 0){ // mode coloc
            int coloc = colocalisation.getColocObject(object3D1, object3D2);
            double pcColoc = (double) coloc /  V1;
            if (pcColoc > pcColocMin) { // colocalised
                return (1.0 - ((double) coloc / Math.max(V1, V2)));// was Max(V1,V2)
            }
        }
        else { // mode distance -> colocMin = 0
            double distCC = object3D1.distCenterPixel(object3D2);
            double distCCMax = distMax + objectsMaxRadialDist1.get(object3D1) + objectsMaxRadialDist2.get(object3D2);
            if (distCC < distCCMax) {
                double dist = object3D1.distBorderPixel(object3D2);
                if (dist < distMax) return dist;
            }
        }

       return -1;
    }


}
