package mcib3d.tracking_dev;

import mcib3d.geom2.Object3DInt;

public interface AssociationCostInt {
     double cost(Object3DInt object3D1, Object3DInt object3D2);
}
